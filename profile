# .profile
#  Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - startup luancher, if needed
# - FYI: File  ~/.profile  is not used by Bash, if file  .bash_profile  exists.

v_profile='7.0.0'


# - Bash profile launcher:
#
if [[ -f ~/.bashrc ]]; then
    source ~/.bashrc
fi


# - A hook for extra stuff:
#
if [[ -f ~/.bashrc2 ]]; then
    source ~/.bashrc2
fi


