# bashrc_204
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - Ansible (server) macros

v_bashrc_204='9.0.0'


#############
# Utilities #
#############

# - Convert double-slash to single, in pathnames.

function f_deslash() {
    sed -e 's/\/\//\//g;'
}


# - Control the output color scheme:

function f_anc() {
    local x="$1"
    if [[ -z "${x}" ]]; then
        # - Visible, with high contrast:
        echo "- anc: color off:  \$ export ANSIBLE_NOCOLOR=1"
        export ANSIBLE_NOCOLOR=1
    else
        # - For people with good contrast-vision.
        echo "- anc: color on:  \$ unset ANSIBLE_NOCOLOR"
        unset ANSIBLE_NOCOLOR
    fi
}

alias anc=f_anc



#################
# Ansible Fixes #
#################

# - Suppress a Python warning.
#
# https://stackoverflow.com/questions/54833752/how-to-silence-paramiko-cryptographydeprecationwarnings-in-ansible
# https://github.com/ansible/ansible/issues/76737
# https://github.com/ansible/ansible/issues/77762
#
# mac$ ansible --version
#      /usr/local/Cellar/ansible/5.7.1/libexec/lib/python3.10/site-packages/paramiko/transport.py:236:
#      CryptographyDeprecationWarning: Blowfish has been deprecated
#      "class": algorithms.Blowfish,
#   ansible [core 2.12.5]
#   ...
#
#
# mac$ export PYTHONWARNINGS=ignore::UserWarning
# mac$ ansible --version
#   ansible [core 2.12.5]
#   ...
#
#
# mac$ env | grep PYTHON
#   PYTHONIOENCODING=UTF-8
#   PYTHONWARNINGS=ignore::UserWarning
#   CONDA_PYTHON_EXE=/Users/jdb/miniconda3/bin/python


export PYTHONWARNINGS=ignore::UserWarning



################
# Ansible Prep #
################

alias ans='echo "- Try these instead: aams aans aii ansd anv ape? xxw"'


# https://www.ansible.com/overview/how-ansible-works
# https://docs.ansible.com/ansible
# https://docs.ansible.com/ansible/latest/modules/modules_by_category.html

# - Wrap this alias in parenthesis sub-shell, so the output is easily grepped.
#
alias aans='(echo "- aan"; alias | egrep "^alias (an_|ap|aan)")'

alias an_v='(echo "-$ which ansible; ansible --version"; which ansible; ansible --version)'
alias anv=an_v

# - Example of ansible-doc:
alias ansd='echo "-$ ansible-doc -t lookup varnames | cat"; ansible-doc -t lookup varnames | cat'

# - For convenience:
alias ap='ansible-playbook'


# - Set the config file, within the Git repo:

if [[ -z "${ME}" ]]; then
    export ME="$(whoami)"       # - Typically set in bashrc_001
fi

if [[ -z "${ANSIBLE_CONFIG}" ]]; then

    if   [[ -f ~/zGit/ansible/etc/ansible."${ME}".cfg ]]; then        # - Uppercase 'J'
        export ANSIBLE_CONFIG=~/zGit/ansible/etc/ansible."${ME}".cfg

    elif [[ -f ~/zGit/ansible/etc/ansible.cfg ]]; then
        export ANSIBLE_CONFIG=~/zGit/ansible/etc/ansible.cfg

    else
        echo "-> FYI: bashrc_204: ANSIBLE_CONFIG was not detected."
    fi
fi



#####################
# Ansible Playbooks #
#####################

alias apes='echo "-$ ls \$ANSIBLE_PLAYBOOK_DIR"; ls $ANSIBLE_PLAYBOOK_DIR'


# - Strangely, ansible-playbook doesn't use the ANSIBLE_PLAYBOOK_DIR variable
#     set in ansible.cfg.
#   Therefore I define it here, for use in script wrappers.
#
# - https://stackoverflow.com/questions/61677294/ansible-change-playbooks-location

if [[ -z "${ANSIBLE_PLAYBOOK_DIR}" ]]; then
    if   [[ -d ~/zGit/ansible/playbooks/ ]]; then                 # - Uppercase 'J'
        export ANSIBLE_PLAYBOOK_DIR=~/zGit/ansible/playbooks/

    elif [[ -d ~/zGit/ansible/playbooks/ ]]; then                 # - Lowercase 'j'
        export ANSIBLE_PLAYBOOK_DIR=~/zGit/ansible/playbooks/

    else
        echo "-> FYI: bashrc_204: ANSIBLE_PLAYBOOK_DIR was not detected."
    fi
fi



#######################
# Ansible Inventories #
#######################

# - This is my own variable for convenience, not used by Ansible.

if [[ -z "${APE_INVENTORIES}" ]]; then
    if   [[ -d ~/zGit/ansible/inventories/ ]]; then               # - Uppercase 'J'
        export APE_INVENTORIES=~/zGit/ansible/inventories/

    elif [[ -d ~/zGit/ansible/inventories/ ]]; then               # - Lowercase 'j'
        export APE_INVENTORIES=~/zGit/ansible/inventories/

    else
        echo "-> FYI: bashrc_204: APE_INVENTORIES was not detected."
    fi
fi


# - Inventory info wrappers
#   https://stackoverflow.com/questions/58651943/can-an-ansible-inventory-include-another

alias aii='echo "-$ ansible-inventory -i \${APE_INVENTORIES} all --graph"; ansible-inventory -i ${APE_INVENTORIES} all --graph'

alias  aii_j='echo "-$ ansible-inventory -i \${APE_INVENTORIES}/hosts.jdb.yml      all --graph"; ansible-inventory -i ${APE_INVENTORIES}/hosts.jdb.yml all --graph'
alias  aii_e='echo "-$ ansible-inventory -i \${APE_INVENTORIES}/hosts.exampler.yml all --graph"; ansible-inventory -i ${APE_INVENTORIES}/hosts.exampler.yml all --graph'


# Simplified:

alias  ali='echo "-$ ansible --list-hosts  \"*\""; ansible --list-hosts  "*"'


# - Massive JSON output:

alias aams="echo '-$ ansible -m setup ocean > ~/tmp/ansible.ocean.json'; ansible -m setup ocean > ~/tmp/ansible.ocean.json"



#################
# Ansible Vault #
#################

# - Environment variables with names  ANSIBLE_VAULT_...  are used by
#   ansible-playbook command, but not necessarily by ansible-vault command!
#
# - FYI: important discussion of the Ansible "vault-id", and "vault-id label" :
#   "By default, the vault ID label is only a hint to remind you which password you used to encrypt a variable or file."
#   "For example, if you encrypt a file with --vault-id dev@prompt, the vault-id-label is dev."
#   https://docs.ansible.com/ansible/latest/vault_guide/vault_managing_passwords.html#managing-multiple-passwords-with-vault-ids
#   https://docs.ansible.com/ansible/latest/vault_guide/vault_using_encrypted_content.html
#
#
# - FYI:
#   https://docs.ansible.com/ansible/latest/reference_appendices/config.html#default-vault-identity
#   https://docs.ansible.com/ansible/latest/reference_appendices/config.html#default-vault-identity-list
#
#   $ apevi "${ANSIBLE_VAULT_IDENTITY}"     # - Optional, when these environment vars are set.
#   $ ape 00_e localhost


# - Vaults and passwords are defined as being in this Ansible area,
#   but might be sym-linked elsewhere.
#
if [[ -z "${APE_VAULT_FILES}" ]]; then
    export APE_VAULT_FILES="${APE_INVENTORIES}/group_vars/all/"
fi
if [[ -z "${APE_VAULT_PASSWORDS}" ]]; then
    export APE_VAULT_PASSWORDS=~/.vaults/
fi


# - ToDo: Stub.
#
if [[ $(false) && -z "${ANSIBLE_VAULT_IDENTITY}" ]]; then
    # - Master tag
    export ANSIBLE_VAULT_ENCRYPT_IDENTITY="demoVaultId"

    # - Master tag, plus Master password file (in a secure local location.)
    #   -> ToDo: The password should not be in a file!
    #
    export ANSIBLE_VAULT_IDENTITY="demoVaultId@${APE_VAULT_PASSWORDS}/password-1.txt"

    # - Skip this to avoid confusion. Another workflow is needed than that used here.
    ## # - This can have multiple vaults:
    ## export ANSIBLE_VAULT_IDENTITY_LIST="demoVaultId@${APE_VAULT_PASSWORDS}/password-1.txt"

    # - The vault itelf!
    export ANSIBLE_VAULT_FILE="${APE_VAULT_FILES}/vault-1.yml"

    # - Password file, on the Ansible controller, never on the Ansible clients. Assumed secure.
    #   ToDo: This is not so secure.
    #   However, it can also be an executable to another vault.
    #
    export ANSIBLE_VAULT_PASSWORD_FILE="${APE_VAULT_PASSWORDS}/password-1.txt"

    # - Enforce matching the VAULT_ID.
    export ANSIBLE_VAULT_ID_MATCH=True

    # - Not set, use Ansible's default randomness.
    ## export ANSIBLE_VAULT_ENCRYPT_SALT=xxx
fi


# - Unconditionally set all valid VAULT-IDENTITY combos.
unset ANSIBLE_VAULT_IDENTITY_LIST
ANSIBLE_VAULT_IDENTITY_LIST="demoVaultId@${APE_VAULT_PASSWORDS}/password-1.txt"
export ANSIBLE_VAULT_IDENTITY_LIST


# - Show aliases:
alias ansv='alias | grep "ansv"; funky | grep vault'


# - Split a list on a comma
#
function f_csplit_csv() {
    perl -nle '@y=split/,/,$_; for my $x (@y) {print "..  $x";}'
}


# - Show Ansible Vault variables
#
function f_ansv_show() {
    local qq

    echo "- v_bashrc_204: ${v_bashrc_204}"

    echo
    echo "$ env | egrep 'ANSV_|ANSIBLE_VAULT|APE_VAULT' | sort"
    echo ANSIBLE_VAULT_IDENTITY_LIST= | f_indent
    echo ${ANSIBLE_VAULT_IDENTITY_LIST} | f_csplit_csv | f_deslash | f_indent
    env | egrep 'ANSV_|ANSIBLE_VAULT|APE_VAULT' | grep -v ANSIBLE_VAULT_IDENTITY_LIST | sort | f_deslash | f_indent

    echo
    echo "$ (cd \${APE_VAULT_FILES}/ ; ls -lhF v*.yml)" | f_deslash
    (cd ${APE_VAULT_FILES}/ ; ls -lhF v*.yml) | f_deslash | f_indent

    ## echo
    echo
    echo "# - Vault keys:"
    for qq in ${APE_VAULT_FILES}/vault*.yml; do
        echo
        echo $qq | f_deslash | f_indent
        guts $qq | perl -nle 'print if (/^\S/);' | f_deslash | f_indent - 8
    done

    ## echo
    echo
    echo "# - Vault password files:"
    echo
    ls -hF ${APE_VAULT_PASSWORDS}/password*.txt | f_deslash | f_indent

    echo
    return 0
}

alias ansv_show=f_ansv_show


# - Set (activate) an Ansible Vault context, which always includes the demo.
#   $ f_ansv_set 0
#   $ f_ansv_set 1
#   ...
#
# - FYI:
#   This selector mechanism doesn't work well, due to Ansible inconsistencies.
#   Simply setting ANSIBLE_VAULT_IDENTITY_LIST works best for reading vaults.

function f_ansv_set() {
    local which=$1
    local error=0
    local qq rc

    # - Unconditionally set all valid VAULT-IDENTITY combos.
    echo '-$ export ANSIBLE_VAULT_IDENTITY_LIST=...'
    echo
    unset ANSIBLE_VAULT_IDENTITY_LIST
    ANSIBLE_VAULT_IDENTITY_LIST="demoVaultId@${APE_VAULT_FILES}/password-1.txt"
    export ANSIBLE_VAULT_IDENTITY_LIST

    # - ToDo: Stub the rest of this intention.
    return 0


    if [[ -z "${which}" ]]; then
        which='none'
    fi

    case ${which} in
        0)
            unset ANSIBLE_VAULT_ENCRYPT_IDENTITY
            unset ANSIBLE_VAULT_IDENTITY
            unset ANSIBLE_VAULT_FILE
            unset ANSIBLE_VAULT_PASSWORD_FILE
            ## unset ANSIBLE_VAULT_IDENTITY_LIST
            ;;
        1)
            # - Set to "demo default".
            export ANSIBLE_VAULT_ENCRYPT_IDENTITY="demoVaultId"
            export ANSIBLE_VAULT_IDENTITY="demoVaultId@${APE_VAULT_FILES}/password-1.txt"
            export ANSIBLE_VAULT_FILE=${APE_VAULT_FILES}/vault-1.yml
            export ANSIBLE_VAULT_PASSWORD_FILE="${APE_VAULT_FILES}/password-1.txt"
            ## export ANSIBLE_VAULT_IDENTITY_LIST="demoVaultId@${APE_VAULT_FILES}/password-1.txt"
            ;;
        *)
            error=1
            ;;
    esac

    if [[ ${error} -ne 0 ]]; then
        echo "- Error, bad or missing WHICH parameter '${which}'. Examples:"
        echo "  $ f_ansv_set 0      # - Unset the variables"
        echo "  $ f_ansv_set 1      # - demoVaultId"
        echo "  $ f_ansv_show       # - Show results"
        echo
        return 1
    fi

    return 0
}

alias ansv_set=f_ansv_set


# - FYI:
#   Avoid using  ANSIBLE_VAULT_PASSWORD_FILE
#   This seems to be obsolete with Ansible v2.4, superseded with
#       ANSIBLE_VAULT_IDENTITY_LIST  and  ANSIBLE_VAULT_IDENTITY
#
# - FYI:
#   https://docs.ansible.com/ansible/latest/reference_appendices/config.html#envvar-ANSIBLE_VAULT_PASSWORD_FILE
#   https://developers.redhat.com/blog/2020/01/30/vault-ids-in-red-hat-ansible-and-red-hat-ansible-tower
#
#   DEFAULT_VAULT_PASSWORD_FILE
#   Description
#      The vault password file to use.
#      Equivalent to –vault-password-file or –vault-id If executable,
#       it will be run and the resulting stdout will be used as the password.
#   Type: path
#   Default: None
#   Environment: Variable: ANSIBLE_VAULT_PASSWORD_FILE
#
## export ANSIBLE_VAULT_PASSWORD_FILE=${APE_VAULT_FILES}/password-1.txt


function i_ansible_vault() {
    cat <<END
    #########################
    # Ansible Vault summary #
    #########################
    - v_bashrc_204: ${v_bashrc_204}

# - https://gitlab.com/umi-ch/ansible
# - https://gitlab.com/umi-ch/bashrc
#
# - With "John's APE convention" (Ansible Playbook via Environment-variables),
#       only this ANSIBLE_ environment variable is needed:  ANSIBLE_VAULT_IDENTITY_LIST
#   It's set to a list of vaults and password files.

$ ansv_show
    - v_bashrc_204: 9.0.0

    $ env | egrep 'ANSV_|ANSIBLE_VAULT|APE_VAULT' | sort
        ANSIBLE_VAULT_IDENTITY_LIST=
        ..  demoVaultId@~/.vaults/password-1.txt
        APE_VAULT_FILES=~/zGit/ansible/inventories/group_vars/all/
        APE_VAULT_PASSWORDS=~/.vaults/

    $ ( cd ${APE_VAULT_FILES} ; ls -lhF v*.yml )
        lrwxr-xr-x@ 1 jdb  staff    30B Nov 17  2022 vault-1.yml@ -> ~/.vaults/vault-1.yml
    
    # Vault keys:
    
        ~/zGit/ansible/inventories/group_vars/all/vault-1.yml
            ---
            demoKey: !vault |
            ansv_demoKey_1: !vault |
            ansv_inbound_authorized_keys_default: !vault |
            ansv_ubuntu_password: !vault |
    
    # Vault password files:
    
        ~/.vaults/password-1.txt


    ###########
    # ansible #
    ###########

# - 'ansible' command works, implicitly using \$ANSIBLE_VAULT_IDENTITY_LIST

$ ansible localhost  -m ansible.builtin.debug  -a var="ansv_demoKey_1"
    localhost | SUCCESS => {
        "ansv_demoKey_1": "demoPassword-1-Default"
    }


    #################
    # ansible-vault #
    #################

# - Ironically, 'ansible-vault' command doesn't support management of "vault files"
#       with multiple secrets.
#   It only works as a "Unix filter", encrypting and decrypting entire strings or files,
#       not "selections" from a vault file.
# - Use 'yq' to extract keys.
#       https://stackoverflow.com/questions/43467180/how-to-decrypt-string-with-ansible-vault-2-3-0

$ yq '.ansv_demoKey_1' "\${APE_VAULT_FILES}/vault-1.yml"
    \$ANSIBLE_VAULT;1.2;AES256;demoVaultId
    336...37
    653...38
    306...32
    363...64
    356...65

$ yq '.ansv_demoKey_1' "\${APE_VAULT_FILES}/vault-1.yml" | ansible-vault decrypt --vault-password-file "\${APE_VAULT_PASSWORDS}/password-1.txt"; echo
    Decryption successful
    demoPassword-1-Default


    ################################
    # ansible-playbook             #
    # - using John's APE mechanism #
    ################################

$ alias ape
    alias ape='f_ape'

$ ape 00_e localhost
    -# 00_e.show-vaulted.yml
    ...
    -# vault_id=''
    -# ANSIBLE_VAULT_IDENTITY_LIST='demoVaultId@~/.vaults/password-1.txt'
    -# ANSIBLE_VAULT_ID_MATCH='True'   
    -$ ansible-playbook   '/Users/jdb/zGit/ansible/playbooks/00_e.show-vaulted.yml'
    PLAY [localhost] ***************************************************************
    ...
    TASK [20. Encrypted variables: ansv_demoKey_*] *********************************
    ok: [localhost] => {
        "msg": [
            ansv_demoKey_0    :  undefined
            ansv_demoKey_1    : demoPassword-1-Default
            ...
            ansv_demoKey_bozo :  undefined
    
    PLAY RECAP *********************************************************************
    localhost                  : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0


    ###########################
    # create a vault identity #
    ###########################

# - Use a local variable here, instead of exporting ANSIBLE_IDENTITY:
$ my_ANSIBLE_IDENTITY='demoVaultId'

# - As new vaults (and corresponding passwords) are created, update this list in ~/.zrc/bashrc_...
$ export ANSIBLE_VAULT_IDENTITY_LIST="\${ANSIBLE_VAULT_IDENTITY_LIST}, ..."


    #####################
    # create a password #
    #####################
    #
    # - FYI: Ansible's scripting mechanism can be used as well, but not shown here.

$ mkdir -p  ~/.vaults/
$ chmod 700 ~/.vaults/

# - Create a password for this vault:
$ echo 'demoMasterPassword' > "\${APE_VAULT_PASSWORDS}/password-1.txt"

# - More secure alternative:
$ openssl rand -hex 20 | tee "\${APE_VAULT_PASSWORDS}/password-1.txt"
    f1f...72

$ chmod 600 "\${APE_VAULT_PASSWORDS}/password-1.txt"


    ##########################
    # new Ansible Vault file #
    ##########################

# - Create a new Ansible Vault file:
$ touch "\${APE_VAULT_PASSWORDS/vault-1.yml}"

# - Sym-linked the new vault into Ansible space:
$ deslashed=\$(echo "\${APE_VAULT_PASSWORDS}/vault-1.yml" | f_deslash)
$ ( cd "\${APE_VAULT_FILES}" ; ln -s "\${deslashed}" . )

# - Populate the new vault:
$ printf "# Ansible Vault YAML\n\n---\n\n"  > "\${APE_VAULT_FILES}/vault-1.yml"

# - FYI: The  --name  must be unique across all vault files, eg: ansv_demoKey_1
#   Otherwise, explicitly set ANSIBLE_VAULT_ID before running f_ape.

$ (ansible-vault encrypt_string  'demoPassword-1-Default'  --name 'ansv_demoKey_1'  --encrypt-vault-id "\${my_ANSIBLE_IDENTITY}"; echo; echo) | tee -a "\${APE_VAULT_FILES}/vault-1.yml"
    ansv_demoKey_1: !vault |
          \$ANSIBLE_VAULT;1.2;AES256;demoVaultId
          336...37
            ...
          356...65

END
}

