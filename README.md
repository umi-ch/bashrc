# John's Agile Bashrc

This is a multi-tenant bashrc system, for interactive Bash shells.<br/>
It's called **"agile"** because it enables good ability to quickly create and respond to change.<br/>

Supported platforms:
- Linux
- MacOS
- GitBash/Cygwin
- Solaris

&nbsp;

# Installation

Download the repository from Git, and make a sym-link: `~/.zrc`

```
$ repo=https://gitlab.com/umi-ch/bashrc.git
$ dest=~/zGit/bashrc

$ cd ~
$ mkdir -p ~/zGit/

$ rm -f ~/.zrc
$ rm -f -r ${dest}

# - Clone the repository, and create a sym-link with this name: &nbsp; ~/.zrc

$ git clone  ${repo}  ${dest}
$ ln -s  ${dest}  .zrc

# - Alternate: shallow clone, for one-time use or "redeploy everything" updates:
$ git clone  --depth=1  ${repo}  ${dest}
$ ln -s  ${dest}  .zrc
```

&nbsp;

## Configure

A configuration is needed, to define which ```bashrc_xxx``` files should be loaded.<br>
The simplest config is **"directly on the server"**, using the sample template file.<br>
Other methods (described elsewhere) include **Ansible configs** and by **nonce** in ```bashrc_020```

```
$ cp  ~/.zrc/bashrc-seq.sample  ~/.bashrc-seq
$ vi  ~/.bashrc-seq
  ...
```

&nbsp;

## Option 1: &nbsp; Auto-load on login

File `.bash_profile` unconditionally loads the bashrc files upon login.

```
$ cd ~

$ .zrc/binp/linker.sh
    ...
    $ mv    .bash_profile      .bash_profile-ORIG
    $ mv    .profile           .profile-ORIG
    $ mv    .bashrc            .bashrc-ORIG
    $ mv    .vimrc             .vimrc-ORIG

    $ ln -s .zrc/bash_profile  .bash_profile
    $ ln -s .zrc/profile       .profile
    $ ln -s .zrc/bashrc_001    .bashrc
    $ ln -s .zrc/vimrc         .vimrc

    $ ls -lahdF .bash* .profile* .vimrc*
    lrwxr-xr-x  1 jdb  staff  17B  .bash_profile@ -> .zrc/bash_profile
    lrwxr-xr-x  1 jdb  staff  15B  .bashrc@ -> .zrc/bashrc_001
    lrwxr-xr-x  1 jdb  staff  12B  .profile@ -> .zrc/profile
    lrwxr-xr-x  1 jdb  staff  10B  .vimrc@ -> .zrc/vimrc
```

&nbsp;

## Option 2:  &nbsp; Load on request.

Create a **Bash alias** to load the setup when desired.<br/>
This is especially useful with shared technical accounts, which don't load it by default.<br/>

```
$ alias brc='source ~/.zrc/bashrc_001'
$ brc
  ...
```

&nbsp;

## Login

Your next login will look something like this:

```
Last login: Thu Feb 18 18:37:49 2019 from ::1
- bashrc_100
- bashrc_101
- bashrc_102
  ...
```

Alternately, if using the **Bash alias**:

```
$ brc
- bashrc_100
- bashrc_101
- bashrc_102
  ...
```

&nbsp;

## Confirm

Try some aliases.

```
$ cd ~/.zrc/

$ cat bashrc_* | wc -l                      # - How many lines of Bashrc code?
   15684

$ cat bashrc_* | wc -l | commify            # - Big number, with comma
   15,684

$ cat bashrc_* | wc -l | commify2           # - Big number, with single quote
   15'684


$ alias guts
    alias guts='perl -nle '\''chomp; next if /^\s*$/; next if /^\s*#/; print;'\'''

$ cat bashrc_* | guts | wc -l | commify2    # - Count Bashrc lines, without blanks or comments
    8'593


$ ssl_grep  https://cnn.com
    -> cnn.com
    -$ openssl s_client -connect cnn.com:443 -servername cnn.com -showcerts < /dev/null 2> /tmp/q_ssl_grep.lKLh0l  |  cert_grep

    0: Certificate:
      Public-Key: (2048 bit)
      Issuer:   C = BE, O = GlobalSign nv-sa, CN = GlobalSign Atlas R3 DV TLS CA 2022 Q1
      Subject:  CN = cnn.com
      Validity:
         Not Before: Feb 11 17:10:46 2022 GMT
         Not After : Mar 15 17:10:45 2023 GMT
      Data:
         Version: 3 (0x2)
         Serial Number: 01:b0:e8:19:b3:bf:cd:a7:fe:cf:da:9f:56:54:4d:55
      X509v3 Subject Alternative Name:
         DNS:*.api.cnn.com
         DNS:*.api.cnn.io
     ...


$ alias funky
    alias funky='( echo; echo "-\$ funcs | grep \" i_\""; funcs | grep " i_"; echo )'

$ funky
    -$ funcs | grep " i_"
    ...
    declare -f i_conda
    declare -f i_conda_2
    ...

$ i_conda_2
    #################################
    # Miniconda Python environments #
    #################################
    https://stackoverflow.com/questions/59163078/how-to-change-python-version-of-existing-conda-virtual-environment#59163125

    $ conda info --envs
        base                  *  /Users/jdb/miniconda3

    $ which python3; python3 --version
        /Users/jdb/miniconda3/bin/python3
        Python 3.9.7
    ...
```

<br>


## Agile theme
This repository has useful working examples, quickly and iteratively adaptable to new environments.
<br/>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>

