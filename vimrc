" v_vimrc='7.0.0'
" Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

" URL: http://vim.wikia.com/wiki/Example_vimrc
" Authors: http://vim.wikia.com/wiki/Vim_on_Freenode
" Description: A minimal, but feature rich, example .vimrc. If you are a
"              newbie, basing your first .vimrc on this file is a good choice.
"              If you're a more advanced user, building your own .vimrc based
"              on this file is still a good idea.

" URL: https://vim.fandom.com/wiki/Disable_automatic_comment_insertion
" Disable auto-comments insertion:
"
set formatoptions-=cro

" - Disable ALL mouse actions in VIM:
"
set mouse=
set ttymouse=

" - Tabs to spaces
set expandtab
set shiftwidth=4
set tabstop=4

" - Other:
set ignorecase
set smartcase
set noshowmatch

