# bashrc_101
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - X509 certificate tools

v_bashrc_101='10.5.0'


# - Tools included here:
#
#   f_zero
#   f_hexify, f_decify
#   f_ugrep1, f_ugrep2, f_ugrep3
#   f_cert_fp


    #############
    # Utilities #
    #############

# - Formatting; Show only the first cert in a PEM file: ie, from 0: to the next cert (if any).
#   -> "Until a blank line" is no longer adequate, with the latest f_cert_grep.
#   -> FYI:
#
#   $ ssl_grep www.ethz.ch | grep 'Certificate:'
#       0: Certificate:
#       1: Certificate:
#       2: Certificate:

function f_zero() {
    perl -ne '
        BEGIN {
            our $seen = 0;
        }
        chomp;
        if (/^0:/) {
            $seen = 1;
            print "$_\n";
            next;
        } elsif ($seen and /^\s*\d+:\s*Certificate:/i) {
            $seen = 0;
            next;
        } elsif ($seen) {
            print "$_\n";
            next;
        } else {
            next;
        }
    '
    rc=$?
    return $rc
}

alias fz=f_zero



    #####################
    # hexify and decify #
    #####################

# $ cat CA/serial
#   F8E204AD59EF7DAB
#
# $ hexify CA/serial
#   F8:E2:04:AD:59:EF:7D:AB

function f_hexify() {
    local v=0

    if [[ -n "${VERBOSE}" && "${VERBOSE}" -ge 1 ]]; then
        v=1
        if [[ "${VERBOSE}" -ge 2 ]]; then
            v="${VERBOSE}"
            echo "- VERBOSE $v"
        fi
    fi

    if [[ -z "$1" ]]; then f='-'; else f="$1"; fi

    cat "$f" | perl -ne '
        use strict;
        chomp;
        print hexify($_). "\n";

        sub hexify() {
            my $x = shift;
            $x =~ s/([0-9a-fA-F]{2})/:\U$1/g;   # - Uppercase
            $x =~ s/^://;
            return $x;
        }
    '

    rc=$?
    return $rc
}

alias hexify=f_hexify


# $ echo '63:cb:58:a6:3f:31:68:af:b9:0a:7e:17:92:a0:38:8d' | decify
#   132649405868466627492479833396983380109

alias decify='perl -nle '\''use strict; use Math::BigInt; chomp; s/://g; my $x=Math::BigInt->new("0x".$_); print $x->bdstr();'\'''



    ##################
    # Misc utilities #
    ##################

# $ cat cert.pem | ugrep1
#
function f_ugrep1() {
	openssl x509 -text -noout | \
	egrep -i '^Cert|Subject:|Issuer:|Serial |Not ' | \
	sed -e 's/^  */  /g;'
}


# $ cat csr.pem | ugrep2
#
function f_ugrep2() {
	openssl req -text -noout | \
	egrep -i '^Cert|Subject:|Issuer:|Serial |Not ' | \
	sed -e 's/^  */  /g;'
}


# $ cat key4.pem      | ugrep3  myPassword
# $ cat key-nopw.pem  | ugrep3
#
function f_ugrep3() {
	local pw="$1"  # - password
	local rc=0

	## -passin pass:myPassword
	## -passin file:/dev/stdin
	## -passin file:/dev/tty

	if  [[ -n "$pw" ]]; then
		openssl rsa  -noout -check  -passin pass:"$pw"
		rc=$?
	else
		openssl rsa  -noout -check  -passin file:/dev/random    # - Ensure no password is delivered.
		rc=$?
	fi
	return $rc
}

alias ugrep1=f_ugrep1
alias ugrep2=f_ugrep2
alias ugrep3=f_ugrep3



    #############
    # f_cert_fp #
    #############
    #
    # - certificate fingerprints

function i_cert_fingerprints() {
    cat <<END
    ###################################
    # f_cert_fp:                      #
    # - Show certificate fingerprints #
    ###################################
    - v_bashrc_101: ${v_bashrc_101}

$ funcs | grep f_cert_fp
    declare -f f_cert_fp_md5
    declare -f f_cert_fp_sha1
    declare -f f_cert_fp_sha256

$ echo \${LE_CA}
    ~/.zrc/zPKI/isrgrootx1.pem

$ cat \${LE_CA} | f_cert_fp_md5
  md5 Fingerprint = 0C:D2:F9:E0:DA:17:73:E9:ED:86:4D:A5:E3:70:E7:4E

$ f_cert_fp_md5  \${LE_CA}
  md5 Fingerprint = 0C:D2:F9:E0:DA:17:73:E9:ED:86:4D:A5:E3:70:E7:4E

$ f_cert_fp_sha1  \${LE_CA}
  sha1 Fingerprint = CA:BD:2A:79:A1:07:6A:31:F2:1D:25:36:35:CB:03:9D:43:29:A5:E8

$ f_cert_fp_sha256 \${LE_CA}
  sha256 Fingerprint = 96:BC:EC:06:26:49:76:F3:74:60:77:9A:CF:28:C5:A7:CF:E8:A3:C0:AA:E1:1A:8F:FC:EE:05:C0:BD:DF:08:C6

END
}

function f_cert_fp_md5() {
    local f="$1"
	local x='-md5'

	if [[ -z "$f" ]]; then f='-'; fi
	cat "$f" | openssl x509 -noout "$x" -fingerprint | sed -e 's/=/ = /'
}

function f_cert_fp_sha1() {
    local f="$1"
	local x='-sha1'

	if [[ -z "$f" ]]; then f='-'; fi
	cat "$f" | openssl x509 -noout "$x" -fingerprint | sed -e 's/=/ = /'
}

function f_cert_fp_sha256() {
    local f="$1"
	local x='-sha256'

	if [[ -z "$f" ]]; then f='-'; fi
	cat "$f" | openssl x509 -noout "$x" -fingerprint | sed -e 's/=/ = /'
}



