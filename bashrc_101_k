# bashrc_101_k
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - X509 certificate tools

v_bashrc_101_k='10.6.0'


# - Tools included here:
#
#   f_crl_grep
#   i_crl_grep


    ##############
    # f_crl_grep #
    ##############

# - CRL : Certificate Revocation List
#   -> In a CRL file, print all lines before the signature hex dump.
#   $ crl_grep  iCerts/CA/crl/iCert-crl.2019-05-29_01.pem
#
# - Both PEM (ASCII text) and DER (binary) file formats are supported.

function f_crl_grep() {
    local v=0
    local data out rc=0   
    local f                                             # - CRL file, or STDIN
    local temp_file_1='/bozo/1'  temp_file_2='/bozo/2'  # - temp files, for preprocessing
    local openssl_cmd='crl'
    local is_pem=0

    if [[ -n "${VERBOSE}" && "${VERBOSE}" -ge 1 ]]; then
        v=1
        if [[ "${VERBOSE}" -ge 2 ]]; then
            v="${VERBOSE}"
            echo "- VERBOSE $v"
        fi
    fi

    # - Create temporary files for pre-processing, but not always used.
    which mktemp > /dev/null 2>&1
    rc=$?
    if [[ $rc -eq 0 ]]; then
        temp_file_1=$(mktemp -q /tmp/q_crl_grep_1.XXXXXX)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, mktemp fails, rc=$rc : /tmp/q_crl_grep_1.XXXXXX"
            return 1
        fi

        temp_file_2=$(mktemp -q /tmp/q_crl_grep_2.XXXXXX)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, mktemp fails, rc=$rc : /tmp/q_crl_grep_2.XXXXXX"
            return 1
        fi
    else
        temp_file_1='/tmp/q_ssl_grep_1.$$'
        temp_file_2='/tmp/q_ssl_grep_2.$$'
    fi
    if [[ $v -ge 1 ]]; then
        echo "- Temporary crl files:"
        echo "  ${temp_file_1}"
        echo "  ${temp_file_2}"
        echo
    fi

    if [[ -n "$1" ]]; then
        f="$1"
    else
        f='-'
        cat "${f}"  > "${temp_file_1}"
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- f_crl_grep(): Command fails, rc=$rc"
            echo "  -$ cat '${f}'  > '${temp_file_1}'"
            rm -f "${temp_file_1}" "${temp_file_2}"     # - Unconditional cleanup.
            echo
            return 1
        fi
        f="${temp_file_1}"
    fi

    # - Is this an ASCII text PEM file?
    #
    out=$( file "${f}" 2>&1 )
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "- f_crl_grep(): Command fails, rc=$rc"
        echo "  -$ file '${f}'"
        echo "${out}"
        echo
        rm -f "${temp_file_1}" "${temp_file_2}"     # - Unconditional cleanup.
        return 1
    fi
    echo "${out}" | grep 'ASCII text'  > /dev/null 2>&1
    rc=$?
    if [[ $rc -eq 0 ]]; then
        # - Yes, effectively a PEM file.
        is_pem=1
    fi

    if [[ $is_pem -eq 1 ]]; then
        # - Remove leading- and trailing- spaces from each line.
        cat "$f" | perl -ple 's/^\s+//g; s/\s+$//g;'  > ${temp_file_2}
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "- Error, Perl pre-processing fails, rc=$rc"
            echo "  -$ cat '$f' | perl -ple 's/^\s+//g; s/\s+$//g;'"
            echo
            rm -f "${temp_file_1}" "${temp_file_2}"     # - Unconditional cleanup.
            return 1
        fi
        f="${temp_file_2}"
    fi

    # - Diagnostic for script testing:
    ## if [[ -z "$2" ]]; then
    ##     openssl_cmd='crl'
    ## else
    ##     openssl_cmd="$2"
    ## fi

    cat "${f}" | openssl "${openssl_cmd}" -text -noout | perl -e '
        use strict;
        my @lines = <>;
        chomp @lines;
        my @a;
        my $seen = 0;
        my $next_to_hex = 0;

        for my $i (0 .. $#lines - 1) {
            $_ = $lines[$i];
            s/^\s+//g;          # - Remove leading spaces
            s/\s+$//g;          # - Remove trailing spaces

            if (/^\s*Signature Algorithm:/i) {
                $seen += 1;
                if ($seen >= 2) {
                    last;
                }
            }
            next if /^\s*$/;

            if ($next_to_hex) {
                $next_to_hex = 0;
                my $hex = sprintf ("%X", $_);
                $_ .= "  [". hexify($hex). "]";
            }
            if (/^\s*X509v3 CRL Number:/) {
                $next_to_hex = 1;
            }
            if (/^(\s*)(keyid:)(.*)$/) {
                $ _= $1. $2. "  ". $3;
            }
            if (/^\s*Revoked Certificates:/i) {
                push @a, "";
            }
            if (/^(\s*)(Serial Number:)\s+(\S+)\s*$/i) {
                push @a, "";
                $ _= $1. $2. " ". $3. "  [". hexify($3). "]";
            }
            push @a, $_;
        }

        END {
                for (@a) {
                    $_ = "  ". $_  if /^\S/;    # - Indent
                    print "$_\n";
                }
            print "\n";
        }

        sub hexify() {
            my $x = shift;
            $x =~ s/([0-9a-fA-F]{2})/:\U$1/g;   # - Uppercase
            $x =~ s/^://;
            return $x;
        }
    '
    rc=$?

    rm -f "${temp_file_1}" "${temp_file_2}"     # - Unconditional cleanup.
    return $rc
}


# - shortcuts:
alias  crlgrep=f_crl_grep
alias crl_grep=f_crl_grep



    ########
    # Info #
    ########

function i_crl_grep() {
    cat <<END
    #######################################
    # OpenSSL wrapper to show CRL content #
    #######################################
    - v_bashrc_101_k: ${v_bashrc_101_k}

- Both PEM (ASCII text) and DER (binary) file formats are supported.

$ cd ~/.zrc/zPKI/       # - Examples in Johns-Bashrc

$ ls -l crl-example.pem  crl-example2.crl
    -rw-r-----@ 1 jdb  staff   625B Feb 16  2024 crl-example.pem
    -rw-r--r--@ 1 jdb  staff    76K Feb  6  2024 crl-example2.crl

$ file  crl-example.pem  crl-example2.crl
    crl-example.pem:  ASCII text
    crl-example2.crl: data


$ cat ~/.zrc/zPKI/crl-example.pem
    -----BEGIN X509 CRL-----
    MIIBpjCBjwIBATANBgkqhkiG9w0BAQUFADBNMQswCQYDVQQGEwJYWTEmMCQGA1UE
    CgwdUHl0aG9uIFNvZnR3YXJlIEZvdW5kYXRpb24gQ0ExFjAUBgNVBAMMDW91ci1j
    YS1zZXJ2ZXIXDTEzMTEyMTE3MDg0N1oXDTIzMDkzMDE3MDg0N1qgDjAMMAoGA1Ud
    FAQDAgEAMA0GCSqGSIb3DQEBBQUAA4IBAQCNJXC2mVKauEeN3LlQ3ZtM5gkH3ExH
    +i4bmJjtJn497WwvvoIeUdrmVXgJQR93RtV37hZwN0SXMLlNmUZPH4rHhihayw4m
    unCzVj/OhCCY7/TPjKuJ1O/0XhaLBpBVjQN7R/1ujoRKbSia/CD3vcn7Fqxzw7LK
    fSRCKRGTj1CZiuxrphtFchwALXSiFDy9mr2ZKhImcyq1PydfgEzU78APpOkMQsIC
    UNJ/cf3c9emzf+dUtcMEcejQ3mynBo4eIGg1EW42bz4q4hSjzQlKcBV0muw5qXhc
    HOxH2iTFhQ7SrvVuK/dM14rYM4B5mSX3nRC1kNmXpS9j3wJDhuwmjHed
    -----END X509 CRL-----

$ crl_grep ~/.zrc/zPKI/crl-example.pem
    Certificate Revocation List (CRL):
    Version 2 (0x1)
    Signature Algorithm: sha1WithRSAEncryption
    Issuer: C = XY, O = Python Software Foundation CA, CN = our-ca-server
    Last Update: Nov 21 17:08:47 2013 GMT
    Next Update: Sep 30 17:08:47 2023 GMT
    CRL extensions:
    X509v3 CRL Number:
    0  [0]
    No Revoked Certificates. 


$ crl_grep crl-example2.crl | wc -l
    7637

$ crl_grep crl-example2.crl | less
    Certificate Revocation List (CRL):
    Version 2 (0x1)
    Signature Algorithm: sha384WithRSAEncryption
    Issuer: C = US, O = "DigiCert, Inc.", CN = DigiCert Trusted G4 Code Signing RSA4096 SHA384 2021 CA1
    Last Update: Feb  1 09:00:09 2024 GMT
    Next Update: Feb  8 09:00:09 2024 GMT
    CRL extensions:
    X509v3 Authority Key Identifier:
    68:37:E0:EB:B6:3B:F8:5F:11:86:FB:FE:61:7B:08:88:65:F4:4E:42
    X509v3 CRL Number:
    1006  [3EE]
    
    Revoked Certificates:
    
    Serial Number: 05EC0DFCB430D94599C493D1FC5B30A2  [05:EC:0D:FC:B4:30:D9:45:99:C4:93:D1:FC:5B:30:A2]
    Revocation Date: May 31 00:00:01 2021 GMT
    ...

    Serial Number: 0C252BCE3F01CD8F92802847BFD7ADA3  [0C:25:2B:CE:3F:01:CD:8F:92:80:28:47:BF:D7:AD:A3]
    Revocation Date: Jan 31 22:59:09 2024 GMT
    CRL entry extensions:
    X509v3 CRL Reason Code:
    Cessation Of Operation
    
    Serial Number: 0CA716A47B72A33E70019480F5D663B3  [0C:A7:16:A4:7B:72:A3:3E:70:01:94:80:F5:D6:63:B3]
    Revocation Date: Feb  1 08:35:47 2024 GMT
    CRL entry extensions:
    X509v3 CRL Reason Code:
    Superseded
      
END
}


