#!/usr/bin/env bash
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# symver.org version
echoer_sh_version='2.0.0'


# - Simple test, to echo all script parameters.
#
#   $ echoer
#   $ echoer one
#   $ echoer one two three
#
#   $ echoer one "two three" four
#   one
#   two three
#   four

if [[ -z "$@" ]]; then
    echo NONE
else
    for i in "$@"; do
        echo $i
    done
fi

exit 0

