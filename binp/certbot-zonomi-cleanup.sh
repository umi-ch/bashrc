#!/bin/bash
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

certbot_zonomi_cleanup_sh_version='7.0.0'


# - Johns-bashrc
source ~/.zrc/bashrc_101_c

le_renew_cleanup_hook_zonomi
rc=$?

exit $rc

