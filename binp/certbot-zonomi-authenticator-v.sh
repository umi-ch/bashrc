#!/bin/bash
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

certbot_zonomi_authenticator_v_sh_version='7.0.0'


# - Johns-bashrc
source ~/.zrc/bashrc_101_c

export VERBOSE=1

## le_renew_auth_hook_zonomi  1>&2     # - Use with: set -x
le_renew_auth_hook_zonomi
rc=$?

exit $rc

