#!/usr/bin/env bash
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - Make links to install John's Bashrc
v_linker_sh='7.0.0'

echo

grep v_bashrc_001 .bashrc > /dev/null  2>&1
rc=$?
if [[ $rc -eq 0 ]]; then
    echo "$ grep v_bashrc_001 .bashrc > /dev/null  2>&1"
    echo "  -> Text found, links are already installed, exit 0."
    echo
    exit 0
fi

mv=0
if [[ -f .bash_profile ]]; then
    echo "$ mv    .bash_profile      .bash_profile-ORIG"
    mv .bash_profile  .bash_profile-ORIG
    rc=$?; if [[ $rc -ne 0 ]]; then echo "- Error, command fails, rc=$rc"; exit 1; fi
    (( mv++ ))
fi

if [[ -f .profile ]];      then
    echo "$ mv    .profile           .profile-ORIG"
    mv .profile  .profile-ORIG
    rc=$?; if [[ $rc -ne 0 ]]; then echo "- Error, command fails, rc=$rc"; exit 1; fi
    (( mv++ ))
fi

if [[ -f .bashrc ]];       then
    echo "$ mv    .bashrc            .bashrc-ORIG"
    mv .bashrc  .bashrc-ORIG
    rc=$?; if [[ $rc -ne 0 ]]; then echo "- Error, command fails, rc=$rc"; exit 1; fi
    (( mv++ ))
fi

if [[ -f .vimrc ]];         then
    echo "$ mv    .vimrc             .vimrc-ORIG"
    mv .vimrc  .vimrc-ORIG;
    rc=$?; if [[ $rc -ne 0 ]]; then echo "- Error, command fails, rc=$rc"; exit 1; fi
    (( mv++ ))
fi

if [[ ${mv} -gt 0 ]]; then echo; fi


echo "$ ln -s .zrc/bash_profile  .bash_profile"
ln -s .zrc/bash_profile  .bash_profile
rc=$?; if [[ $rc -ne 0 ]]; then echo "- Error, command fails, rc=$rc"; exit 1; fi

echo "$ ln -s .zrc/profile       .profile"
ln -s .zrc/profile  .profile
rc=$?; if [[ $rc -ne 0 ]]; then echo "- Error, command fails, rc=$rc"; exit 1; fi

echo "$ ln -s .zrc/bashrc_001    .bashrc"
ln -s .zrc/bashrc_001  .bashrc
rc=$?; if [[ $rc -ne 0 ]]; then echo "- Error, command fails, rc=$rc"; exit 1; fi

echo "$ ln -s .zrc/vimrc         .vimrc"
ln -s .zrc/vimrc  .vimrc
rc=$?; if [[ $rc -ne 0 ]]; then echo "- Error, command fails, rc=$rc"; exit 1; fi


echo
echo "$ ls -lahdF .bash* .profile* .vimrc* .zrc* zGit*"
ls -lahdF .bash* .profile* .vimrc* .zrc*  zGit* | perl -ple '
            $s=" "x10;
            if (/(\s\S+)(\s+)(->)/) {
                my $a = $1;
                my $b = $2;
                my $c = $3;
                my $len = length ($a);
                my $pad = " " x (15 - $len);
                s/${a}${b}${c}/${a}${pad}${b}${c}/;
            }
        '

rc_0=${PIPESTATUS[0]}  rc_1=${PIPESTATUS[1]}
if [[ $rc_0 -ne 0 ]]; then echo "- Error, command fails, rc_0=$rc"; exit 1; fi
if [[ $rc_1 -ne 0 ]]; then echo "- Error, command fails, rc_1=$rc"; exit 1; fi

echo
exit 0


