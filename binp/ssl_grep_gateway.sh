#!/usr/bin/env bash
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

v_ssl_grep_gateway_sh='3.0.0'


# JDB, 08. Dec 2019
#
# ssl_grep_gateway.sh :
# - Determine the "default gateway address" of the current machine (if any),
#     and run f_ssl_grep upon it.
#   This can be useful on virtual-network machines.
#
# - f_ssl_grep is a function in John's Bashrc.
#
#   vlan-133_$ ifc4
#   -$ ifconfig -a | grep inet | grep -v inet6
#             inet addr:192.168.1.119  Bcast:192.168.1.255  Mask:255.255.255.0
#             inet addr:127.0.0.1  Mask:255.0.0.0
#
#   vlan-133_$ netstat -rn
#       Kernel IP routing table
#       Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
#       0.0.0.0         192.168.1.1     0.0.0.0         UG        0 0          0 eth0
#       192.168.1.0     0.0.0.0         255.255.255.0   U         0 0          0 eth0
#
#   vlan-133_$ netstat -rn | grep UG | perl -ple 's/^\S+\s+(\S+)\s.*$/$1/;'
#       192.168.1.1
#
#   vlan-133_$ netstat -rn | grep BOZO | perl -ple 's/^\S+\s+(\S+)\s.*$/$1/;'
#   vlan-133_$
#
#   $ ssl_grep_gateway.sh
#       ...

rc=0
v=0

echo "= ssl_grep_gateway.sh v${v_ssl_grep_gateway_sh}"

if [[ -n "${VERBOSE}" && "${VERBOSE}" -gt 0 ]]; then
    v=${VERBOSE}
    echo
fi


    ####################
    # Helper functions #
    ####################

function f_indent_old() {
    local c="$1"
    local str

    if [[ -z "${c}" ]]; then
        c=4
    fi
    str=$(printf "%${c}s")
    sed -e "s/^/${str}/;"
}

    ############################################################
    # - Grunge: Load all cert_grep functions in John's Bashrc. #
    ############################################################
    #
    #   -> No guarantees about the naming conventions!

if [[ -z "${myBashDir}" ]]; then
    myBashDir=~/.zrc                # - Default.
fi

files="${myBashDir}/bashrc_101*"
if [[ $v -ge 1 ]]; then
    echo "- files: ${files}"
fi

for file in ${files}; do
    if [[ $v -ge 1 ]]; then
        echo "  -$ source ${file}"
    fi
    source "${file}"
    rc=$?
    if [[ $rc -ne 0 ]]; then
        if [[ $v -eq 0 ]]; then
            echo "  -$ source ${file}"
        fi
        echo "    -> Error, rc: $rc"
        echo "    ${out}"
    fi
done


    #################
    # Sanity checks #
    #################

type f_ssl_grep > /dev/null 2>&1
rc=$?
if [[ $rc -ne 0 ]]; then
    echo
    echo "- Error, f_ssl_grep is not found."
    echo "  Auto-load from \"John's Bashrc\" has failed."
    echo "  Hint: ~/.zrc/bashrc_101_i"
    echo
    exit 1
fi


if [[ $v -ge 1 ]]; then
    echo
    echo "v\$ netstat=\"\$(netstat -rn)\"";
fi
#
netstat=$(netstat -rn)
#
rc=$?
if [[ $rc -ne 0 ]]; then
    echo "- Error, netstat -rn failed, rc=$?"
    exit 1
fi


    ############
    # gateways #
    ############

# - Look for UG string: an interface is UP, and is a gateway.
#
if [[ $v -ge 1 ]]; then
    echo "v\$ gateways=\"\$(echo \"\${netstat}\" | grep UG)\""
fi

gateways="$(echo "${netstat}" | grep UG)"

if [[ -z "${gateways}" ]]; then
    # - Quick hack: MS WSL doesn't shows the UG flag, it just has ^0.0.0.0
    # - FYI:
    #   w$ uname -a
    #   Linux VDWP1805 4.4.0-18362-Microsoft #1801-Microsoft Sat Sep 11 15:28:00 PST 2021 x86_64 x86_64 x86_64 GNU/Linux
    #
    if [[ -n "$(uname -a | grep Microsoft)" ]]; then
        if [[ $v -ge 1 ]]; then
            echo
            echo "-> Microsoft WSL workaround:"
            echo "v\$ gateways=\"\$(echo \"\${netstat}\" | grep '^0\.0\.0\.0' | perl -ple 's/^\S+\s+(\S+)\s.*$/\$1/;' | head -1)\""
        fi
        gateways=$(echo "${netstat}" | grep '^0\.0\.0\.0' | head -1)
    fi
fi

if [[ -z "${gateways}" ]]; then
    echo "- Error, no UG gateaways found."
    echo
    exit 1
elif [[ $v -ge 1 ]]; then
    echo  "-> gateways:"
    echo "${gateways}"
fi


# - Initialize:
gateway=''


# - Look for something like this: (Ubuntu)
#   0.0.0.0  10.92.204.1  255.255.255.255 U   0 0 0 eth0
#
if [[ -z "${gateway}" ]]; then
    if [[ $v -ge 1 ]]; then
        echo
        echo "v\$ gateway=\"\$(echo \"\${gateways}\" | grep '^0\.0\.0\.0' | perl -ple 's/^\S+\s+(\S+)\s.*$/\$1/;' | head -1)\""
    fi
    #
    gateway=$(echo "${gateways}" | grep '^0\.0\.0\.0' | perl -ple 's/^\S+\s+(\S+)\s.*$/$1/;' | head -1)
    rc=$?
    if [[ $v -ge 1 ]]; then
            echo  "-> gateway: '${gateway}'"
    fi
fi


# - Look for something like this: (MacOS)
#   default  192.168.1.1  UGScI   en0
#
if [[ -z "${gateway}" ]]; then
    if [[ $v -ge 1 ]]; then
        echo
        echo "v\$ gateway=\"\$(echo \"\${gateways}\" | grep '^default' | perl -ple 's/^\S+\s+(\S+)\s.*$/\$1/;' | head -1)\""
    fi
    #
    gateway=$(echo "${gateways}" | grep '^default' | perl -ple 's/^\S+\s+(\S+)\s.*$/$1/;' | head -1)
    rc=$?
    if [[ $v -ge 1 ]]; then
            echo  "-> gateway: '${gateway}'"
    fi
fi

if [[ $v -ge 1 ]]; then
        echo
fi
if [[ -z "${gateway}" ]]; then
    echo "- Error, no gateway found, with netstat -rn"
    exit 1
fi


    ############
    # ssl_grep #
    ############

# - Transparent:
echo
echo "-$ f_ssl_grep \"${gateway}\""

output=$( f_ssl_grep "${gateway}" 2>&1 )
rc=$?

if [[ $rc -ne 0 ]]; then
    echo "$output"
else
    echo "$output" | f_indent_old
fi
echo

if [[ $rc -ne 0 ]]; then
    echo "- exit $rc"
    echo
fi

exit $rc

