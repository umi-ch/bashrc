#!/bin/bash
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - Run this with sudo.
# - https://workbench.cisecurity.org/sections/752369/recommendations/1229123

if command -v nmcli >/dev/null 2>&1 ; then
  echo '$' nmcli radio all off
  nmcli radio all off
else
  if [ -n "$(find /sys/class/net/*/ -type d -name wireless)" ]; then
    drivers=$(for driverdir in $(find /sys/class/net/*/ -type d -name wireless | xargs -0 dirname); do basename "$(readlink -f "$driverdir"/device/driver)";done | sort -u)
    for dm in $drivers; do
      echo "install $dm /bin/true >> /etc/modprobe.d/disable_wireless.conf"
      echo "install $dm /bin/true" >> /etc/modprobe.d/disable_wireless.conf
    done
  fi
  #
  echo '$' cat /etc/modprobe.d/disable_wireless.conf
  cat /etc/modprobe.d/disable_wireless.conf
fi

