#!/usr/bin/env bash

# - Create or renew LetsEncrypt SSL/TSL X.509 certficates using LEGO
#   and DNS validation, with a supported DNS provider, eg Zonomi.
#
# - LEGO is an alternative to using CertBot, but this script still uses
#   CertBot filename conventions for "PROD" use, for historical compatiblity.
#
# - The working directory (aka DEV) for obtaining certs is defined in $ELE_DIR_DEV
#   which defaults to  ~/.lego/
#
# - The PROD  /etc/letsencypt/  directory structure from CertBot is used
#   for use by web servers (etc), eg, referenced in NGINX configs
#
# - A two-step loading process is used:
#   1. DEV:  certificates are acquired with LEGO, into $ELE_DIR_DEV, typically ~/.lego/ 
#   2. PROD: certificates are copied from LEGO directory: $ELE_DIR_DEV
#      into CertBot directory: $ELE_DIR_PROD, typically /etc/letsencrypt/
#
# - This script either runs as root, or uses sudo to get root access for ELE_DIR_PROD work.
#   It can also run as non-root mode for DEV/TEST, but this is somewhat less secure
#   as access to the certificate private key is not restricted to root.
#
# - Beware of external service limits by LetsEncrypt, getting certs for a particular domain.
#   Eg, no more 5 requests in 24 hours for a specific domain & domain_sans bundle.
#
#
# - Dependency:
#   Bashrc functions from John's Bashrc:  f_cert_grep, f_zero   
#   -> Loaded from  ~/.zrc/bashrc_101*  else this script fails with an error message.
#
# - FYI:
#   https://github.com/go-acme/lego/discussions/1914
#   https://go-acme.github.io/lego/installation/
#   https://go-acme.github.io/lego/dns/
#   https://go-acme.github.io/lego/usage/cli/renew-a-certificate/
#   https://www.mslinn.com/blog/2023/03/02/lego.html
#   https://github.com/go-acme/lego
#
#   https://go-acme.github.io/lego/dns/zonomi/
#   https://zonomi.com/

lego_certs_zonomi_sh_version='2.3.0'


    ############
    # Defaults #
    ############
    #
    # - ELE = /etc Let's Encrypt

# - Verbose mode can be overriden with -v option.
if [[ -n "${VERBOSE}" && "${VERBOSE}" -ge 1 ]]; then
    v=1
else
    v=0
fi

if [[ -z "${ELE_DIR_PROD}" ]]; then
    # - To test this script, try this:  $ export ELE_DIR_PROD=/etc/letsencrypt-X/
    export ELE_DIR_PROD='/etc/letsencrypt/'
fi

if [[ -z "${ELE_DIR_DEV}" ]]; then
    # - Eg:  ~jdb/.lego/  and  ~root/.lego/  but it can be elsewhere.
    export ELE_DIR_DEV=~/.lego/
fi


# - Assume this works.
whoami=$(whoami 2>&1)
rc=$?
if [[ $rc -ne 0 ]]; then
    echo "-$ whoami"
    echo "   - Error, command fails, rc: $rc"
    echo "  ${whoami}"
    exit 1
fi


p1='-$ '    # - Prompt: always a trailing space.
p2='-$'     # - Prompt: no trailing space, if $SUDO is empty.

if [[ -z "${ELE_USE_SUDO}" ]]; then
    # - Default. sudo is required to setup the default $ELE_DIR_PROD, if we're not root.
    if [[ "${whoami}" == 'root' ]]; then
        export ELE_USE_SUDO=0               # - False
        SUDO=''
    else
        export ELE_USE_SUDO=1               # - True
        SUDO='sudo'
        p2='-$ '
    fi
elif [[ -n "${ELE_USE_SUDO}" && "${ELE_USE_SUDO}" -eq 0 ]]; then
    # - Use this setting, with a test $ELE_DIR_PROD, eg: /tmp/letsencrypt/
    export ELE_USE_SUDO=0               # - False
    SUDO=''
elif [[ -n "${ELE_USE_SUDO}" && "${ELE_USE_SUDO}" -eq 1 ]]; then
    # - Typically not useful, but available anyway.
    export ELE_USE_SUDO=1               # - True
    SUDO='sudo'
    p2='-$ '
else
    echo "- Error, assertion failure: ELE_USE_SUDO: '${ELE_USE_SUDO}'"
    exit 1
fi


    ##############################################
    # Load required functions from John's Bashrc #
    ##############################################
    #
    # - Assumed location:  ~/.zrc/bashrc_101*
    # - Script exit on failure.

function f_load_bashrc() {
    local rc rc_1 rc_2
    local file files

    files=~/.zrc/bashrc_101*

    type f_show_certs > /dev/null 2>&1
    rc_1=$?

    type f_zero > /dev/null 2>&1
    rc_2=$?

    if [[ $rc_1 -eq 0 && $rc_2 -eq 0 ]]; then
        return 0
    fi

    if [[ $v -ge 1 ]]; then
        echo
        echo "## source ~/.zrc/bashrc_101*"      # - Summary
    fi

    for file in ${files}; do
        if [[ $v -ge 1 ]]; then
            echo "-$ source ${file}"
        fi
        source "${file}"
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "$ source ${file}"
            echo "- Error, rc: $rc"
            exit $rc
        fi
    done

    return 0
}


    #########
    # Usage #
    #########

function f_usage() {
    local bn=$(basename $0)

    cat <<END

- This script manages Lets Encrypt X.509 certificates, using Lego instead of CertBot.
  The /etc/letsencrypt/ directory structure of CertBot is used.
  DNS validation is used.

- https://go-acme.github.io/lego
- https://github.com/go-acme/lego/discussions/1914

Usage:
  $ $bn [-v] [--help]  diags | sel | run | renew | copy | show

Examples:
  $ $bn  diags
  $ $bn  show
  $ $bn  sel              # - Setup /etc/letsencrypt/

  $ export LEGO_DNS_PROVIDER=zonomi
  $ export LEGO_EMAIL=john@...
  $ export LEGO_DOMAIN=aws.example.com
  $ export LEGO_DOMAIN_SANS='*.aws.example.com'
  $ export LEGO_EXPIRATION_DAYS=35      # - Optional, override default=30, for longer expirations.
  $ export ZONOMI_API_KEY=...

  $ $bn  run show         # - First time: get cert, and confirm.
  $ $bn  copy show        # - Then copy it into PROD area.
  ... restart NGINX                 # - Web servers may need to reload their cert caches.

  $ $bn  renew copy show  # - Renew a cert, > 30 days, < 90 days
  ... restart NGINX

END

    return 1
}


    #############
    # Utilities #
    #############

# - Confirm that sudo works.
#   ToDo: This test is incomplete, as other sudo commands are needed too.
#   But the purpose of this check is confirming that password-less sudo is mostly available.

function f_confirm_sudo() {
    local rc=0

    echo 
    echo "-$ sudo cp /etc/hosts /dev/null"
    sudo cp /etc/hosts /dev/null
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        return 1
    fi
    echo "   -> sudo works."
    return 0
}


# - Indent the output, line by line in realtime.
#   -> For compatibility with Bashrc, this is the "old" f_indent function.

function f_indent_old() {
    sed -e "s/^/   /;"
    return 0
}


# - Convert double-slash to single-slash in pathnames,
#   This is for printing aesthetics in Linux, which accepts multiple slashes as one.
#       (But this is not necessarily true elsewhere, eg: Amazon AWS S3 bucket objects.)
#
#   $ echo 'top/playbooks//05_a.apt-upgradable-list.yml' | f_deslash
#   -> top/playbooks/05_a.apt-upgradable-list.yml
#
#   $ echo nix | f_deslash
#   nix
#
#   $ f_deslash file
#   -> not useful, the function just "hangs" in an STDIN read.

function f_deslash() {
    sed  -e 's/\/\//\//g;'
}


# - Remove a trailing slash:
#
#   $ echo '/etc//letsencrypt/ | f_deslash2
#   -> /etc/letsencrypt

function f_deslash2() {
    sed  -e 's/\/$//g;'
}


# - Show run context.
#   Just assume it works.

f_context_stamp() {
    local qq qq2
    local which

    echo
    echo "-$ date; whoami; hostname -f"
    (date; whoami; hostname -f) | f_indent_old

    return 0
}


# - Diagnostics
#   Skipped: ZONOMI_API_KEY
#
# - Command errors here are not fatal, merely skipped.

f_diags() {
    local rc=0  rcx=0
    local out qq which

    echo
    echo
    echo "#########" | f_indent_old
    echo "# Diags #" | f_indent_old
    echo "#########" | f_indent_old

    echo
    echo "- Variables:"
    for qq in \
        v                       \
        ELE_DIR_DEV             \
        ELE_DIR_PROD            \
        ELE_USE_SUDO            \
        SUDO                    \
        LEGO_DNS_PROVIDER       \
        LEGO_DOMAIN             \
        LEGO_DOMAIN_SANS        \
        LEGO_EMAIL              \
        LEGO_EXPIRATION_DAYS    \
    ; do
        printf "  %-25s: %s\n"  ${qq}  "${!qq}"
    done

    echo
    echo "-$ which lego"
    which=$(which lego 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, lego not found. Try this:"
        echo "     $ sudo apt install lego"
        rcx=1
    else
        echo "${which}" | f_indent_old

        echo
        echo "-$ ${which} --version"
        out=$(${which} --version 2>&1)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "   - Error, command fails, rc: $rc"
            rcx=1
        fi
        echo "${out}" | f_indent_old
    fi

    # - Sadly, --version above might say 'dev' instead of a version number.
    #   Try this instead on an Debian/Ubuntu system, accepting errors.

    which=$(which dpkg 2>&1)
    rc=$?
    if [[ $rc -eq 0 ]]; then
        echo
        echo "-$ dpkg --list lego | grep ^ii | perl -ple 's/\s\s\s+/  /g'"
        out=$( dpkg --list lego 2>&1 )
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "   - Error, command fails, rc: $rc"
            rcx=1
        else
            out=$( dpkg --list lego | grep ^ii | perl -ple 's/\s\s\s+/  /g' )
        fi
        echo "${out}" | f_indent_old
    else
        echo
        echo "- Command skipped:"
        echo "  -$ dpkg --list lego | grep ^ii | perl -ple 's/\s\s\s+/  /g'"
    fi


    echo
    echo
    echo "###############" | f_indent_old
    echo "# Directories #" | f_indent_old
    echo "###############" | f_indent_old
    echo

    echo "${p2}${SUDO} ls -ldhF \${ELE_DIR_DEV}"
    out=$(${SUDO} ls -ldhF "${ELE_DIR_DEV}" 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        rcx=1
    fi
    echo "${out}" | f_deslash | f_indent_old

    echo
    echo "${p2}${SUDO} ls -lhF \${ELE_DIR_DEV}"
    out=$(${SUDO} ls -lhF "${ELE_DIR_DEV}" 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        rcx=1
    fi
    echo "${out}" | f_deslash | f_indent_old

    echo
    echo "${p2}${SUDO} ls -lhF \${ELE_DIR_DEV}/certificates/"
    out=$(${SUDO} ls -lhF "${ELE_DIR_DEV}/certificates/" 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        rcx=1
    fi
    echo "${out}" | f_deslash | f_indent_old

    echo
    echo
    echo "${p2}${SUDO} ls -ldhF \${ELE_DIR_PROD}"
    out=$(${SUDO} ls -ldhF "${ELE_DIR_PROD}" 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        rcx=1
    fi
    echo "${out}" | f_deslash | f_indent_old

    echo
    echo "${p2}${SUDO} ls -lhF \${ELE_DIR_PROD}"
    out=$(${SUDO} ls -lhF ${ELE_DIR_PROD} 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        rcx=1
    fi
    echo "${out}" | f_deslash | f_indent_old

    echo
    echo "${p2}${SUDO} ls -lhF \${ELE_DIR_PROD}/UMI/"
    out=$(${SUDO} ls -lhF ${ELE_DIR_PROD}/UMI/ 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        rcx=1
    fi
    echo "${out}" | f_deslash | f_indent_old

    return $rcx
}


    ###########################
    # Setup /etc/letsencrypt/ #
    ###########################
    #
    # - FYI:
    #   /etc/letsencrypt/live/...   ->  Certs from LetEncrypt CertBot tool
    #   /etc/letsencrypt/LEGO/      ->  My convention, for directory with the 2 cert files from Lego tool
    #   /etc/letsencrypt/UMI        ->  My convention:
    #                                   -> original sym link to  /etc/letsencrypt/live/...
    #                                   -> new      sym link to  /etc/letsencrypt/LEGO/
    #
    # - In general, file access here is restricted to root, except that the certificate
    #       itself should be "world readable" to users on this system, eg:
    #   $ certgrep ${ELE_DIR_PROD}/UMI/fullchain.pem

function f_setup_etc_letsencrypt() {
    local rc=0
    local out pattern qq

    echo
    echo
    echo "##########################" | f_indent_old
    echo "# Setup /etc/letsencrypt #" | f_indent_old
    echo "# -> CertBot layout      #" | f_indent_old
    echo "##########################" | f_indent_old
    echo

    if [[ ! -d "${ELE_DIR_PROD}" ]]; then
        echo "${p2}${SUDO} mkdir -p '${ELE_DIR_PROD}'"
        ${SUDO} mkdir -p "${ELE_DIR_PROD}"
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "   - Error, command fails, rc: $rc"
            return 1
        fi
    fi

    echo "${p2}${SUDO} chown root:root '${ELE_DIR_PROD}'"
    ${SUDO} chown root:root "${ELE_DIR_PROD}"
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        return 1
    fi

    echo "${p2}${SUDO} chmod 711 '${ELE_DIR_PROD}'"
    ${SUDO} chmod 711 "${ELE_DIR_PROD}"
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        return 1
    fi

    echo "-$ cd '${ELE_DIR_PROD}'"
    cd "${ELE_DIR_PROD}"
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        return 1
    fi

    if [[ ! -d "./LEGO/" ]]; then
        echo "${p2}${SUDO} mkdir -p './LEGO/'"
        ${SUDO} mkdir -p "./LEGO/"
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "   - Error, command fails, rc: $rc"
            return 1
        fi
    fi

    echo "${p2}${SUDO} chmod 711 './LEGO/'"
    ${SUDO} chmod 711 "./LEGO/"
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        return 1
    fi

    if [[ ! -L "./UMI" ]]; then
        echo "${p2}${SUDO} ln -s ./LEGO UMI"
        ${SUDO} ln -s ./LEGO UMI
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "   - Error, command fails, rc: $rc"
            return 1
        fi
    else
        qq=$(file ./UMI 2>&1)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "-$ file ./UMI"
            echo "   - Error, command fails, rc: $rc"
            echo "     ${qq}"
            return 1
        fi

        # ./UMI: symbolic link to ./live/aws...
        # ./UMI: symbolic link to ./LEGO

        if [[ $v -ge 1 ]]; then
            echo
            echo "- file UMI: '${qq}'"
        fi

        pattern="UMI: symbolic link to \./LEGO"
        if [[ "${qq}" =~ ${pattern} ]]; then
            echo
            echo "- file UMI: '${qq}'"
            echo "  -> Expected sym link already exists".
        else
            # - If UMI.orig already exists...
            orig_dir='UMI.orig'
            while [[ -e "${orig_dir}" ]]; do
                orig_dir="${orig_dir}+"
            done

            echo "${p2}${SUDO} mv UMI ${orig_dir}"
            ${SUDO} mv UMI "${orig_dir}"
            rc=$?
            if [[ $rc -ne 0 ]]; then
                echo "   - Error, command fails, rc: $rc"
                return 1
            fi

            echo "${p2}${SUDO} ln -s ./LEGO UMI"
            ${SUDO} ln -s ./LEGO UMI
            rc=$?
            if [[ $rc -ne 0 ]]; then
                echo "   - Error, command fails, rc: $rc"
                return 1
            fi
        fi
    fi

    if [[ -f ./LEGO/fullchain.pem ]]; then
        echo
        echo "${p2}${SUDO} chmod 644 './LEGO/fullchain.pem'"
        ${SUDO} chmod 644 './LEGO/fullchain.pem'
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "   - Error, command fails, rc: $rc"
            return 1
        fi
    fi

    if [[ -f ./LEGO/privkey.pem ]]; then
        echo "${p2}${SUDO} chmod 600 './LEGO/privkey.pem'"
        ${SUDO} chmod 640 './LEGO/privkey.pem'      # - Allow group read
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "   - Error, command fails, rc: $rc"
            return 1
        fi
    fi

    echo
    echo
    echo "${p2}${SUDO} ls -ldhF \${ELE_DIR_PROD}"
    out=$(${SUDO} ls -ldhF "${ELE_DIR_PROD}" 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        echo "${out}"
        return 1
    fi
    echo "${out}" | f_deslash | f_indent_old

    echo
    echo "${p2}${SUDO} ls -lhF"
    out=$(${SUDO} ls -lhF 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        echo "${out}"
        return 1
    fi
    echo "${out}" | f_indent_old

    echo
    echo "${p2}${SUDO} ls -lhF ./LEGO/"
    out=$(${SUDO} ls -lhF ./LEGO/ 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
        echo "${out}"
        return 1
    fi
    echo "${out}" | f_indent_old

    return 0
}


    ###############
    # Validations #
    ###############
    #
    # - This setup is tailored for Zonomi DNS provider.
    #   Other providers can be readily added.
    #
    # - LetsEncrypt service needs an email address, eg:
    #   $ export LEGO_EMAIL='john@...'
    #
    # - LEGO needs a DNS provider, eg:
    #   $ export LEGO_DNS_PROVIDER=zonomi
    #
    # - LEGO indexes your cert requests with a "main domain" used as a file name
    #   and supports optional additional domains as SANs.
    #   "LEGO run" needs all the domains, but "LEGO renew" uses only $LEGO_DOMAIN. Eg:
    #   $ export LEGO_DOMAIN=john.example.com
    #   $ export LEGO_DOMAIN_SANS='joe.example.com,*.john.example.com'  # - Optional
    #
    # - DNS providers typically need an API key, which is provider-specific. Eg:
    #   $ export ZONOMI_API_KEY='123...89'
    #
    # - LetsEncrypt, by default, will not renew a cert with more than 30 days
    #   to go before expiration. Override this with an optional setting. Eg:
    #   $ export LEGO_EXPIRATION_DAYS=31

function f_confirm_lego_parameters() {
    local mini="$1"         # - If true, only check for LEGO_DOMAIN
    local rc=0
    local out which

    if [[ -z "${mini}" ]]; then
            mini=0      # - False
    else
            mini=1      # - True
    fi

    if [[ -z "${LEGO_DOMAIN}" ]]; then
        echo
        echo "- Error, variable not set: \$LEGO_DOMAIN"
        echo "  eg:  $ export LEGO_DOMAIN=john.example.com"
        echo
        return 1
    fi
    if [[ "${LEGO_DOMAIN}" =~ , ]]; then
        echo
        echo "- Error, \$LEGO_DOMAIN is not a list, and should not have a comma."
        echo "  LEGO_DOMAIN: '${LEGO_DOMAIN}'"
        echo
        return 1
    fi

    if [[ $mini -ge 1 ]]; then
        # - All mini checks succeeded.
        return 0
    fi

    which=$(which lego 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo
        echo "- Error, lego not found. Try this:"
        echo "  $ sudo apt install lego"
        echo
        return 1
    fi

    if [[ -z "${LEGO_EMAIL}" ]]; then
        echo
        echo "- Error, variable not set: \$LEGO_EMAIL"
        echo "  eg:  $ export LEGO_EMAIL=john@..."
        echo
        return 1
    fi

    if [[ -z "${LEGO_DOMAIN_SANS}" ]]; then
        # - This parameter is optional.
        true

    fi

    if [[ -z "${LEGO_EXPIRATION_DAYS}" ]]; then
        # - This parameter is optional.
        true

    fi

    if [[ -z "${LEGO_DNS_PROVIDER}" ]]; then
        echo
        echo "- Error, variable not set: \$LEGO_DNS_PROVIDER"
        echo "  eg:  $ export LEGO_DNS_PROVIDER=zonomi"
        echo "  https://go-acme.github.io/lego/dns/"
        echo
        return 1
    fi

    case "$LEGO_DNS_PROVIDER" in
        zonomi)
            if [[ -z "${ZONOMI_API_KEY}" || "${ZONOMI_API_KEY}" == 'no-key' ]]; then
                echo
                echo "- Error, variable not set: \$ZONOMI_API_KEY"
                echo "  eg:  $ export ZONOMI_API_KEY=123...89"
                echo
                echo
                return 1
            fi

            # - Extra parameters, as needed. Eg, timeouts.
            #   -> https://github.com/go-acme/lego/issues/967
            #   -> https://go-acme.github.io/lego/dns/desec/
            #   -> Caution, timeout tolerence can make the process slower.
            #   -> This may or may not help, depending on API implementation.
            #
            if [[ $v -ge 1 ]]; then
                echo
                echo "-$ export DESEC_PROPAGATION_TIMEOUT=600"
                echo "-$ export NAMESILO_PROPAGATION_TIMEOUT=3600"
                echo "-$ export NAMESILO_POLLING_INTERVAL=120"
                echo "-$ export NAMESILO_TTL=3600"
            fi
            export DESEC_PROPAGATION_TIMEOUT=600
            export NAMESILO_PROPAGATION_TIMEOUT=3600
            export NAMESILO_POLLING_INTERVAL=120
            export NAMESILO_TTL=3600
            ;;

        *)
            echo
            echo "- Error, only these DNS providers are supported at this time:"
            echo "  zonomi"
            echo
            return 1
            ;;
    esac

    return 0
}


    ####################
    # Lego run & renew #
    ####################

# - Convert a list of comma-separated (or space-separated) strings, to a list of parameters. Eg:
#   LEGO_DOMAIN_SANS='*.john.example.com'                   ->  "--domains '*.john.example.com'"
#   LEGO_DOMAIN_SANS='joe.example.com, *.john.example.com'  ->  "--domains 'joe.example.com' --domains '*.john.example.com'"
#   LEGO_DOMAIN_SANS='joe.example.com  *.john.example.com'  ->  "--domains 'joe.example.com' --domains '*.john.example.com'"
#   LEGO_DOMAIN_SANS=''                                     ->  ""

function f_domain_unwrap() {
    local str="$1"
    local str2

    if [[ -z "${str}" ]]; then
        return 0
    fi

    str2=$(echo "${str}" | perl -ne '
        my @spl = split(" ", $_);       # - Space-separated, as with Ansible
        for my $i (@spl) {
            my @spl2 = split(",", $i);  # - Comma-separated, often with unquoted CLI
            for my $j (@spl2) {
                print "--domains $j ";
            }
        }
        exit 0;
      '
    )
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "- f_domain_unwrap(): Error"
        return 1
    fi

    printf "%s" "${str2}" | tr -d '\n'
    return 0
}


# - Get initial certs
# - When testing, beware of LetsEncrypt limits, eg:
#   -$ lego ...  --domains 'aws.example.com'  ...  run
#      2024/03/22 15:48:15 Could not obtain certificates:
#        acme: error: 429 :: POST ::
#           https://acme-v02.api.letsencrypt.org/acme/new-order :: urn:ietf:params:acme:error:rateLimited :: 
#        Error creating new order ::
#           too many certificates (5) already issued for this exact set of domains in the last 168 hours:
#        https://letsencrypt.org/docs/duplicate-certificate-limit/

function f_lego_run() {
    local san_string=''
    local rc=0

    if [[ -n "${LEGO_DOMAIN_SANS}" ]]; then
        san_string=$(f_domain_unwrap "${LEGO_DOMAIN_SANS}" 2>&1)
        rc=$?
        if [[ $rc -ne 0 ]]; then
            echo "${san_string}"
            return 1
        fi
    fi

    echo
    echo
    echo "################" | f_indent_old
    echo "# Lego Run     #" | f_indent_old
    echo "# -> New certs #" | f_indent_old
    echo "################" | f_indent_old
    echo

    echo "-$ lego  --accept-tos  --dns ${LEGO_DNS_PROVIDER}  --domains '${LEGO_DOMAIN}'  ${san_string}  --email ${LEGO_EMAIL}  --path '${ELE_DIR_DEV}'  --cert.timeout 120  --dns-timeout 120  run"

    lego  --accept-tos  --dns "${LEGO_DNS_PROVIDER}"  --domains "${LEGO_DOMAIN}"  ${san_string}  --email "${LEGO_EMAIL}"  --path "${ELE_DIR_DEV}"  --cert.timeout 120  --dns-timeout 120  run  2>&1 | f_indent_old
    rc=${PIPESTATUS[0]}
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, rc: $rc"
        echo
        return 1
    fi

    return 0
}


# - Renew an existing cert
#   - ignore $LEGO_DOMAIN_SANS

function f_lego_renew() {
    local rc=0
    local expires=''
    local out pattern

    if [[ -n "${LEGO_EXPIRATION_DAYS}" ]]; then
        expires="--days ${LEGO_EXPIRATION_DAYS}"
    fi

    echo
    echo
    echo "##################" | f_indent_old
    echo "# Lego Renew     #" | f_indent_old
    echo "# -> Renew certs #" | f_indent_old
    echo "##################" | f_indent_old
    echo

    echo "-$ lego  --accept-tos  --dns ${LEGO_DNS_PROVIDER}  --domains '${LEGO_DOMAIN}'  --email ${LEGO_EMAIL}  --path '${ELE_DIR_DEV}' --cert.timeout 120  --dns-timeout 120  renew  ${expires}"

    ## lego  --accept-tos  --dns "${LEGO_DNS_PROVIDER}"  --domains "${LEGO_DOMAIN}"  --email "${LEGO_EMAIL}"  --path "${ELE_DIR_DEV}"  renew  ${expires}  2>&1 | f_indent_old
    ## rc=${PIPESTATUS[0]}

    out=$(lego  --accept-tos  --dns "${LEGO_DNS_PROVIDER}"  --domains "${LEGO_DOMAIN}"  --email "${LEGO_EMAIL}"  --path "${ELE_DIR_DEV}" --cert.timeout 120  --dns-timeout 120  renew  ${expires}  2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "${out}"
        echo "   - Error, rc: $rc"
        echo
        return 1
    fi
    echo "${out}"

    # - Lego does not set a non-zero return code if renewal is denied. :-(
    #   So we do it ourself.
    #
    #   -$ lego  --accept-tos  --dns zonomi  --domains 'ocean...'  --email ...  --path ...   renew  
    #      2024/03/27 16:04:15 [ocean...] The certificate expires in 82 days, the number of days defined to perform the renewal is 30: no renewal.

    pattern='the number of days defined to perform the renewal is .+: no renewal.'
    if [[ "${out}" =~ ${pattern} ]]; then
        return 3
    fi

    return 0
}


    #################################
    # Copy certs from DEV into PROD #
    #################################
    #
    # - Do this after acquiring and confirming the cert.
    #   A name change is needed, from LEGO convention to CertBot convention.
    #
    #   ${ELE_DEV}/certificates/${LEGO_DOMAIN}.crt  ->  ${ELE_PROD}/LEGO/fullchain.pem
    #   ${ELE_DEV}/certificates/${LEGO_DOMAIN}.key  ->  ${ELE_PROD}/LEGO/privkey.pem
    #
    # - For historical compatibility, the "active cert" used by my NGINX web servers
    #   assume that the real certs are in this sym link:  ${ELE_PROD}/UMI/...
    #   This is due to quirks of the older CertBot script.
    #
    # - Both LEGO and CertBot produce a fullchain cert (all intermediates) and private key.
    #   CertBot also writes an end-chain cert file (cert.pem) and a "chain file" (chain.pem).
    #   For compatibility, these two additional files are synthesized: f_split_fullchain()
    #
    #   $ cat /etc/letsencrypt/live/README
    #       This directory contains your keys and certificates.
    #       `privkey.pem`  : the private key for your certificate.
    #       `fullchain.pem`: the certificate file used in most server software.
    #       `chain.pem`    : used for OCSP stapling in Nginx >=1.3.7.
    #       `cert.pem`     : will break many server configurations, and should not be used
    #                        without reading further documentation (see link below).
    #       WARNING: DO NOT MOVE OR RENAME THESE FILES!
    #                Certbot expects these files to remain in this location in order
    #                to function properly!

function f_split_fullchain() {
    local rc=0

    echo
    echo "${p2}${SUDO} perl -nle '...'"
    echo "   <- IN:  ${ELE_DIR_PROD}/LEGO/fullchain.pem" | f_deslash
    echo "   -> OUT: ${ELE_DIR_PROD}/LEGO/cert.pem"      | f_deslash
    echo "   -> OUT: ${ELE_DIR_PROD}/LEGO/chain.pem"     | f_deslash

    ${SUDO} perl -e '
        my $seen  = 0;
        my $count = 0;
        my $fullchain_file  = "'${ELE_DIR_PROD}/LEGO/fullchain.pem'";
        my $cert_file       = "'${ELE_DIR_PROD}/LEGO/cert.pem'";
        my $chain_file      = "'${ELE_DIR_PROD}/LEGO/chain.pem'";

        open (INF, "<", "$fullchain_file") ||
            die "Cannot open fullchain_file $fullchain_file for reading.";

        while (<INF>) {
            chomp;

            if (/^\s*-+BEGIN CERTIFICATE-+\s*$/) {
                $count++;
                if ($count == 1) {
                    open (OUTF, ">", "$cert_file") ||
                        die "Cannot open cert_file $cert_file for writing.";
                    $seen = 1;
                    print OUTF "$_\n";

                } elsif ($count > 1) {
                    if ($count == 2) {
                        open (OUTF, ">", "$chain_file") ||
                            die "Cannot open chain_file $chain_file for writing.";
                    }
                    $seen = 1;
                    print OUTF "$_\n";
                }
                next;

            } elsif ($seen and /^\s*-+END CERTIFICATE-+\s*$/) {
                print OUTF "$_\n";
                $seen = 0;
                if ($count == 1) {
                    close OUTF;
                }
                next;

            } elsif ($seen) {
                print OUTF "$_\n";

            } else {
                next;
            }
        }

        close INF;
        close OUTF;  # - Duplicate close is OK in Perl.
    ' 
    rc=$?

    if [[ $rc -ne 0 ]]; then
        echo "  - Error,  perl rc: $rc"
        echo
        return 1
    fi
    
    return 0
}


function f_copy() {
    local rc=0
    local pem_file

    echo
    echo
    echo "########" | f_indent_old
    echo "# Copy #" | f_indent_old
    echo "########" | f_indent_old

    if [[ ! -f ${ELE_DIR_DEV}/certificates/${LEGO_DOMAIN}.crt ]]; then
        echo
        echo "- Error, LEGO certificate file does not exist:"
        echo "  \${ELE_DIR_DEV}/certificates/\${LEGO_DOMAIN}.crt"
        echo "  ${ELE_DIR_DEV}/certificates/${LEGO_DOMAIN}.crt"
        echo
        return 1
    fi

    if [[ ! -f ${ELE_DIR_DEV}/certificates/${LEGO_DOMAIN}.key ]]; then
        echo
        echo "- Error, LEGO private key file does not exist:"
        echo "  \${ELE_DIR_DEV}/certificates/\${LEGO_DOMAIN}.key"
        echo "  ${ELE_DIR_DEV}/certificates/${LEGO_DOMAIN}.key"
        echo
        return 1
    fi

    echo
    echo "${p2}${SUDO} cp  \${ELE_DIR_DEV}/certificates/\${LEGO_DOMAIN}.crt   \${ELE_DIR_PROD}/LEGO/fullchain.pem"
    pem_file="${ELE_DIR_PROD}/LEGO/fullchain.pem"
    echo "   -> ${pem_file}" | f_deslash
    ${SUDO} cp  "${ELE_DIR_DEV}/certificates/${LEGO_DOMAIN}.crt"  "${ELE_DIR_PROD}/LEGO/fullchain.pem"
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, rc: $rc"
        return 1
    fi

    echo
    echo "${p2}${SUDO} cp  \${ELE_DIR_DEV}/certificates/\${LEGO_DOMAIN}.key   \${ELE_DIR_PROD}/LEGO/privkey.pem"
    pem_file="${ELE_DIR_PROD}/LEGO/privkey.pem"
    echo "   -> ${pem_file}" | f_deslash
    ${SUDO} cp  "${ELE_DIR_DEV}/certificates/${LEGO_DOMAIN}.key"  "${ELE_DIR_PROD}/LEGO/privkey.pem"
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, rc: $rc"
        return 1
    fi

    echo
    echo "##############" | f_indent_old
    echo "# Synthesize #" | f_indent_old
    echo "##############" | f_indent_old

    f_split_fullchain
    rc=$?
    if [[ $rc -ne 0 ]]; then
        return 1
    fi


    echo
    echo "###############" | f_indent_old
    echo "# Permissions #" | f_indent_old
    echo "###############" | f_indent_old

    # - Skip errors in chmod.

    echo
    echo "${p2}${SUDO} chmod 600 \${ELE_DIR_PROD}/LEGO/privkey.pem"
    ${SUDO} chmod 600 "${ELE_DIR_PROD}/LEGO/privkey.pem"  2>&1 | f_deslash | f_indent_old
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
    fi

    echo "${p2}${SUDO} chmod 644 \${ELE_DIR_PROD}/LEGO/fullchain.pem"
    ${SUDO} chmod 644 "${ELE_DIR_PROD}/LEGO/fullchain.pem"  2>&1 | f_deslash | f_indent_old
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
    fi

    echo "${p2}${SUDO} chmod 644 \${ELE_DIR_PROD}/LEGO/cert.pem"
    ${SUDO} chmod 644 "${ELE_DIR_PROD}/LEGO/cert.pem"  2>&1 | f_deslash | f_indent_old
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
    fi

    echo "${p2}${SUDO} chmod 644 \${ELE_DIR_PROD}/LEGO/chain.pem"
    ${SUDO} chmod 644 "${ELE_DIR_PROD}/LEGO/chain.pem"  2>&1 | f_deslash | f_indent_old
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "   - Error, command fails, rc: $rc"
    fi

    return 0
}


    ##############
    # Show certs #
    ##############

function f_show_certs() {
    local rc=0  rc_0=0  rc_1=0  rc_2=0
    local file files pem_file
    local summary

    echo
    echo
    echo "##############" | f_indent_old
    echo "# Show certs #" | f_indent_old
    echo "##############" | f_indent_old

    f_load_bashrc       # - Script exit on failure.

    if [[ $v -ge 1 ]]; then
        summary='summary3'
    else
        summary='summary'
    fi

    # - Directory  ${ELE_DIR_DEV}/certificates/  may contain multiple certs.
    #   The environment variable  LEGO_DOMAIN  is not required here,
    #   so all certs shown here, skipping "issuing certs".
    #
    # - Skipped:
    #   echo "-$ cat \${ELE_DIR_DEV}/certificates/\${LEGO_DOMAIN}.crt | f_cert_grep - | f_zero | f_indent_old"
    #
    # - Explicit sudo is not used here.

    files=''
    if [[ ! -d "${ELE_DIR_DEV}/certificates/" ]]; then 
        echo
        echo "- Missing directory: \${ELE_DIR_DEV}/certificates/"
        echo "  -> ${ELE_DIR_DEV}/certificates/" | f_deslash
    else
        files=$(/bin/ls ${ELE_DIR_DEV}/certificates/ 2> /dev/null | egrep '\.crt$' | grep -v issuer)
        if [[ -z "${files}" ]]; then
            echo
            echo "- No certificate files found:  \${ELE_DIR_DEV}/certificates/*.crt | grep -v issuer"
            echo "  -> ${ELE_DIR_DEV}/certificates/" | f_deslash
        fi
    fi

    for file in ${files}; do

        # - Only show the DEV dir certs in verbose mode.
        if [[ $v -eq 0 ]]; then
            continue
        fi

        echo
        echo "-$ cat \${ELE_DIR_DEV}/certificates/${file} | f_cert_grep - ${summary} | f_zero | f_indent_old"
        pem_file="${ELE_DIR_DEV}/certificates/${file}"
        echo "   -> ${pem_file}" | f_deslash

        if [[ -f "${pem_file}" ]]; then
            cat "${pem_file}" | f_cert_grep - ${summary} | f_zero | f_indent_old
            rc_0=${PIPESTATUS[0]}  rc_1=${PIPESTATUS[1]}  rc_2=${PIPESTATUS[2]}
            if [[ $rc_0 -ne 0 || $rc_1 -ne 0 || $rc_2 -ne 0 ]]; then
                echo "   - Error, rc_0: $rc_0, rc_1: $rc_1, rc_2: $rc_2"
                return 1
            fi
        else
            echo "   -> File not found."
        fi
    done


    # - Conditionally use sudo, for the PROD directory.
    echo
    echo "${p2}${SUDO} cat \${ELE_DIR_PROD}/UMI/fullchain.pem | f_cert_grep - ${summary} | f_zero | f_indent_old"
    pem_file="${ELE_DIR_PROD}/UMI/fullchain.pem"
    echo "   -> ${pem_file}" | f_deslash

    # - This construct fails if the file exists but there's no permission into the directory to check:
    #   $ if [[ -f "${pem_file}" ]]; then ...
    #
    ${SUDO} test -f "${pem_file}"
    rc=$?
    if [[ $rc -eq 0 ]]; then
        ${SUDO} cat "${pem_file}" | f_cert_grep - ${summary} | f_zero | f_indent_old
        rc_0=${PIPESTATUS[0]}  rc_1=${PIPESTATUS[1]}  rc_2=${PIPESTATUS[2]}
        if [[ $rc_0 -ne 0 || $rc_1 -ne 0 || $rc_2 -ne 0 ]]; then
            echo "   - Error, rc_0: $rc_0, rc_1: $rc_1, rc_2: $rc_2"
            return 1
        fi
    else
        echo "   -> File not found."
    fi

    return 0
}


    ########
    # Main #
    ########

echo "lego-certs.sh : v${lego_certs_zonomi_sh_version}"

if [[ -z "$1" ]]; then
    echo "- Error, no parameters. Use --help for more info."
    echo
    exit 1
fi

if [[ "$1" == '-h' || "$1" == '--help' ]]; then
    f_usage
    exit 1
fi

f_context_stamp

for arg in "$@"; do
    case "${arg}" in

        -h | --help)
            f_usage
            exit 1
            ;;

        -v)
            # - Enable verbose mode
            v=1
            ;;

        diag | diags)
            f_diags
            ;;

        copy)
            f_confirm_lego_parameters mini
            rc=$?
            if [[ $rc -ne 0 ]]; then
                exit $rc
            fi

            if [[ $ELE_USE_SUDO -eq 1 ]]; then
                f_confirm_sudo
                rc=$?
                if [[ $rc -ne 0 ]]; then
                    exit $rc
                fi
            fi

            f_copy
            rc=$?
            if [[ $rc -ne 0 ]]; then
                exit $rc
            fi
            ;;

        renew)
            f_confirm_lego_parameters
            rc=$?
            if [[ $rc -ne 0 ]]; then
                exit $rc
            fi

            f_lego_renew
            rc=$?
            if [[ $rc -ne 0 ]]; then
                exit $rc
            fi
            ;;

        run)
            f_confirm_lego_parameters
            rc=$?
            if [[ $rc -ne 0 ]]; then
                exit $rc
            fi

            f_lego_run
            rc=$?
            if [[ $rc -ne 0 ]]; then
                exit $rc
            fi
            ;;

        sel)
            if [[ $ELE_USE_SUDO -eq 1 ]]; then
                f_confirm_sudo
                rc=$?
                if [[ $rc -ne 0 ]]; then
                    exit $rc
                fi
            fi

            f_setup_etc_letsencrypt
            rc=$?
            if [[ $rc -ne 0 ]]; then
                exit $rc
            fi
            ;;

        show)
            f_show_certs
            rc=$?
            if [[ $rc -ne 0 ]]; then
                exit $rc
            fi
            ;;

        *)
            echo
            echo "- Error, no such parameter: '${arg}'"
            echo "  Use --help for more info."
            echo
            exit 1
            ;;
    esac
done


echo
exit 0


