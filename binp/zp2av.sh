#!/usr/bin/env bash

# - ap2av.sh : zPass to Ansible Vault

zp2av_sh_version='1.2.0'


    ############
    # Defaults #
    ############

# - Verbose mode. Cautions:
#   Verbose == 0 reveals no credentials. (safe)
#   Verbose == 1 reveals blurred credentials. (safe)
#   Verbose >= 2 reveals full credentials. (unsafe)

v=0
if [[ -n "${VERBOSE}" && "${VERBOSE}" -ge 1 ]]; then
    v="${VERBOSE}"
fi

if [[ -z "${ZP2APV_ZPASS_FILE}" ]]; then
    export ZP2APV_ZPASS_FILE=~/.zPass.LEGO
fi
if [[ -z "${ZP2APV_VAULT_FILE}" ]]; then
    export ZP2APV_VAULT_FILE=~/.vaults/vault-4.yml
fi
if [[ -z "${ZP2APV_VAULT_PASSWORD_FILE}" ]]; then
    export ZP2APV_VAULT_PASSWORD_FILE=~/.vaults/password-4.txt
fi
if [[ -z "${ZP2APV_VAULT_IDENTITY}" ]]; then
    export ZP2APV_VAULT_IDENTITY='legoVaultId'
fi


    #########
    # Usage #
    #########

function f_usage() {
    local bn=$(basename $0)

    cat <<END

- zPass to Ansible Vault

- Extract Bash variables declarations from a "Bash password file", eg: ~/.zPass*
    and write these into an Ansible Vault file, eg: ~/.vaults/vault-4.yml
    The vault file is completely overwritten, not updated.
    An asssociated Ansible Vault "password file" is also required, eg: ~/.vaults/password-4.txt
    \$ANSIBLE_VAULT_IDENTITY_LIST is unset and not used.

Usage:
    $ $bn [-v] [--help]  [-d | --defaults] | zPass_file  av_file  av_password_file  av_identity

Examples:
    $ $bn  --defaults
    $ $bn  \${ZP2APV_ZPASS_FILE}  \${ZP2APV_VAULT_FILE}  \${ZP2APV_VAULT_PASSWORD_FILE}  \${ZP2APV_VAULT_IDENTITY}

    $ $bn  '${ZP2APV_ZPASS_FILE}'  '${ZP2APV_VAULT_FILE}'  '${ZP2APV_VAULT_PASSWORD_FILE}'  '${ZP2APV_VAULT_IDENTITY}'

END

    return 1
}


    #############
    # Utilities #
    #############

# - Indent the output, line by line in realtime

function f_indent() {
    sed -e "s/^/   /;"
    return 0
}


# - Blur a string, to partially (mostly) hide a credential,
#   but still giving a hint for manageability.
#   -> Show only the first 3 characters.
#
#   $ echo hello | f_blur
#       hel...

function f_blur() {
    local lines="$1"
    local line out 

    if [[ -z "${lines}" || "${lines}" == '-' ]]; then
        lines=$(cat)     # - Slurp STDIN. Function "hangs" until given input with EOF.
    fi  

    echo "${lines}" | perl -ple '
        next if /^\s*$/;
        s`^(\s*\S.{2}).*$`$1...`;
    '   

    return 0
}


# - Convert double-slash to single-slash in pathnames,
#   This is for printing aesthetics in Linux, which accepts multiple slashes as one.
#       (But this is not necessarily true elsewhere, eg: Amazon AWS S3 bucket objects.)
#
#   $ echo 'top/playbooks//05_a.apt-upgradable-list.yml' | f_deslash
#   -> top/playbooks/05_a.apt-upgradable-list.yml
#
#   $ echo nix | f_deslash
#   nix
#
#   $ f_deslash file
#   -> not useful, the function just "hangs" in an STDIN read.

function f_deslash() {
    sed  -e 's/\/\//\//g;'
}


# - Remove a trailing slash:
#
#   $ echo '/etc//letsencrypt/ | f_deslash2
#   -> /etc/letsencrypt

function f_deslash2() {
    sed  -e 's/\/$//g;'
}


# - Show run context.
#   Just assume it works.

f_context_stamp() {
    local qq qq2
    local which

    echo
    echo "-$ date; whoami; hostname -f"
    (date; whoami; hostname -f) | f_indent

    return 0
}


    ###############
    # Validations #
    ###############

function f_confirm() {
    local rc=0
    local out which

    if [[ $v -ge 1 ]]; then
        echo
        echo "- f_confirm ( verbose=$v ):"
    fi

    if [[ $v -ge 1 ]]; then echo "    $ ansible-vault"; fi
    which=$(which ansible-vault 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo
        echo "- Error, ansible-vault not found in PATH."
        echo
        return 1
    fi

    if [[ $v -ge 1 ]]; then echo "    $ perl"; fi
    which=$(which perl 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo
        echo "- Error, perl not found in PATH."
        echo
        return 1
    fi

    if [[ $v -ge 1 ]]; then echo "    $ yq"; fi
    which=$(which yq 2>&1)
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo
        echo "- Error, yq not found in PATH."
        echo
        return 1
    fi

    if [[ $v -ge 1 ]]; then echo "  ZP2APV_ZPASS_FILE"; fi
    if [[ ! -f "${ZP2APV_ZPASS_FILE}" ]]; then
        echo
        echo "- Error, ZP2APV_ZPASS_FILE not found: '${ZP2APV_ZPASS_FILE}'"
        echo
        exit 1
    fi

    if [[ $v -ge 1 ]]; then echo "  ZP2APV_VAULT_FILE"; fi    
    if [[ ! -f "${ZP2APV_VAULT_FILE}" ]]; then
        echo
        echo "- Error, ZP2APV_VAULT_FILE not found: '${ZP2APV_VAULT_FILE}'"
        echo
        exit 1
    fi

    if [[ $v -ge 1 ]]; then echo "  ZP2APV_VAULT_PASSWORD_FILE"; fi
    if [[ ! -f "${ZP2APV_VAULT_PASSWORD_FILE}" ]]; then
        echo
        echo "- Error, ZP2APV_VAULT_PASSWORD_FILE not found: '${ZP2APV_VAULT_PASSWORD_FILE}'"
        echo
        exit 1
    fi

    if [[ $v -ge 1 ]]; then echo "  ZP2APV_VAULT_IDENTITY"; fi
    # - Just accept it.

    return 0
}


    #########
    # Do it #
    #########

function f_do_it() {
    local tmp_file
    local vault_id
    local zPass_vars  zPass_vars_sorted
    local rc=0 rc2=0
    local line out
    local key value value_b

    tmp_file="${ZP2APV_VAULT_FILE}".tmp
    vault_id=""

    zPass_vars=$( cat "${ZP2APV_ZPASS_FILE}" | perl -nle '
        chomp;
        next if /^\s*$/;
        next if /^\s*#/;
        next unless /=/;
        s/^\s+//g;
        s/^export\s*//;

        s/"//g;         # - Remove double-quotes
        s/\x27//g;      # - Remove single-quotes

        if ( /([^=]+=\S+)/ ) {
            my $var = $1;
            if ($var !~ /^ansv_/) {
                $var = "ansv_". $var;       # - Add prefix.
            }
            print "$var";
        } 
    ' 2>&1 )
    rc=$?

    if [[ $rc -ne 0 ]]; then
        echo
        echo "f_do_it(): Perl fails. rc: $rc"
        echo " ${zPass_vars}"
        echo
        return 1
    fi
    if [[ $v -ge 2 ]]; then
        # - Caution: Reveals full credentials.
        echo
        echo "- f_do_it(): zPass_vars:"
        echo "${zPass_vars}" | sort --ignore-case | f_indent
    fi


    # - Sorted list
    # - Avoid this construct, which creates a Bash subshell and hence no return variables:
    #   $ echo "${zPass_vars}" | sort --ignore-case | while read line; do
    #
    zPass_vars_sorted=$( echo "${zPass_vars}" | sort --ignore-case )


    echo
    echo "- f_do_it()"
    echo "  $ unset ANSIBLE_VAULT_IDENTITY_LIST"
    unset ANSIBLE_VAULT_IDENTITY_LIST


    # - Write the Ansible Vault TEMPORARY file.

    if [[ $v -ge 1 ]]; then
        echo "$ touch     ${tmp_file}" | f_indent
        echo "$ chmod 600 ${tmp_file}" | f_indent
    fi

    touch "${tmp_file}"
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo
        echo "- Error, command fails: rc: $rc"
        echo "  -> $ touch '${tmp_file}'"
        echo
        return 1
    fi

    chmod 600 "${tmp_file}"
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo
        echo "- Error, command fails: rc: $rc"
        echo "  -> $ chmod 600 '${tmp_file}'"
        echo
        return 1
    fi

    echo
    printf "# Ansible Vault YAML\n\n---\n"  > "${tmp_file}"
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo
        echo "f_do_it(): printf fails, rc: $rc"
        echo
        return 1
    fi


    # - Encrypt

    for line in ${zPass_vars_sorted}; do
        ## echo "- <${line}>"

        key=$(     echo "${line}"  | cut -f1    -d= )
        value=$(   echo "${line}"  | cut -f2-99 -d= )
        value_b=$( echo "${value}" | f_blur         )

        # - Construct an Ansible Vault "vault-id" structure, with selector and password.
        #   This is necessary when ANSIBLE_VAULT_IDENTITY_LIST is unset.
        #   The "password file" can also be an API script to access another vault.
        #
        vault_id="${ZP2APV_VAULT_IDENTITY}@${ZP2APV_VAULT_PASSWORD_FILE}"

        # - FYI: If a password item is not provide here,
        #   ansible-vault reads from /dev/tty to get it,
        #   which hangs this script.

        if [[ $v -eq 0 ]]; then
            echo "  - ${key}"
        elif [[ $v -eq 1 ]]; then
            # - Caution: Reveals blurred credential.
            printf "$ ansible-vault  encrypt_string %s  --name %-20s  --vault-id %s\n"  "'${value_b}'"  "'${key}'"  "'${vault_id}'" | f_indent
        else
            # - Caution: Reveals a full credential.
            echo "$ ansible-vault  encrypt_string '${value}'  --name '${key}'  --vault-id '${vault_id}'" | f_indent
        fi

        out=$( ansible-vault  encrypt_string  "${value}"  --name "${key}"  --vault-id "${vault_id}"  2>&1  < /dev/null )
        rc=$?
    
        if [[ $rc -ne 0 ]]; then
            echo
            echo "f_do_it(): ansible-vault fails, rc: $rc"
            echo
            echo "  ${out}"
            echo
            echo
            echo "- Temporary file is preserved for review, remove when it done:"
            echo "  ${tmp_file}"
            echo
            return 1
        fi
        printf "\n%s\n"  "${out}"  >> "${tmp_file}"

    done
    echo >> "${tmp_file}"

    ## echo
    ## echo "$ ls -lhF ${tmp_file}"
    ## ls -lhF "${tmp_file}"
    ## echo


    # - Decrypt, as a sanity test.

    if [[ $v -ge 1 ]]; then
        echo
        echo "- f_do_it(): Sanity test decryptions"
    fi

    for line in ${zPass_vars_sorted}; do
        ## echo "- <${line}>"

        key=$(     echo "${line}"  | cut -f1    -d= )
        value=$(   echo "${line}"  | cut -f2-99 -d= )
        value_b=$( echo "${value}" | f_blur         )

        out=$( yq ".${key}" "${tmp_file}" | ansible-vault  decrypt --vault-id "${vault_id}" 2>&1 )
        rc=$?

        if [[ $rc -ge 1 ]]; then
            echo
        fi
        if [[ $rc -ge 1 || $v -ge 1 ]]; then
            printf "$ yq  %-20s '${tmp_file}' | ansible-vault  decrypt --vault-id '${vault_id}'\n"  "'.${key}'" | f_indent
        fi

        if [[ $rc -ne 0 ]]; then
            echo
            echo "f_do_it(): ansible-vault fails, rc: $rc"
            echo
            echo "  ${out}"
            echo
            echo
            echo "- Temporary file is preserved for review, remove when it done:"
            echo "  ${tmp_file}"
            echo
            return 1
        fi

    done


    # - Replace the Ansible Vault file.

    echo
    echo "  $ mv '${tmp_file}' '${ZP2APV_VAULT_FILE}'"
    mv "${tmp_file}" "${ZP2APV_VAULT_FILE}"
    rc=$?

    if [[ $rc -ne 0 ]]; then
        echo "- Error, command fails, rc: $rc"
        echo
        return 1
    fi

    return 0
}


    ########
    # Main #
    ########

echo "zp2av.sh : v${zp2av_sh_version}"

if [[ -z "$1" ]]; then
    echo "- Error, no parameters. Use --help for more info,"
    echo "      or use 'do_it' to run with defaults."
    echo
    exit 1
fi


if [[ "$1" == '-h' || "$1" == '--help' ]]; then
    f_usage
    exit 1
fi

f_context_stamp

argc=0
seen_doit=0

for arg in "$@"; do
    case "${arg}" in

        -h | --help)
            f_usage
            exit 1
            ;;

        -v)
            # - Enable verbose mode
            (( v += 1 ))
            ;;

        -d | --doit | --do_it | --default | --defaults)
            seen_doit=1
            ;;

        *)
            if [[ ${arg:0:1} == "-" ]]; then
                echo
                echo "- Error, invalid option: '${arg}'"
                echo
                exit 1
            fi

            (( argc += 1 ))
            case "${argc}" in
                1) ZP2APV_ZPASS_FILE="${arg}" ;;
                2) ZP2APV_VAULT_FILE="${arg}" ;;
                3) ZP2APV_VAULT_PASSWORD_FILE="${arg}" ;;
                4) ZP2APV_VAULT_IDENTITY="${arg}" ;;
                *)  echo
                    echo "- Error, extra parameter: '${arg}'"
                    echo
                    exit 1
                ;;
            esac
            ;;
    esac
done


# - Any / enough parameters given?
if [[ $seen_doit != 1 && $argc -eq 0 ]]; then
    echo
    echo "- Error, insufficient parameters."
    echo "  Use --help for more info."
    echo
    exit 1
fi


# - Validate:
f_confirm
rc=$?
if [[ $rc -ne 0 ]]; then
    exit $rc
fi

echo
echo "- Parameters:"
echo "  - zPass file:                   '${ZP2APV_ZPASS_FILE}'"
echo "  - Ansible Vault YAML file:      '${ZP2APV_VAULT_FILE}'"
echo "  - Ansible Vault password file:  '${ZP2APV_VAULT_PASSWORD_FILE}'"
echo "  - Ansible Vault identity:       '${ZP2APV_VAULT_IDENTITY}'"


# - Create Ansible Vault:
f_do_it
rc=$?
if [[ $rc -ne 0 ]]; then
    exit $rc
fi


echo
exit 0


