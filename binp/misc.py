
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - Utilities


# - "print no newline"
def print_nonl (*args, **kwargs):
    for item in args:
        print (item, end='')


def u_str (item):
    try:
        uni = str (item)            # - Prone to decode crashes, but still worth a try.
    except (UnicodeEncodeError, UnicodeDecodeError) as ex1:
        ## printx ("- DIAG: u_str(): type: " + str(type(item)))         # - Reliable ??
        ## printx ("- DIAG: u_str(): >>> item='" + item + "' <<<")      # - Reliable ??
        ## printx ("- DIAG: exception 1: " + str (ex1))                 # - Hope this works.

        try:
            uni = unicode (item)                # - How about this?
            ## uni = unicode (item, 'utf-8')    # - maybe this is better??

        except (UnicodeEncodeError, UnicodeDecodeError) as ex2:
            # Abandon ship.
            printx ("- Error in u_str(): ")
            printx ("  exception 1: " + str (ex1))          # - Hope this works.
            printx ("  exception 2: " + str (ex2))          # - Hope this works.
            printx ("  item type: " + str(type(item)))      # - Reliable ??
            printx ("  item: '" + item + "'")               # - Reliable ??
            raise

    return uni


# - Generic structure diagnostic
#   https://stackoverflow.com/questions/2540567/is-there-a-python-equivalent-to-perls-datadumper#2540660

def printStruct (struc, indent=0, annotate=False, name='', stream=0):

    def printStruct2 (struc, indent=0, annotate=False):

        if isinstance (struc, dict):
            if annotate:
                print_nonl ("= dict" + "\n", stream=stream)

            print_nonl ('  '*indent+'{' + "\n", stream=stream)
            for key,val in sorted (struc.iteritems()):
                if isinstance (val, (dict, list, tuple)):
                    print_nonl ('  '*(indent+1) + u_str (key) + ' => ', stream=stream)

                    printStruct2 (val, indent+2)
                else:
                    print_nonl ('  '*(indent+1) + u_str (key) + ' => ' + u_str (val) + "\n", stream=stream)
            print_nonl ('  '*indent+'}' + "\n", stream=stream)

        elif isinstance (struc, list):
            if annotate:
                print_nonl ("= list" + "\n", stream=stream)

            print_nonl ('  '*indent + '[' + "\n", stream=stream)
            for item in struc:
                printStruct2 (item, indent+1)

            print_nonl ('  '*indent + ']' + "\n", stream=stream)

        elif isinstance (struc, tuple):
            if annotate:
                print_nonl ("= tuple" + "\n", stream=stream)
            print_nonl ('  '*indent + '(' + "\n", stream=stream)

            for item in struc:
                printStruct2 (item, indent+1)
            print_nonl ('  '*indent + ')' + "\n", stream=stream)

        else:
            if annotate:
                print_nonl ("= other: type=" + u_str (type (struc)) + "\n", stream=stream)
            print_nonl ('  '*indent + u_str (struc) + "\n", stream=stream)
        return

    if name:
        print_nonl ("<" + "\n" + "==vv==  printStruct (" + name + ")  ==vv==" + "\n", stream=stream)

    printStruct2 (struc, indent=indent, annotate=annotate)

    if name:
        print_nonl ("==^^==  printStruct  ==^^==" + "\n" + ">" + "\n", stream=stream)

    return



