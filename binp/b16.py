#!/usr/bin/env python3

# - Quickie converter to/from base-16, also showing string/bytes conversion.
#   https://docs.python.org/3/library/base64.html
#   https://docs.python.org/3/library/binascii.html#module-binascii
#   https://stackoverflow.com/questions/7585435/best-way-to-convert-string-to-bytes-in-python-3


__version__ = '1.0.0'

import argparse
import base64
import binascii
import contextlib
import sys

entire_file = False
double_space = False


###########
# Methods #
###########

# - Print to STDERR.
# - FYI: to print without a newline:
#   printE ("Hello: ", end='')
#   printE ("")     # - This prints the ending newline.

def printE (*args, **kwargs):
    print (*args, file=sys.stderr, **kwargs)
    return


def parseArgs():

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=" " +
        "Demo: Base16 encoding / decoding.\n" +
        "  This is more complicated than expected, due to I/O with bytes not strings,\n" +
        "  which means some interfacing for use as a simple STDIN / STDOUT filter.\n" +
        "\n" +
        " Examples:\n" +
        "   $ b16.py -e       /etc/hosts          /tmp/hosts-16\n" +
        "   $ b16.py -e  -  < /etc/hosts        > /tmp/hosts-16\n" +
        "   $ cat /etc/hosts |  b16.py    -     > /tmp/hosts-16\n" +
        "   $ cat /etc/hosts |  b16.py -e -     > /tmp/hosts-16\n" +
        " \n" +
        "   $ b16.py -d       /tmp/hosts-16       /tmp/hosts\n" +
        "   $ b16.py -d  -  < /tmp/hosts-16     > /tmp/hosts\n" +
        "   $ cat /tmp/hosts-16 |  b16.py -d -  > /tmp/hosts\n" +
        " \n" +
        "   $ b16.py -e -ef   /etc/hosts          /tmp/hosts-16\n" +
        "   $ b16.py -d -ef   /tmp/hosts-16       /tmp/hosts\n" +
        " \n")


    parser.add_argument(
        '-ef', '--entire-file',
        action='store_true',
        default=False,
        help='Encode/Decode the entire file. Default is False, process line-by-line.')

    parser.add_argument(
        '-dsp', '--double-space',
        action='store_true',
        default=False,
        help='In line-by-line mode, double-space the encoded output with an extra newline.')


    parser.add_argument(
        '-e', '--encode',
        action='store_true',
        default=True,
        dest='encode',
        help='Base-16 encode. (default)')

    parser.add_argument(
        '-d', '--decode',
        action='store_false',
        dest='encode',
        help='Base-16 encode. (default)')


    parser.add_argument(
        '-V', '--version',
        action='version',
        help='show script version',
        version="%(prog)s (v"+__version__+")")

    parser.add_argument(
        '-v', '--verbose',
        action='count',
        default=0,
        help="verbose level... repeat up to three times.")

    parser.add_argument(
        'files',
        nargs=argparse.REMAINDER)

    args = parser.parse_args()  ## (sys.argv[1:])
    return args



# - Wrappers to open a file, or fake it for STDIN / STDOUT.
# - https://stackoverflow.com/questions/17602878/how-to-handle-both-with-open-and-sys-stdout-nicely/75196027#75196027
#   ## s = sys.stdin.read()             # - FYI: Read string
#   ## s = sys.stdin.buffer.read()      # - FYI: Read bytes

def reader(fn):
    @contextlib.contextmanager
    def stdin():
        yield sys.stdin
    if (not fn) or (fn == '-'):
        return stdin()
    else:
        return open(fn, 'r')

def writer(fn):
    @contextlib.contextmanager
    def stdout():
        yield sys.stdout
    if (not fn) or (fn == '-'):
        return stdout()
    else:
        return open(fn, 'w')


def main():

    args = parseArgs()

    if args.files and (len(args.files) == 1) and (args.files[0] == '-'):
        input  = '-'
        output = '-'
    elif args.files and (len(args.files) == 2):
        input  = args.files[0]
        output = args.files[1]
    else:
        printE ("- Error, zero or two files required.")
        printE ("")
        sys.exit(1)


    with reader(input) as fr:
        with writer(output) as fw:

            if args.entire_file:
                # - Encode the entire file at once
                s = fr.read()
                s2 = s.encode()                 # - string to bytes

                if args.encode:
                    x = base64.b16encode(s2)    # - bytes required, not string
                else:
                    x = base64.b16decode(s2)    # - bytes required, not string

                x2 = x.decode()                 # - bytes to string
                print (x2, file=fw)             # - fw.write (x2)

            else:
                # - Encode Line-by-line
                lines = fr.readlines()
                for i,line in enumerate(lines):

                    if args.encode:
                        s2 = line.encode()
                        x = base64.b16encode(s2)
                        x2 = x.decode()         # - bytes to string

                    else:
                        line = line.strip()
                        if not line:
                            continue
                        s2 = line.encode()      # - string to bytes
                        try:
                            x = base64.b16decode(s2)    # - bytes required, not string
                        except binascii.Error as e:
                            # binascii.Error
                            printE (f"- Line {i+1}: {str(e)}")
                            print (f"-- {str(e)}: {s2}", file=fw)
                            continue
                        x2 = x.decode()         # - bytes to string
                        x2 = x2.strip()

                    print (x2, file=fw)         # - fw.write (x2)
                    if args.encode and args.double_space:
                        print ("", file=fw)
    return 0



########
# Main #
########

if __name__ == "__main__":
    rc = 0
    try:
        rc = main()
    except KeyboardInterrupt:
        print ("\n" + "- Keyboard Interrupt")

    sys.exit (rc)


