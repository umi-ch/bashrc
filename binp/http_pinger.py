#!/usr/bin/env python3
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - Wrapper for HTTP fetches in a loop, with control parameters.

import datetime
import inspect
import json
import re
import requests
import sys

from time import gmtime, sleep, strftime


http_pinger_version = '8.0.0'

url = "http://localhost:80"     # - Same as: localhost
iterations = 1
pause = 2.0
timeout = 5.0


def usage():
    global http_pinger_version

    msg = f'''
http_pinger.py v{http_pinger_version}
- Continuously fetch web content from a URL, useful for testing firewalls etc.

Usage:
  http_pinger.py  [-h|--help]  url  [iterations]  [pause]  [timeout]

Examples:
  http_pinger  localhost:80              1    2    5    # - Default.
  http_pinger  http://172.16.180.2:5080  100  0.5  2
  http_pinger  https://ocean.umi.ch
  http_pinger  https://www.ethz.ch
  http_pinger  http://github.com                        # - Shows redirects.
    '''
    print (msg)
    return False


def get_args():
    global url, iterations, pause, timeout
    argc =  len(sys.argv) - 1

    if argc == 0:
        usage()
        return False

    if (argc == 1) and (sys.argv[1] in ['-h', '--help']):
        usage()
        return False

    if argc >= 1 and sys.argv[1]:
        url = sys.argv[1]

        # - Fix up a servername into a real URL ?
        if not re.match (r'^https?://', url, re.IGNORECASE):
            url = "http://" + url

    if argc >= 2 and sys.argv[2]:
        iterations = int (sys.argv[2])

    if argc >= 3 and sys.argv[3]:
        pause = float (sys.argv[3])

    if argc >= 4 and sys.argv[4]:
        timeout = float (sys.argv[4])

    print (
        "\n- http_pinger.py  v" + http_pinger_version,
        "\n  url:           ", url,
        "\n  iterations:    ", iterations,
        "\n  sleep:         ", pause, "seconds",
        "\n  timeout:       ", timeout, "seconds")
    return True


# - HTTP module - with error handling.
#   Plenty can go wrong, but we don't want to quit the mission on each error!
#
# - Tips for exception handling:
#   https://stackoverflow.com/questions/16511337/correct-way-to-try-except-using-python-requests-module
#   https://github.com/kennethreitz/requests/blob/master/requests/exceptions.py

def get_data (url):
    global timeout
    ex = ""
    msgs = []
    rc = 990

    headers = {
        # https://stackoverflow.com/questions/10606133/sending-user-agent-using-requests-library-in-python
        'user-agent': (
            "custom-agent: http_pinger.py " + http_pinger_version
            ## 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5)'
            ## 'AppleWebKit/537.36 (KHTML, like Gecko)'
            ## 'Chrome/45.0.2454.101 Safari/537.36'
        ),
        ## 'referer': 'http://example.com/'
    }

    try:
        r = requests.get (
            url,
            headers = headers,
            timeout = timeout,
            allow_redirects = False,
        )
        rc = r.status_code

    except requests.exceptions.Timeout as e:
        rc = 992
        ex = get_full_class_name(e)
        r = e
        if hasattr(e.args[0], 'reason'):
            reason = e.args[0].reason
        else:
            reason = e.args[0]
        msgs.append (str(reason))

    except requests.exceptions.ConnectionError as e:
        rc = 993
        ex = get_full_class_name(e)
        r = e
        if hasattr(e, 'args'):
            if hasattr(e.args[0], 'reason'):
                reason = e.args[0].reason
                msgs.append (str(reason))
            else:
                reason = e.args[0]
                msgs.append (str(reason))
        else:
            pass    ## reason = (e])

    except (IOError, OSError) as e:
        # https://docs.python.org/3/library/exceptions.html
        rc = 991
        ex = get_full_class_name(e)
        r = e
        if hasattr(e.args[0], 'reason'):
            reason = e.args[0].reason
        else:
            reason = e.args[0]
        reason = e.args[0]
        msgs.append (str(reason))

    except Exception as e:
        rc = 999
        ex = get_full_class_name(e)
        r = e


    # - Show redirects:
    #   https://stackoverflow.com/questions/20475552/python-requests-library-redirect-new-url
    #
    if (rc >= 300) and (rc <= 399):
        msgs.append ("original: " + url)

        if r.url != url:
            msgs.append ("redirect: " + r.url)

        if r.headers and r.headers['Location']:
            msgs.append ("redirect: " + r.headers['Location'])

    return [rc, r, msgs, ex]


# - Main work here.

def main():
    global url, iterations, pause

    if not get_args():
        return False

    my_grep = re.compile (r'<title>.*?</title>', re.MULTILINE)

    for i in range (1, iterations+1):
        # - For compatibilty wiht Python 3.5.2, avoid modern constructs: f'{i:>03}'
        print ("\n" + '{:>03d}'.format(i) + " : " + strftime("%H:%M:%S", gmtime()) )

        [rc, response, msgs, ex] = get_data(url)
        print("  rc: " + '{:>03d}'.format(rc))

        if rc < 990:
            # - Normal HTTP status codes.
            data = response.text

            #  <!DOCTYPE html><html><body>
            #  <title>vlan-361</title>
            #  <br/>
            #  <b>Hello World</b> : vlan-361
            #  </body></html>

            titles = my_grep.findall (data)
            if titles:
                ## [print("  " + i) for i in titles]
                # - Add spaces, for improved readability:
                for title in titles:
                    title = re.sub (r'(<title>\s*)(.*?)(\s*</title>)', r'\1 \2 \3', title)
                    print("  " + title)
            else:
                print ("  -> no <title>")

            if msgs:
                for msg in msgs:
                    print ("  -> " + msg)

        else:
            # - The are my internal error codes, for exceptions.
            print ("  -> " + ex)
            if msgs:
                for msg in msgs:
                    # - Remove the object prefix, if any.
                    #   <urllib3.connection.HTTPConnection object at 0x7faa0b8a85b0>: ...error...
                    #   (<urllib3.connection.HTTPConnection object at 0x7fe57f6ea0a0>, '...error...')
                    #
                    msg = re.sub (r'^\s*(\S.*?)\s*$', r'\1', msg)                       # - Trim spaces
                    msg = re.sub (r'^\s*\(?<.*?>[:,]\s*(\S.*?)\)?\s*$', r'\1', msg)     # - Remove object name
                    msg = re.sub (r'^["\'](.*?)["\']$', r'\1', msg)                     # - Remove quotes
                    print ("    ", msg)
            else:
                print ("    ", str(response))

        if i < iterations:
            sleep (pause)

    print()
    return True


# - Diagnostics:

# - Get full name of an exception:
#   https://stackoverflow.com/questions/18176602/how-to-get-the-name-of-an-exception-that-was-caught-in-python
#
def get_full_class_name(obj):
    module = obj.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return obj.__class__.__name__
    return module + '.' + obj.__class__.__name__


# - Quick introspection:
#   https://stackoverflow.com/questions/2540567/is-there-a-python-equivalent-to-perls-datadumper-for-inspecting-data-structur#2540660
#
def printStruct(struc, indent=0):

   if isinstance(struc, dict):
     print ("= dict")
     print ('  '*indent+'{')
     for key,val in struc.items():
       if isinstance(val, (dict, list, tuple)):
         print ('  '*(indent+1) + str(key) + '=> ')
         printStruct(val, indent+2)
       else:
         print ('  '*(indent+1) + str(key) + '=> ' + str(val))
     print ('  '*indent+'}')

   elif isinstance(struc, list):
     print ("= list")
     print ('  '*indent + '[')
     for item in struc:
       printStruct(item, indent+1)
     print ('  '*indent + ']')

   elif isinstance(struc, tuple):
     print ("= tuple")
     print ('  '*indent + '(')
     for item in struc:
       printStruct(item, indent+1)
     print ('  '*indent + ')')

   else:
     print ("= other: type=" + str (type (struc)))
     print ('  '*indent + str(struc))



    ########
    # Main #
    ########

if __name__ == "__main__":
    good = True
    try:
        good = main()

    except KeyboardInterrupt:
        print ("\n" + "- Keyboard Interrupt")

    if good:
        sys.exit (0)
    else:
        sys.exit (1)


