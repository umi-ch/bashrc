# bashrc_401
# Copyright (c) 2024, Mountain Informatik GmbH. All rights reserved.

# - EJBCA stuff, on Amazon AWS

v_bashrc_401='1.1.2'


###############
# Bash prompt #
###############

# - Append a prompt suffix, if root. Eg:
#   ejb_CA$
#   ejb_CA_R$
#
# - Primary determination by hostname, eg in /etc/hostname

if   [[ "$PC" == 'ejbca.umi.ch' ]]; then
    bashrc_prompt="ejb_CA${root_prompt_suffix}$ "

elif [[ "$PC" =~ 'ejbra.umi.ch'  ]]; then
    bashrc_prompt="ejb_RA${root_prompt_suffix}$ "

elif [[ "$PC" =~ 'ejbva.umi.ch'  ]]; then
    bashrc_prompt="ejb_VA${root_prompt_suffix}$ "

else
    # - NOOP
    true
fi


############
# Timezone #
############

# - For convenience, let's assume the Zurich timezone everywhere in this work context.
#   Your mileage may vary.
#   $ /bin/ls -lhF  /usr/share/zoneinfo/Europe/
#
export TZ=Europe/Zurich


#########
# PATHs #
#########

# - Working directory (or SymLink), based on current user home dir.
export EJBCA_DIR=~/ejbca/


#############
# SSH & SCP #
#############
#
# - Demo project. Eg:
#   $ sshj ejbca.umi.ch
#
# - Assume special SSH key is here:
#   ~/.ssh-aws2/id_ed25519

ssh_ejbca='ssh -o IdentitiesOnly=yes -i ~/.ssh-aws2/id_ed25519'
alias sshj="${ssh_ejbca}"
alias sshjs="alias | grep 'alias ssh' | grep '.ssh-aws2' | sort"

alias sshj1="${ssh_ejbca} ec2-user@ejbca.umi.ch"
alias sshj2="${ssh_ejbca} ec2-user@ejbra.umi.ch"
alias sshj3="${ssh_ejbca} ec2-user@ejbva.umi.ch"


scp_ejbca='scp -o IdentitiesOnly=yes -i ~/.ssh-aws2/id_ed25519'
alias scpj="${scp_ejbca}"
alias scpjs="alias | grep 'alias scp' | grep '.ssh-aws2' | sort"

# - These aren't so useful, except as reference examples:
alias scpj1="${scp_ejbca} ec2-user@ejbca.umi.ch:tmp/* ."
alias scpj2="${scp_ejbca} ec2-user@ejbra.umi.ch:tmp/* ."
alias scpj3="${scp_ejbca} ec2-user@ejbva.umi.ch:tmp/* ."


