# bashrc_280
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - AWS stuff: Public

v_bashrc_280='9.1.3'


# - FYI: Show info:
#   $ aaws
#   $ aws_oz
#   $ aaws5


#################
# Credentials 1 #
#################

if [[ -r ~/.zPass ]]; then
    source ~/.zPass
fi

if [[ -r ~/.zPass.cr ]]; then
    source ~/.zPass.cr
fi


########
# PATH #
########

# - Add Git components.

if [[ -d ~/zGit.aws/ ]]; then
    # - NOOP: placeholder
    true
fi


#############
# Autostart #
#############

# - Load Conda, if present and not already loaded - and the conda module was loaded too.
#   $ i_python
#
# - FYI: If startup script sets 'CONDA_EXE=disable', Conda will not load here.
#   If Conda is already loaded, ${CONDA_EXE} points to the Python loader script.

type load_conda > /dev/null 2>&1
rc=$?
if [[ $rc -eq 0 ]]; then    # - Found.
    if   [[ -z "${CONDA_EXE}" && -d ~/mambaforge/ ]]; then
        load_conda silent
    elif [[ -z "${CONDA_EXE}" && -d ~/miniconda3/ ]]; then
        load_conda silent
    fi
fi



    ###########
    # Regions #
    ###########

# - Set / show the current region.
#   https://stackoverflow.com/questions/31331788/using-aws-cli-what-is-best-way-to-determine-the-current-region

function f_aws_region() {
    local region="$1"

    if [[ -n "${region}" ]]; then
        if [[ 1 -eq 2 ]]; then
            false
        elif [[ "${region}" == '1' ]]; then
            export AWS_DEFAULT_REGION='eu-central-1'    # - Frankfurt
        elif [[ "${region}" == '2' ]]; then
            export AWS_DEFAULT_REGION='eu-central-2'    # - Zurich
        elif [[ "${region}" == 'u' ]]; then
            export AWS_DEFAULT_REGION='us-east-1'       # - N.Virginia, aka Global
        elif [[ "${region}" == 'i' ]]; then
            export AWS_DEFAULT_REGION='us-west-1'       # - Ireland
        else
            export AWS_DEFAULT_REGION="$region"
        fi
        echo "AWS_DEFAULT_REGION: '${AWS_DEFAULT_REGION}'"
    fi

    if [[ -z "${region}" ]]; then           ## && -n "${AWS_PROFILE}" ]]; then
        ## echo
        ## echo "-$ grep region ~/.aws/config"
        ## grep region ~/.aws/config
        ## echo

        if [[ -n "${AWS_DEFAULT_REGION}" ]]; then
            echo "AWS_DEFAULT_REGION: '${AWS_DEFAULT_REGION}'"
        else
            # - Introspection of current CLI config, no permissions required:
            echo "-$ aws configure get region"
            aws configure get region 

            # - Introspection with API call, which requires permission:
            ## echo "-$ aws ec2 describe-availability-zones --output text --query 'AvailabilityZones[0].[RegionName]'"
            ## aws ec2 describe-availability-zones --output text --query 'AvailabilityZones[0].[RegionName]' | indent
        fi
    fi

    echo
    return 0
}

alias    awsr=f_aws_region
alias  awsreg=f_aws_region
alias aws_reg=f_aws_region


    #################
    # Misc settings #
    #################

# - Prefer JSON output:
export AWS_DEFAULT_OUTPUT="json"

# - Prefer no pager, unless requested. (see below)
export AWS_PAGER=''


########
# Prep #
########

# - For standalone use:
#   Ensure these constructions from bashrc_001
#
if [[ -z "${PC}" ]]; then
    export PC="$(uname -n)"

    # - Manually set the Bash prompt:
    alias prompt='PS1=${bashrc_prompt}'
fi

if [[ -z "${myBashDir}" ]]; then
    # - This can be set manually before running this script,
    #   eg in ~blabbers-tech-account/.bash_aliases:
    #
    #   $ export myBashDir=~saablabb/.zrc
    #
    export myBashDir=~/.zrc
fi


if [[ -z "${LANG}" ]]; then
    # $ aws ec2 get-console-output --instance-id i-0db15beae72d9bebf --output text | egrep -i 'cloud-init'
    # 'ascii' codec can't encode character '\u2026' in position 6161: ordinal not in range(128)
    #
    # $ export LANG='en_US.UTF-8'
    #
    # $ aws ec2 get-console-output --instance-id i-0db15beae72d9bebf --output text | egrep -i 'cloud-init'
    # ...
    #
    export LANG='en_US.UTF-8'   # - Also: C.UTF-8
fi


# - JAWS: John's AWS Scripts

if [[ -z "${MY_IP}" ]]; then
    # - AWS EC2 meta-data endpoint:
    ## https://stackoverflow.com/questions/51486405/aws-ec2-command-line-display-instance-type
    #
    export MY_IP="169.254.169.254"
fi

if [[ -z "${AWS_JAWS_DIR}" ]]; then
    for dir in \
        /bozo/                  \
        ~/zGit/aws/binp/        \
        ~/zGit.umi/aws/binp/    \
    ; do
        if [[ -d "${dir}" ]]; then
            export AWS_JAWS_DIR="${dir}"
        fi
    done

    if [[ -z "${AWS_JAWS_DIR}" ]]; then
        export AWS_JAWS_DIR="AWS_JAWS_DIR not found."
    fi
fi

#
if [[ -d "${AWS_JAWS_DIR}" ]]; then
    alias   jaws_gt='echo "-\$ source \${AWS_JAWS_DIR}/get_token.sh"; source "${AWS_JAWS_DIR}"/get_token.sh'
    alias   jaws_st='echo "-\$ source \${AWS_JAWS_DIR}/set_token.sh"; source "${AWS_JAWS_DIR}"/set_token.sh'
    alias  jaws_stv='echo "-\$ source \${AWS_JAWS_DIR}/set_token.sh v"; source "${AWS_JAWS_DIR}"/set_token.sh v'
    #
    alias  jaws_gup='echo "-\$ source \${AWS_JAWS_DIR}/get_user_permissions.sh"; source "${AWS_JAWS_DIR}"/get_user_permissions.sh'
    alias  jaws_pip='echo "-\$ \${AWS_JAWS_DIR}/get_pip.sh"; "${AWS_JAWS_DIR}"/get_pip.sh'
    alias jaws_pip2='echo "-\$ sudo -u www-data \${AWS_JAWS_DIR}/get_pip.sh"; sudo -u www-data "${AWS_JAWS_DIR}"/get_pip.sh'
    alias jaws_keys='echo "-\$ \${AWS_JAWS_DIR}/get_public_keys.sh"; "${AWS_JAWS_DIR}"/get_public_keys.sh'
    alias jaws_screds='echo "-\$ \${AWS_JAWS_DIR}/get_screds.sh"; "${AWS_JAWS_DIR}"/get_screds.sh'
fi


function i_aws_jaws() {
    cat <<END
    ###############################
    # "JAWS" : John's AWS Scripts #
    ###############################
    - v_bashrc_280: ${v_bashrc_280}

aws_$ echo \${AWS_JAWS_DIR} \${MY_IP}
$(echo "${AWS_JAWS_DIR}" | f_indent)
$(echo "${MY_IP}" | f_indent)

aws_$ alias | grep jaws_
$(alias | grep jaws_ | f_indent)

aws_$ jaws_pip
    $ whoami : jdb
    
    - instance-id:
    $ timeout 5  curl -s "-w %{stderr}\n- http_code: %{http_code}\n" -H "X-aws-ec2-metadata-token: \${METADATA_TOKEN}"  http://\${MY_IP}/latest/meta-data/instance-id
        - http_code: 200
        - rc: 0
        i-043...
    
    - public-hostname:
    $ timeout 5  curl -s "-w %{stderr}\n- http_code: %{http_code}\n" -H "X-aws-ec2-metadata-token: \${METADATA_TOKEN}"  http://\${MY_IP}/latest/meta-data/public-hostname
        - http_code: 200
        - rc: 0
        ec2-16...eu-central-2.compute.amazonaws.com

aws_$ curl http://\${MY_IP}/latest/meta-data/instance-type; echo
    t3.micro

END
}



    ######################################
    # Credentials 2: John's conveniences #
    ######################################

# - Set keys here:
#   https://us-east-1.console.aws.amazon.com/iam/home?region=eu-central-1#/security_credentials

alias aws_oz='alias | egrep "oz=|shoz|poz"'

alias   noz='echo "-\$ unset  AWS_ACCESS_KEY_ID  AWS_MY_CLI_PROFILE  AWS_PROFILE  AWS_SECRET_ACCESS_KEY  AWS_SESSION_TOKEN"; unset AWS_ACCESS_KEY_ID AWS_MY_CLI_PROFILE  AWS_PROFILE AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN'
alias noz_1='unset AWS_ACCESS_KEY_ID AWS_PROFILE AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN AWS_DEFAULT_REGION'  # - Silent, keep AWS_MY_CLI_PROFILE

# - Show the credential accounts:
alias croz='echo "-\$ egrep \"^\[\" ~/.aws/credentials"; egrep "^\[" ~/.aws/credentials'

alias   shoz='echo "-\$ env | egrep ..."; env | egrep "AWS_ACCESS_KEY_ID|AWS_PROFILE|AWS_SECRET_ACCESS_KEY|AWS_SESSION_TOKEN" | cut -f1 -d= | sort; echo "- AWS_PROFILE:" \[${AWS_PROFILE}\]'
alias shoz_1='env | egrep "AWS_ACCESS_KEY_ID|AWS_PROFILE|AWS_SECRET_ACCESS_KEY|AWS_SESSION_TOKEN" | cut -f1 -d= | sort; echo "- AWS_PROFILE:" \[${AWS_PROFILE}\]'
alias shoz_2='echo "-\$ env | egrep AWS_ | cut -f1 -d= | sort"; env | egrep AWS_ | cut -f1 -d= | sort'
alias shoz_3='echo "-\$ env | egrep AWS_ | sort"; env | egrep AWS_ | sort'
alias shoz_4='echo "-\$ env | egrep ..."; env | egrep "AWS_ACCESS_KEY_ID|AWS_PROFILE|AWS_SECRET_ACCESS_KEY|AWS_SESSION_TOKEN" | sort'


    ######################
    # Select Credentials #
    ######################

# - Set access keys from ~/.aws/Credentials
#
#   $ f_poz_list        # - Make the list, implicit in f_poz
#   $ f_poz             # - Show the list
#   $ f_poz 1           # - Activate item #1
#   $ f_poz default     # - Activate item with this name

declare -a aws_pozs_LA

function f_poz_list() {
    true
}

function f_poz() {
    local item=$1       # - Eg: empty (to get a list), or a number starting from 1, or a name.
    export AWS_MY_CLI_PROFILE="$item"
}

alias poz=f_poz

# - Revert to no session credentials, or sandbox SSO credentials.
alias pozd='noz; echo "-\$ soz"; soz; shoz'

# - Show poz aliases
alias pozs='echo; echo -$ alias | grep poz; alias | grep poz; echo'



function i_aws_poz() {
    cat <<END
    ##########################
    # Switch CLI credentials #
    ##########################
    - v_bashrc_280: ${v_bashrc_280}

# - Show accounts:
$ croz
$(croz)

# - Show aliases:
$ aws_oz

# - Tests:
$ poz1
$ shoz_4
$ aws_whoami
$ aws_s3_ls
$ aws ec2 describe-vpcs --output=json | cat

# - Sandbox FYI (2021, 2022):
#   $ poz1; shoz        # - Use the API credentials in poz1
#   $ noz; soz; shoz    # - pozz:  Revert to SSO zPass session credentials

END
}


# - Use credentials from jaws_gt (Instance Metadata token)
#   -> Now built into jaws_st.
#   $ jaws_gt
#   $ f_poz_jaws
#
#   $ aws_whoami
#   $ aws s3 ls
#
function f_poz_jaws() {
    if [[ -z "${jaws_AccessKeyId}" ]]; then
        echo "- Error, jaws_AccessKeyId is empty."
        echo "  $ jaws_gt; f_poz_jaws; aws_whoami"
        echo
        return 1
    fi
    #
    noz_1
    export AWS_ACCESS_KEY_ID="${jaws_AccessKeyId}"
    export AWS_SECRET_ACCESS_KEY="${jaws_SecretAccessKey}"
    export AWS_SESSION_TOKEN="${jaws_Token}"
}
alias pozj=f_poz_jaws


# - The auto-pager is both useful and annoying.
#   Test: aws_whoami_raw
#
alias aws_pager='echo -n "- AWS_PAGER: "; if [[ -z ${AWS_PAGER+x} ]]; then echo unset; else echo \"${AWS_PAGER}\"; fi'
alias aws_pager_off="echo \"- export AWS_PAGER=''\"; export AWS_PAGER=''"
alias aws_pager_none=aws_pager_off
alias aws_pager_less="echo \"- export AWS_PAGER='less'\"; export AWS_PAGER='less'"
alias aws_pager_cat="echo \"- export AWS_PAGER='cat'\"; export AWS_PAGER='cat'"
alias aws_pager_unset="echo \"- unset AWS_PAGER\"; unset AWS_PAGER"


    #############
    # Helpers 1 #
    #############

# - Overview
#   -> Wrap the alias in parenthesis sub-shell, so the output is easily grepped.
#
alias aaws='(echo; aaws2; echo; aaws3; echo; aaws4; echo; aaws5; echo; echo "- aws_oz"; aws_oz; echo)'
alias aaws2='echo "- aaws2"; alias | egrep "^alias aws_"'
alias aaws3='echo "- aaws3"; alias | egrep "jqa_| cj=|no_json=|pf_|jaws_|aws_j" | egrep -v "^alias aws_|aaws"'
alias aaws4='echo "- aaws4"; alias | egrep "aws_j" | egrep -v "aaws"'
alias aaws5='echo "- aaws5"; set | perl -nle '\''next unless /^([^\s=]+)=/; $a=$1; next unless ($a =~ /aws/i); print;'\'' | sort'

# - Best guess:
#
alias aws_ping='echo "-$ aws_whoami"; aws_whoami'


# - Show helpers:
#
alias aws_h='echo "-\$ aws help"; aws help'

# - Identity:
#
alias aws_whoami='echo "-$ aws sts get-caller-identity | jqx; f_aws_region"; aws sts get-caller-identity | jqx; f_aws_region'
alias aws_whoami2='echo "-$ aws sts get-caller-identity | grep Arn"; aws sts get-caller-identity | grep Arn'
alias aws_whoami_raw='echo "-$ aws sts get-caller-identity"; aws sts get-caller-identity'   # - no jq processing
#
alias aw=aws_whoami
alias aw2=aws_whoami2
alias awr=aws_whoami_raw
#
# - https://awscli.amazonaws.com/v2/documentation/api/latest/reference/sts/get-session-token.html
# - https://unix.stackexchange.com/questions/481369/how-can-we-generate-the-session-token-in-aws
#   -> "AWS_SESSION_TOKEN is not used when you have IAM User access and secret keys."
#
alias aws_sts_h='aws sts help'
alias aws_sts_gst='echo "-$ aws sts get-session-token"; aws sts get-session-token'


# - Organizations:
#   This might fail to work due to permission errors, but the query is valid:
#
alias aws_org_la='aws organizations list-accounts'


    ######
    # S3 #
    ######

if [[ -z "${AWS_S3_BUCKET}" ]]; then
    export AWS_S3_BUCKET="deslasher-56b797933240a8a0e51a40399fec9306"   # - public read-only, but does not exist.
fi


function i_aws_delete_s3_folders() {
    cat <<END

    ###############################################
    # Delete all files in an S3 bucket sub-folder #
    ###############################################
    - v_bashrc_280: ${v_bashrc_280}

# - To do this with S3: Delete all files with the sub-folder pattern.
# - In this example: Delete all CloudTrail logs outside our prefered region list.

aws_3 mount-s3 --allow-delete aws-cloudtrail-logs-umi4  ~/mnt/s3/  --profile umi4a

aws_$ date; cd ~/mnt/; find s3 -wholename '*2023/10/24' -type d  | tee ~/tmp/qdirs
    Tue Oct 24 09:31:43 CEST 2023
    s3/AWSLogs/710.../CloudTrail/ap-northeast-1/2023/10/24
    s3/AWSLogs/710.../CloudTrail/ca-central-1/2023/10/24
    ...

aws_$ pozu4a
    -$ export AWS_PROFILE="umi4-admin"
    ...

aws_$ dirs=\$(cat ~/tmp/qdirs | cut -f5 -d/ | sort -u | egrep -v 'eu-central-1|eu-central-2|eu-west-1|us-east-1')

aws_$ echo \$dirs
    ap-northeast-1 ap-northeast-2 ap-northeast-3 ...


aws_$ for qq in \$dirs; do echo; echo == \$qq; aws s3 rm s3://aws-cloudtrail-logs-umi4/  --dryrun --recursive --exclude '*' --include "*/CloudTrail/\$qq/2023/10/24/*"; done
    ...
    == us-west-2
    (dryrun) delete: s3://aws-cloudtrail-logs-umi4/AWSLogs/924.../CloudTrail/us-west-2/2023/10/24/924..._CloudTrail_us-west-2_20231024T0200Z_NcigUGUWzHysb92j.json.gz

aws_$ for qq in \$dirs; do echo; echo == \$qq; aws s3 rm s3://aws-cloudtrail-logs-umi4/  --recursive --exclude '*' --include "*/CloudTrail/\$qq/2023/10/24/*"; done
    ...
    == us-west-2
    delete: s3://aws-cloudtrail-logs-umi4/AWSLogs/924.../CloudTrail/us-west-2/2023/10/24/924..._CloudTrail_us-west-2_20231024T0200Z_NcigUGUWzHysb92j.json.gz

aws_$ find s3 -wholename '*2023/10/24' -type d  | tee ~/tmp/qdirs2
    eu-central-1
    eu-central-2
    eu-west-1
    us-east-1

END
}

    ########
    # JSON #
    ########

# - JQ helpers:
#   $ aws_sg_Lj > $QJ
#   $ cj | jq -r -S -M "${jqa_sg_Lj_1}" | head
#   $ cj | jq -c -S -M "${jqa_sg_Lj_2}" | head
#   $ cj | jq -c -S -M "${jqa_sg_Lj_2}" | no_json
#
export QJ=~/tmp.aws/qj
alias cj="cat $QJ"

alias jqx='jq -S -M .'

# - Eg:
#   aws_$ zcat s3/AWSLogs/924.../CloudTrail/ap-south-1/2023/10/21/*.gz | jqz 
#
alias jqz='jq . | egrep -i '\''eventName|userAgent|roleArn|eventTime|errorCode|eventSource'\'' | sed -e '\''s/"eventTime":/\n"eventTime":/'\'' '

alias   no_json='perl -ple '\'' s/[\[\]\{\}":,]/ /g; s/ +/ /g; '\'''    # - Remove JSON delimiters
alias no_json_2='perl -ple '\'' s/[\[\]\{\}":,]/ /g; '\'''
alias no_json_3='perl -ple '\'' s/[\[\]\{\}",]/ /g '\'''
#
alias    pf_20='perl -ne '\'' $w=20; chomp; s/^\s+//; s/\s+$//; @x=split /,/,$_; print (" "x2); for (@x) {printf "%-${w}s ", $_}; print "\n"; '\'' '
alias pf_15_nj='perl -ne '\'' $w=15; chomp; s/^\s+//; s/\s+$//; @x=split /,/,$_; print (" "x2); for (@x) {s/[\[\]\{\}":,]//g; printf "%-${w}s ", $_}; print "\n"; '\'' '
alias pf_20_nj='perl -ne '\'' $w=20; chomp; s/^\s+//; s/\s+$//; @x=split /,/,$_; print (" "x2); for (@x) {s/[\[\]\{\}":,]//g; printf "%-${w}s ", $_}; print "\n"; '\'' '
alias pf_25_nj='perl -ne '\'' $w=25; chomp; s/^\s+//; s/\s+$//; @x=split /,/,$_; print (" "x2); for (@x) {s/[\[\]\{\}":,]//g; printf "%-${w}s ", $_}; print "\n"; '\'' '
alias pf_30_nj='perl -ne '\'' $w=30; chomp; s/^\s+//; s/\s+$//; @x=split /,/,$_; print (" "x2); for (@x) {s/[\[\]\{\}":,]//g; printf "%-${w}s ", $_}; print "\n"; '\'' '
alias pf_35_nj='perl -ne '\'' $w=35; chomp; s/^\s+//; s/\s+$//; @x=split /,/,$_; print (" "x2); for (@x) {s/[\[\]\{\}":,]//g; printf "%-${w}s ", $_}; print "\n"; '\'' '
alias pf_40_nj='perl -ne '\'' $w=40; chomp; s/^\s+//; s/\s+$//; @x=split /,/,$_; print (" "x2); for (@x) {s/[\[\]\{\}":,]//g; printf "%-${w}s ", $_}; print "\n"; '\'' '
alias pf_45_nj='perl -ne '\'' $w=45; chomp; s/^\s+//; s/\s+$//; @x=split /,/,$_; print (" "x2); for (@x) {s/[\[\]\{\}":,]//g; printf "%-${w}s ", $_}; print "\n"; '\'' '

# - JSON parsers: jqa_ == "jq for AWS"
#
jqa_sg_Lj_1='.SecurityGroups[] | (.GroupId, ("  " + .GroupName))'
jqa_sg_Lj_2='.SecurityGroups[] | { (.GroupId) : .GroupName}'
jqa_sg_Lj_3='.SecurityGroups[] | { (.GroupId) : .Description}'

jqa_backups_jobs_1='.BackupJobs[] | [.ResourceType,  .State, .PercentDone + "%", .CompletionDate, .BackupJobId, "bytes: " + (.BackupSizeInBytes|tostring)]'
jqa_backups_plans_1='.'


jqa_ec2_summary='.Reservations[].Instances[] | [.InstanceId, .InstanceType, .IamInstanceProfile.Arn, .PublicIpAddress, .PrivateIpAddress, .KeyName, (.BlockDeviceMappings[]? | .Ebs.VolumeId), .VpcId, .Tags]'
#
jqa_ec2_DI_1='.Reservations[].Instances[] | { (.InstanceId) : .KeyName}'
jqa_ec2_DI_2='.Reservations[].Instances[] | [.InstanceId, .PublicIpAddress, .PrivateIpAddress, .KeyName]'
jqa_ec2_DI_3='.Reservations[].Instances[] | [.InstanceId, (.Tags[]? | select(.Key == "Name") | .Value)]'
#
jqa_ec2_DKP_1='.KeyPairs[] | {(.KeyPairId) : .KeyName}'
#
jqa_ec2_EIP_1='.Addresses[] | [.PublicIp, .PrivateIpAddress, .InstanceId, [.Tags[] | .Value]] | flatten'
jqa_ec2_EIP_2='.Addresses[] | [.AllocationId, .AssociationId, .PublicIp, .PrivateIpAddress, .InstanceId, [.Tags[] | .Value]] | flatten'
#
jqa_ec2_IPA_1='.IamInstanceProfileAssociations[] | [.InstanceId, .AssociationId, .IamInstanceProfile.Arn]'
#
jqa_ec2_OPS_1='.Reservations[].Instances[] | [.InstanceId, .State, (.Tags[] | .Value)]'
#
jqa_ec2_PIP_1='.Reservations[].Instances[] | .PublicIpAddress'
#
jqa_ec2_TAGS_1='.Reservations[].Instances[] | .Tags'
#
jqa_ec2_VOL_1='.Volumes[] | [.VolumeId, .State, [.Attachments[]? | .InstanceId], [.Tags[]? | select(.Key == "Name") | .Value]] | flatten'
#
jqa_ec2_VPC_1='.Vpcs[] | [.VpcId, .CidrBlock, (.Tags[]? | select (.Key == "Name") | .Value) ] | flatten'


jqa_iam_IP_1='.InstanceProfiles[] | [.InstanceProfileId, .InstanceProfileName]'
jqa_iam_LU_1='.Users[] | [.UserName, .CreateDate, .PasswordLastUsed]'


    #######
    # API #
    #######

# - Overview
#   https://docs.aws.amazon.com/AWSEC2/latest/APIReference/Welcome.html

# - AWS API work is not readily done with Curl.  :-(
#   https://stackoverflow.com/questions/60986947/how-to-perform-a-simple-aws-ec2-rest-api-call-with-curl#62504735

# - Python access:
#   https://realpython.com/python-boto3-aws-s3/
#   https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html
#   https://github.com/awsdocs/aws-doc-sdk-examples/tree/main/python/example_code

# - Python samples:
#   $ mkdir -p ~/zGit/aws/
#   $ cd       ~/zGit/aws/
#   $ git clone https://github.com/awsdocs/aws-doc-sdk-examples.git
#   $ load_conda
#   $ pip install boto3
#   $ pip install pytest
#   $ cd ~/zGit/aws/aws-doc-sdk-examples/python/example_code/ec2/
#   $ python -m pytest
#       ... === 40 passed in 0.66s === ...
#   $ python ec2/ec2_basics_demo.py  2>&1 | tee ~/tmp.aws/qq.sdk:f
#       ...
#   $ grep 'INFO: Created' ~/tmp.aws/qq.sdk
#       ...

# - API output is in XML, not JSON. :-(
#   https://stackoverflow.com/questions/16090869/how-to-pretty-print-xml-from-the-command-line
#   ubuntu$ sudo apt-get install libxml2-utils      # - Gives xmllint
#   $ brew install xmlformat                     # - ToDo: test this.
#
alias xmx=xmx_1
alias xmx_1='tidy -xml -iq'
alias xmx_2='xmllint --format -'

# - Regions:
#   https://docs.aws.amazon.com/general/latest/gr/ec2-service.html
#   ec2.eu-central-1.amazonaws.com

## export AWS_URL_EC2="https://ec2.${AWS_DEFAULT_REGION}.amazonaws.com"        # - IPv4 Service Endpoint
export AWS_URL_EC2="https://ec2.amazonaws.com"        # - IPv4 Service Endpoint
C_CTAJ='Content-type: application/json'
C_CHCe='-w %{stderr}- http_code: %{http_code}\n'

# - Curl examples:
#
alias aws_api_1="curl -s \"${C_CHCe}\" -H \"${C_CTAJ}\" ${AWS_URL_EC2}?Action=GetEbsEncryptionByDefault | xmx"
#   <Message>The action GetEbsEncryptionByDefault is not valid for this web service.</Message>


    ################
    # AWS mount.s3 #
    ################
    #
    # - Linux only, not MacOS.

# - Show S3 mounts:
alias mgm="echo -$ mount \| grep mountpoint; mount | grep mountpoint"

# - CloudFront logs: Look for unexpected Honeypot references to the /video directory.
alias vgrep='echo "  -$" zcat \*.gz \| grep video \| perl ...; zcat *.gz | grep video | perl -nle '\''@x=split/\s+/; printf "%s  %s  %-15s  %-4s  %-20s  %s\n", @x[0],@x[1],@x[4],@x[5],@x[7],@x[8]'\'' | indent'

# - Summarize access in CloudFront logs.
#   IPv4 width: 15
#   IPv6 width: 40
#
alias cf_log='echo "  -$" zcat \*.gz \| perl ...; zcat *.gz | perl -nle '\''next if /^#/; @x=split/\s+/; printf "%s  %s  %-40s  %-4s  %s  %s\n", @x[0],@x[1],@x[4],@x[5],@x[8],@x[7]'\'' | indent'

# - S3 Bucket logs, of static websites: Review today's access.
#   $ cd ~/mnt/s3-webber/moxfo2/
#   $ s3grep
#   $ s3grep *
#   $ s3grep joe.2023-10-16-07-46-05-1788FE4F9060AD68

function f_s3grep() {
    local pattern="$@"
    local rc=0
    local sort=""
    local sorted=1      # - Flag: sort the output by timestamp?

    if [[ $sorted -ge 1 ]]; then
        # - Unfortunately, bucket logs can arrive in any order,
        #   which means we must do the sorting/ordering if we want
        #   to see the results in a time-increasing list.
        # - Ensure the sort flag matches the timestamp order.
        #
        sort="sort +2"
    else
        sort="cat"
    fi

    if [[ -z "${pattern}" ]]; then
        pattern="*$(date +%Y-%m-%d)-*"     # - isodate, eg: 2023-10-16
        echo "-$ cat ${pattern} | perl ... | ${sort}"
    fi

    # fb6b... cortez16 [16/Oct/2023:06:11:27 +0000] 85.7.123.59 - 8MQM1X8C6B098ZHC REST.GET.OBJECT robots.txt "GET /robots.txt HTTP/1.1" 403 AccessDenied 243 - 18 - "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/118.0" - RaV...4= - ECDHE-RSA-AES128-GCM-SHA256 - cortez16.s3.eu-central-2.amazonaws.com TLSv1.2 - -

    cat ${pattern} | perl -nle '
        next unless m|"GET /[^\?]|;

        my @x = split/\s+/;
        next if @x[8] eq "-";   # - Skip internal AWS operations.

        my $verb = @x[9];
        $verb =~ s/"//g;

        printf "%s  %s  %s %s %-15s  %s  %-15s %s %s %s\n", @x[1], @x[6],  @x[2], @x[3], @x[4],  @x[12], @x[13],  $verb, @x[10];
    ' | ${sort}

    rc=$?
    return $rc
}

alias s3grep=f_s3grep


function i_aws_mount_s3() {
    cat <<END

    ############
    # mount.s3 #
    ############
    - v_bashrc_280: ${v_bashrc_280}

# https://docs.aws.amazon.com/AmazonS3/latest/userguide/mountpoint.html
# https://github.com/awslabs/mountpoint-s3/blob/main/doc/SEMANTICS.md

aws_$ mkdir -p ~/mnt/s3-cf/
aws_$ aws sts get-caller-identity | grep Arn
    "Arn": "arn:aws:iam::710...:user/umi-robot"

aws_$ mount-s3  --allow-delete   webber-logs-cloudfront2  ~/mnt/s3-cf/
    bucket webber-logs-cloudfront2 is mounted at /home/jdb/mnt/s3-cf/

aws_$ mgm
    -$ mount | grep mountpoint
    mountpoint-s3 on /home/jdb/mnt/s3-cf type fuse (rw,nosuid,nodev,noatime,user_id=1001,group_id=1001,default_permissions)
    mountpoint-s3 on /home/jdb/mnt/s3-waf type fuse (rw,nosuid,nodev,noatime,user_id=1001,group_id=1001,default_permissions)
    mountpoint-s3 on /home/jdb/mnt/s3-webber type fuse (rw,nosuid,nodev,noatime,user_id=1001,group_id=1001,default_permissions)

aws_$ df -h /home/jdb/mnt/s3-cf/    # - this doesn't show these mount:  $ df -h
    Filesystem      Size  Used Avail Use% Mounted on
    mountpoint-s3      0     0     0    - /home/jdb/mnt/s3-cf

aws_$ alias cf_log mgm s3grep vgrep

# - On a Linux server: Look for Honeypot references to unpublished URL path: /video
aws_$ cd ~/mnt/s3-cf/cf.moxfo/
aws_$ vgrep
    -$ zcat *.gz | grep video | perl ...
    2023-10-14  18:57:18  188.000.00.100  GET  /video/  200

# - Observe CloudFront access to S3 buckets:
aws_$ cd ~/mnt/s3-webber/moxfo2/
aws_$ s3grep *
    ...

aws_$ cd ~/mnt/s3-webber/moxfo/
aws_$ s3grep
    -$ cat *2023-10-15-* | perl ... | sort +2
     moxfo.ch  YXBP52S8CQ0JZCYS  [15/Oct/2023:00:48:04 +0000] -   200  -   GET /index.html 
     moxfo.ch  B4GTFQB4S53T387Z  [15/Oct/2023:06:21:04 +0000] -   200  -   GET /robots.txt 

aws_$ pozu4a
    -$ export AWS_PROFILE="umi4-admin"
    ...

aws_$ mount-s3  aws-cloudtrail-logs-umi4  ~/mnt/s3/
    bucket aws-cloudtrail-logs-umi4 is mounted at /home/jdb/mnt/s3/

aws_$ umount ~/mnt/s3-cf/   # - No need to unmount.

END
}


function i_aws_log_loops() {
    cat <<END
    #############################################
    # Examples of looping through AWS JSON logs #
    #############################################
    - v_bashrc_280: ${v_bashrc_280}

aws_$ pozu4a
    -$ export AWS_PROFILE="umi4-admin"
    ...

aws_$ mount-s3  aws-config-logs-umi4      ~/mnt/s3-configs/
aws_$ mount-s3  aws-cloudtrail-logs-umi4  ~/mnt/s3-trails/

aws_$ which jq yq gron; alias | egrep 'alias jq|gron'
    ...


    # - CloudTrail logs

aws_$ cd ~/mnt/s3-trails/
aws_$ yy="s3-trails/AWSLogs/924.../CloudTrail/us-east-1/2023/10/22/924..._CloudTrail_us-east-1_20231022T1450Z_8eSQomcRVzFwV7mj.json.gz"

aws_$ zcat "$yy" | jqz
    "eventTime": "2023-10-22T14:41:12Z",
          "eventSource": "health.amazonaws.com",
          "eventName": "DescribeEventAggregates",
          "userAgent": "AWS Internal",
      
    "eventTime": "2023-10-22T14:41:39Z",
          "eventSource": "s3.amazonaws.com",
          "eventName": "ListBuckets",
          "userAgent": "[S3Console/0.4, aws-internal/3 aws-sdk-java/1.12.488 Linux/5.10.196-163.743.amzn2int.x86_64 OpenJDK_64-Bit_Server_VM/25.372-b08 java/1.8.0_372 vendor/Oracle_Corporation cfg/retry-mode/standard]",

aws_$ date; find s3-trails -wholename '*2023/10/24/*' -type f | while read qq; do x=\$(zcat \$qq | jqz | grep aws-cli); rc=\$?; if [[ \$rc -eq 0 ]]; then echo; echo \$qq; echo "\$x"; fi; done
    Tue Oct 24 07:47:53 CEST 2023

    AWSLogs/710.../CloudTrail/eu-central-2/2023/10/24/710..._CloudTrail_eu-central-2_20231024T0545Z_LgukrXsCXEnjMJTy.json.gz
          "userAgent": "aws-cli/2.13.17 Python/3.11.5 Darwin/22.6.0 exe/x86_64 prompt/off command/sts.get-caller-identity",

    AWSLogs/924.../CloudTrail/eu-west-1/2023/10/24/924..._CloudTrail_eu-west-1_20231024T0545Z_p3B3tToAa4oxyNQw.json.gz
          "userAgent": "[aws-cli/2.13.17 Python/3.11.5 Darwin/22.6.0 exe/x86_64 prompt/off command/s3api.get-bucket-location]",

    AWSLogs/924.../CloudTrail/eu-west-1/2023/10/24/924..._CloudTrail_eu-west-1_20231024T0545Z_q6twtmAHb4quUQEl.json.gz
          "userAgent": "aws-cli/2.13.17 Python/3.11.5 Darwin/22.6.0 exe/x86_64 prompt/off command/s3api.get-bucket-location",


    # - Config logs

aws_$ cd ~/mnt/s3-configs/
aws_$ xx7="s3-configs/AWSLogs/924.../Config/eu-west-1/2023/10/24/ConfigHistory/924..._Config_eu-west-1_ConfigHistory_AWS::S3::Bucket_20231024T102055Z_20231024T102055Z_1.json.gz"

aws_$ zcat \$xx7 | sed -e 's/"configurationItemCaptureTime/"_configurationItemCaptureTime/g' -e 's/ARN/zARN/g' | jqc | sed -e 's/"_configurationItemCaptureTime/\n"_configurationItemCaptureTime/g' | head -20
    "_configurationItemCaptureTime": "2023-10-25T15:06:51.256Z",
            "configRuleArn": "arn:aws:config:eu-central-2:924...:config-rule/aws-service-rule/securityhub.amazonaws.com/config-rule-5b6w1h",
            "configRuleArn": "arn:aws:config:eu-central-2:924...:config-rule/aws-service-rule/securityhub.amazonaws.com/config-rule-0qjabu",
            "configRuleArn": "arn:aws:config:eu-central-2:924...:config-rule/aws-service-rule/securityhub.amazonaws.com/config-rule-tsabh0",
            "configRuleArn": "arn:aws:config:eu-central-2:924...:config-rule/aws-service-rule/securityhub.amazonaws.com/config-rule-szm8vm",
            "configRuleArn": "arn:aws:config:eu-central-2:924...:config-rule/aws-service-rule/securityhub.amazonaws.com/config-rule-qwtxn8",
            "configRuleArn": "arn:aws:config:eu-central-2:924...:config-rule/aws-service-rule/securityhub.amazonaws.com/config-rule-6vjdur",
            "configRuleArn": "arn:aws:config:eu-central-2:924...:config-rule/aws-service-rule/securityhub.amazonaws.com/config-rule-sdq046",
            "configRuleArn": "arn:aws:config:eu-central-2:924...:config-rule/aws-service-rule/securityhub.amazonaws.com/config-rule-byt1fn",
            "configRuleArn": "arn:aws:config:eu-central-2:924...:config-rule/aws-service-rule/securityhub.amazonaws.com/config-rule-ijavs6",
            "configRuleArn": "arn:aws:config:eu-central-2:924...:config-rule/aws-service-rule/securityhub.amazonaws.com/config-rule-c2grzy",
      "configurationItemStatus": "ResourceDiscovered",
      "resourceId": "AWS::::Account/924...",
      "resourceType": "AWS::Config::ResourceCompliance",
            "resourceId": "924...",
            "resourceType": "AWS::::Account"

aws_$ zcat \$xx7 | jqcg
    configurationItemCaptureTime   : 2023-10-25T15:06:51.256Z
      awsAccountId                 : 924...
      awsRegion                    : eu-central-2
      configurationItemStatus      : ResourceDiscovered
      configurationItemVersion     : 1.3
      configurationStateId         : 1698246411256
      configurationStateMd5Hash    : 
      resourceId                   : AWS::::Account/924...
      resourceType                 : AWS::Config::ResourceCompliance

aws_$ find . -type f | grep '/2023/10/25/' | while read qq; do echo; echo == \$qq; zcat \$qq | jqcg; done

== ./AWSLogs/924.../Config/eu-central-2/2023/10/25/ConfigHistory/924..._Config_eu-central-2_ConfigHistory_AWS::Config::ConfigurationRecorder_20231025T053941Z_20231025T055955Z_1.json.gz
    configurationItemCaptureTime   : 2023-10-25T05:39:41.614Z
      awsAccountId                 : 924...
      awsRegion                    : eu-central-2
      configurationItemStatus      : ResourceDiscovered
      configurationItemVersion     : 1.3
      configurationStateId         : 1698212381614
      configurationStateMd5Hash    : ""
      resourceId                   : default
      resourceType                 : AWS::Config::ConfigurationRecorder
    ...

END
}


    ####################################
    # Pretty-print the AWS Config logs #
    ####################################

# - Extract summary from AWS Config logs, and nicify the output. Eg:
#
#   $ aws_config_grep xx7.json
#   $ zcat xx7.json.gz | aws_config_grep
#
# - Prep:
#   aws_$ sudo cpanm JSON::Parse
#   $       cpanm FJSON::Parse
#
# - JSON parse:
#   https://metacpan.org/dist/JSON-Parse/view/lib/JSON/Parse.pod
#
# - Quick slurp:
#   https://stackoverflow.com/questions/206661/what-is-the-best-way-to-slurp-a-file-into-a-string-in-perl#206682
#
#   $ perl -n0e 'print "content is in $_\n"' xx7 | cutt
#   $ cat xx7 | perl -n0e 'print "content is in $_\n"' - | cutt

function aws_config_grep() {
    local file=$1
    local rc=0

    if [[ -z "${file}" ]]; then
        file="-"
    fi

    perl -n0e '
        use strict;
        use warnings;
        no warnings "experimental::smartmatch";

        use JSON::Parse qw /parse_json/;

        use Data::Dumper;                   #  $ man Data::Dumper
        $Data::Dumper::Indent   = 1;
        $Data::Dumper::Terse    = 1;
        $Data::Dumper::Sortkeys = 1;

        my @tops    = qw/bozo configurationItems/;
        my @headers = qw/configurationItemCaptureTime/;

        my $j = parse_json($_);
        ## print Dumper $j;

        for my $top (@tops) {
            my $listR = $j->{$top};
            if (! defined $listR) {
                ## print "- undefined top: $top\n";
                next;
            } else {
                ## print "- $top\n";
            }

            for my $blurbR (@$listR) {                  # - Eg: @$listR  :"configurationItems" => [
                ## print Dumper $blurbR;

                # - Loop 1: Print desired header
                for my $item (keys %$blurbR) {
                    if ($item ~~ @headers) {            # - "smartmatch", Perl >= 5.10
                        ## print "$item\n";
                        print "\n";
                        lp (0, $item, $blurbR);
                    }
                }

                # - Loop 2: Print desired items
                for my $item (sort keys %$blurbR) {
                    if ($item ~~ @headers) {            # - "smartmatch", Perl >= 5.10
                        next;
                    }
                    ## print "  $item\n";
                    lp (2, $item, $blurbR);
                }

            }
        }

    print "\n";

    # line print: with indentation
    sub lp {
        my $indent = shift;
        my $key    = shift;
        my $refR   = shift;

        my $wide = 30;
        my $i;

        # - Start: skip non-SCALARS
        my $target = $refR->{$key};
        my $rt = ref $target;
        if ($rt ne "SCALAR" and $rt ne "") {
            return;
        }
        if ($target eq "") {
            $target = "\"\"";       # - empty string
        }

        print (" " x $indent);      # - spaces

        $i = $wide - $indent;
        ## $i = 10;                    # - skip fixed fields?
        printf "%-*s", $i, $key;

        if ($rt eq "SCALAR" or $rt eq "") {
            print " : ". $target;
        } else {
           print " : ... ". $rt;
        }
        print "\n";
    }

    ' "${file}"

    return $rc
}

alias jqcg=aws_config_grep



    #########
    # Other #
    #########

# - SSH:
#
# - Option  "-o IdentitiesOnly=yes"  ensures that ONLY the indicated key is used,
#   and not any keys already buffered in a running ssh-agent.

alias ssh_aws='ssh -o IdentitiesOnly=yes -i ~/.ssh-aws/id_ed25519'      # - SSH with AWS private key
alias scp_aws='scp -o IdentitiesOnly=yes -i ~/.ssh-aws/id_ed25519'


# - SSM (on EC2 Linux)
#
alias aws_ssm_tail='sudo tail -f  /var/log/amazon/ssm/amazon-ssm-agent.log'
alias aws_ssm_restart='sudo systemctl --no-pager restart snap.amazon-ssm-agent.amazon-ssm-agent.service'
alias aws_ssm_status='sudo systemctl --no-pager status snap.amazon-ssm-agent.amazon-ssm-agent.service'



    ################
    # Combinations #
    ################

# - Show all S3 policies.
#   FYI: Strange output from  aws s3api get-bucket-policy:
#   -> A single JSON object with tag "Policy"" and content is a quoted JSON string.
#   -> Use  --output=text  to skip this wrapper and get pure JSON of the policy.
#   -> This is mentioned here:  aws s3api get-bucket-policy help
#
function f_s3_show_policies() {
    local ok_only="$1"                  # - If set, then only show valid policies, skip errors
    local list_buckets
    local bucket buckets
    local rc
    local show=0

    if [[ -z "${ok_only}" ]]; then
        ok_only=0
    else
        ok_only=1
    fi

    echo
    echo f_s3_show_policies : $(date)
    echo "- List-buckets"
    list_buckets=$(aws s3api list-buckets)

    echo "- Buckets"
    buckets=$(echo "${list_buckets}" | jq -S -M '.Buckets[] | .Name' | no_json)

    echo "- List Policies"
    echo

    for bucket in ${buckets}; do
        policy=$(aws s3api get-bucket-policy --bucket ${bucket} --output=text 2>&1)
        rc=$?

        if [[ $rc -eq 0 || $ok_only -ne 1 ]]; then
            show=1
        else
            show=0
        fi

        if [[ $show -eq 1 ]]; then
            echo == ${bucket}
            echo

            if [[ $rc -eq 0 ]]; then
                echo "${policy}" | jq -S -M .
            else
                echo "${policy}" | guts
            fi

            echo
            echo
        fi
    done
}

alias aws_s3_show_policies=f_s3_show_policies


########
# Info #
########

function i_aws() {
    cat <<END

    ############
    # AWS Info #
    ############
    - v_bashrc_280: ${v_bashrc_280}

- CLI config info:
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html

$ aws --version
    aws-cli/2.13.17 Python/3.11.5 Darwin/22.6.0 exe/x86_64 prompt/off

aws_$ aws --version
    aws-cli/1.22.34 Python/3.10.12 Linux/6.2.0-1011-aws botocore/1.23.34

aws_$ aws help
    ...

aws_$ aws sts get-caller-identity help
    ...

aws_$ aws ec2 help | grep -i security-group
    ...

$ aaws       # - Show AWS aliases
    ...

aws_$ aws_ping
    -$ aws_whoami
    -$ aws sts get-caller-identity | jqx
    {
      "Account": "710...",
      "Arn": "arn:aws:iam::710...:user/umi-admin",
      "UserId": "AIDA2K2TPJMYSFDSOU33W"
    }

aws_$ aws_c_L
          Name                    Value             Type    Location
          ----                    -----             ----    --------
       profile                <not set>             None    None
    access_key     ****************RAVJ shared-credentials-file    
    secret_key     ****************p/Xn shared-credentials-file    
        region             eu-central-2              env    AWS_DEFAULT_REGION


# - On EC2 Ubuntu instances:
aws_$ sudo apt install awscli
aws_$ dpkg -L awscli | grep /bin/
    /usr/bin/aws
    /usr/bin/aws_completer

# - Full support now on MacOS, partial support on Linux.
$ echo; echo AWS_JAWS: $AWS_JAWS; echo; alias | grep jaws; echo

    AWS_JAWS: /Users/jdb/zGit/aws/binp

    alias aaws3='echo "- aaws3"; alias | egrep "jqa_| cj=|no_json=|pf_|jaws_|aws_j" | egrep -v "^alias aws_|aaws"'
    alias jaws_gt='echo "-\$ source \${AWS_JAWS}/get_token.sh"; source "${AWS_JAWS}"/get_token.sh'
    alias jaws_st='echo "-\$ source \${AWS_JAWS}/set_token.sh"; source "${AWS_JAWS}"/set_token.sh'
    alias jaws_gup='echo "-\$ source \${AWS_JAWS}/get_user_permissions.sh"; source "${AWS_JAWS}"/get_user_permissions.sh'
    alias jaws_keys='echo "-\$ \${AWS_JAWS}/get_public_keys.sh"; "${AWS_JAWS}"/get_public_keys.sh'
    alias jaws_pip='echo "-\$ \${AWS_JAWS}/get_pip.sh"; "${AWS_JAWS}"/get_pip.sh'
    alias jaws_pip2='echo "-\$ sudo -u www-data \${AWS_JAWS}/get_pip.sh"; sudo -u www-data "${AWS_JAWS}"/get_pip.sh'
    alias pozj='f_poz_jaws'

END
}


function i_aws_EBS_resize() {
    cat <<END

    ################################
    # Resize an AWS EC2 EBS volume #
    ################################
    - v_bashrc_280: ${v_bashrc_280}

https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/recognize-expanded-volume-linux.html
https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Volumes:search=John

aws_$ date; whoami; hostname; df -h /
    Tue Mar  9 14:00:00 CET 2019
    ubuntu
    aws
    Filesystem      Size  Used Avail Use% Mounted on
    /dev/root       7.7G  7.6G  123M  99% /

# - Extend the EBS volume on AWS EC2 volumes page: 8 Gb -> 12 Gb.
#   Then:

aws_$ lsblk
    NAME    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
    ...
    xvda    202:0    0    12G  0 disk
    └─xvda1 202:1    0     8G  0 part /

aws_$ sudo growpart /dev/xvda 1
    CHANGED: partition=1 start=2048 old: size=16775135 end=16777183 new: size=25163743 end=25165791

aws_$ lsblk
    NAME    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
    xvda    202:0    0    12G  0 disk
    └─xvda1 202:1    0    12G  0 part /

aws_$ sudo resize2fs /dev/xvda1
    resize2fs 1.45.5 (07-Jan-2020)
    Filesystem at /dev/xvda1 is mounted on /; on-line resizing required
    old_desc_blocks = 1, new_desc_blocks = 2
    The filesystem on /dev/xvda1 is now 3145467 (4k) blocks long.

aws_$ df -h /
    Filesystem      Size  Used Avail Use% Mounted on
    /dev/root        12G  7.6G  4.0G  66% /

END
}



