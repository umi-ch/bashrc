# bashrc_001
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - launcher

v_bashrc_001='10.0.0'

# - FYI:
# - Directory conventions:
#
#       ~/.zrc       ->  ~/zGit/bashrc/     # - ${myBashDir} == ~/.zrc
#       ~/.bashrc    ->  .zrc/bashrc_001    # - this launcher file
#
# - Multiple locations of bashrc files are supported, eg, from multiple repositories.
#   If needed, set  myExtraBashDirs  in a zrc.env file (see below) with extra locations.
#   Then,  ~/.bashrc-seq  can list the desired files without regard to location. Eg:
#
#   $ export myExtraBashDirs="~/.zrc2/ ~/.zrc3/"    # - put this in  ~/.zrc.env
#   $ printf "100\n800\n900\n"  > ~/.bashrc-seq     # - example
#   $ ls -l ~/.zrc/bashrc_100   # - standard bashrc file, in this distribution.
#   $ ls -l ~/.zrc2/bashrc_800  # - your site-specific file, in a separate repository.
#   $ ls -l ~/.zrc3/bashrc_900  # - your site-specific file, in yet another repository.


# - Here's an opportunity to load system-specific variables. Don't abuse it.
#   This is done here at the beginning, before  ${myBashDir}  is determined.
#
if [[ -f ~/.zrc.env ]]; then
    source ~/.zrc.env           # - with "dot prefix"
fi

if [[ -f ~/.zrc/zrc.env ]]; then
    source ~/.zrc/zrc.env       # - without "dot prefix"
fi

# - Stop processing immediately, if this is not an interactive shell. Eg: scp
#   The zrc.env files above can trigger this, by unsetting variable: $PS1
#   Beware, this can lead to an invisible prompt.
#
# - Alternately, set the flag SKIP_JOHNS_BASHRC in a  zrc.env  file above.
##
if [[ -z "$PS1" || -n "${SKIP_JOHNS_BASHRC}" ]]; then
   return 0
fi


export ME="${LOGNAME}"      # - FYI: `logname` seems deliberately broken in recent Ubuntus.
export myHome=$(bash -c "echo ~$ME")   # ~t12..
export myOS=$(uname -s)
export PC=$(uname -n)


# - Return code globals, for convenience.
rc=0
rc_0=0
rc_1=0
rc_2=0
rc_3=0
rc_4=0
rc_5=0
rc_out=''
rc_out_2=''

# - Set verbose mode:
if [[ -z "${VERBOSE}" ]]; then
    export VERBOSE=0
elif [[ "${VERBOSE}" -gt 0 ]]; then
    echo "- bashrc_001: VERBOSE=${VERBOSE}"
fi



##################################
# Find this location: $myBashDir #
##################################
#
# - This file bashrc_01 is typically source'd as a Unix "sym link" from ~/.bashrc
#   We must find it's directory location, to source related files.
#
# - On Linux, this is easy, with "GNU readlink":
#       $ echo "${BASH_SOURCE[0]}"
#       $ readlink -f "${BASH_SOURCE[0]}"
#
# - On MacOS, this is not so easy. It uses "BSD readlink", without option '-f'.
#   Also, this sym-link may be with the file itself, or the parent directory.
#       $ echo "${BASH_SOURCE[0]}"
#       $ readlink "${BASH_SOURCE[0]}"
#       $ readlink $(dirname "${BASH_SOURCE[0]}")
#
# - On GitBash (CygWin), the Linux strategy also works because sym links
#   appear as regular files.  (MS-Windows doesn't support Unix-style sym links)
#
# - On WSL 1.0, the Linux method also works.
#   Beware: Windows directory path may have a blank spaces.
#   Eg:  /mnt/c/Users/john/OneDrive - Company/work/xx
#   In Bash, be sure to quote variables with pathnames, eg: "${myBashDir}"


# - $myBashDir either doesn't exist now, in which case it's determined here,
#   or it's preset by another launcher, in which case it's used as is.

this_file=''

if [[ -z "${myBashDir}" ]]; then
    this_file="${BASH_SOURCE[0]}"           # - Eg: /Users/jdb/.bashrc
else
    this_file="${myBashDir}/bashrc_001"
fi

if [[ -z "${this_file}" ]]; then
    echo "- Error: \${this_file} is not defined: Can't determine \$myBashDir."
    echo "- this_file='${this_file}', myBashDir='${myBashDir}', \${BASH_SOURCE[0]}='${BASH_SOURCE[0]}'"
    return 1
fi


# - Determine $myBashDir, so the other files can be source'd, eg:
#   $ source ${myBashDir}/bashrc_100
#
if [[ -n "${myBashDir}" ]]; then
    true    # - NOOP

elif [[ "$myOS" == 'Darwin' ]]; then
    tmp_1="$(readlink ${this_file})"                # - Eg: '.zrc/bashrc_001'
    tmp_2="$(readlink $(dirname ${this_file}))"     # - Eg: ''
    tmp_3="$(dirname ${this_file})"                 # - Eg: '/Users/jdb'
    tmp_4="$(dirname ${tmp_3}/${tmp_1})"            # - Eg: '/Users/jdb/.zrc'
    #
    ## echo "- \$this_file == \${BASH_SOURCE[0]} == '${this_file}'"
    ## echo "- MacOS: tmp_1 == '${tmp_1}'"
    ## echo "- MacOS: tmp_2 == '${tmp_2}'"
    ## echo "- MacOS: tmp_3 == '${tmp_3}'"
    ## echo "- MacOS: tmp_4 == '${tmp_4}'"

    if [[ -d "${tmp_4}" ]]; then
        myBashDir="${tmp_4}"
        ## echo "- MacOS myBashDir 4: myBashDir == '${myBashDir}'"

    elif [[ -n "${tmp_1}" ]]; then
        myBashDir="$(dirname ${tmp_1})"
        ## echo "- MacOS myBashDir 1: myBashDir == '${myBashDir}'"

    elif [[ -n "${tmp_2}" ]]; then
        myBashDir="${tmp_2})"
        ## echo "- MacOS myBashDir 2: myBashDir == '${myBashDir}'"

    elif [[ -n "${tmp_3}" ]]; then
        myBashDir="${tmp_3}"
        ## echo "- MacOS myBashDir 2: myBashDir == '${myBashDir}'"

    else
        echo "- Error, MacOS sanity test 1 fails: directory not found: \$myBashDir"
        echo "- \${BASH_SOURCE[0]} == '${BASH_SOURCE[0]}'"
        return 1
    fi

else
    this_file_2="$(readlink -f ${this_file})"
    myBashDir="$(dirname ${this_file_2})"
    ## echo "- Generic myBashDir 3: myBashDir == '${myBashDir}'"
fi


if [[ ! -d "${myBashDir}" ]]; then
    echo "- Error, sanity test 2 fails: directory not found: \$myBashDir == '${myBashDir}'"
    return 1
elif [[ ! -f "${myBashDir}/bashrc_001" ]]; then
    echo "- Error, sanity test 3 fails: file not found: \$myBashDir/bashrc_001 == '\${myBashDir}/bashrc_001'"
    return 1
else
    true
    ## echo "- FYI: \$myBashDir == '${myBashDir}'"
fi


# - Cleanup:
export myBashDir
unset this_file
unset this_file_2
unset tmp_1
unset tmp_2
unset tmp_3



########################
# OS-specific specials #
########################
#
# - Old Solaris stuff is elsewhere.
# - FYI: Apple Mac has many BSD Unix implementations, instead of Linux GNU.

## if [[ "$myOS" == 'Darwin' ]]; then
##     ## echo "- Special Prep for MacOS""
##     source ${myBashDir}/bashrc_002
## fi

## if [[ "$OS" = "Windows_NT" ]]; then
##     ## echo "- Special Prep for MS-Windows"
##     source ${myBashDir}/bashrc_003
## fi


##################
# Default prompt #
##################
#
# - Start with a basic prompt.
# - Alias 'ty2' starts a typescript'ed session, with a predefined prompt.

bashrc_prompt_orig="${bashrc_prompt}"   # - Empty, in most cases.
root_prompt_suffix=''

if [[ $(id -u) -eq 0 ]]; then           # - Assume that $(id) exists.
    # - Got root?
    root_prompt_suffix='_R'
    bashrc_prompt='R$ '
else
    bashrc_prompt='$ '
fi


######################################
# Fetch this server's nonce, if any. #
######################################

if [[ -z "${bashrc_nonce}" ]]; then
    export bashrc_nonce=''

    if [[ -f ~/.bashrc-id ]]; then
        export bashrc_nonce="$(cat ~/.bashrc-id)"
        ## echo "- FYI: read nonce: '${bashrc_nonce}'"

    elif [[ -f "${myBashDir}/.bashrc-id" ]]; then
        export bashrc_nonce=$(cat "${myBashDir}/.bashrc-id")

    elif [[ -f "${myBashDir}/../.bashrc-id" ]]; then
        export bashrc_nonce=$(cat "${myBashDir}/../.bashrc-id")

    elif [[ -f "${myBashDir}/../../.bashrc-id" ]]; then
        export bashrc_nonce=$(cat "${myBashDir}/../../.bashrc-id")

    else
        true
        ## echo "- FYI: no nonce"
    fi
fi


##################################################
# Fetch this server's sequence list from a file? #
##################################################
#
# - Eg:
#   $ cat ~/.bashrc-seq
#       # - Comments like this, and blank lines, are ignored.
#       100     # - This right-side comment is ignored too.
#       101
#       102

bashrc_seq=''

if [[ -f ~/${myBashDir}/bashrc-seq ]]; then
    bashrc_seq=~/${myBashDir}/bashrc-seq
elif [[ -f ~/.bashrc-seq ]]; then
    bashrc_seq=~/.bashrc-seq
fi

if [[ -n ${bashrc_seq} ]]; then

    if [[ "${VERBOSE}" -gt 0 ]]; then
        echo "- FYI: read sequence file: .bashrc-seq"
    fi

    bashrc_sequences=$(
        cat ${bashrc_seq} |
        perl -ne '
            chomp;
            next if /^\s*$/;    # - Skip blank lines
            next if /^\s*#/;    # - Skip comments
            #
            s/\s+//g;           # - Skip spaces
            s/#.*$//;           # - Skip comments on the right-side
            #
            next if /^0/;       # - Skip references to "preload" bashrc files
            print "$_ ";
        ';
        exit ${PIPESTATUS[0]};  # - In case Perl does not set an error return code.
    )
    rc=$?

    if [[ $rc -ne 0 ]]; then
        echo "- Error, rc: ${rc}: Failed to read sequence file: .bashrc-seq"
        bashrc_sequences=''
    fi

    if [[ "${VERBOSE}" -gt 0 ]]; then
        echo "- FYI: done read sequence file."
        echo "  -> bashrc_sequences: ${bashrc_sequences}"
    fi

else
    true
    ## echo "- FYI: no .bashrc-seq"
fi


##################
# Helper utility #
##################
#
# - Get all subgroups, ie, expand wildcards, and make a print list.

# - Verbose example:
#   bashrc_360 [360_a.pl]x 360_b
#
#   -> files loaded:   bashrc_360, bashrc_360_b
#   -> files skipped:  bashrc_360_a.pl

bashrc_expander_f_list=''       # - Eg: bashrc_310 bashrc_310_b bashrc_310_c bashrc_310_d
bashrc_expander_p_list=''       # - Eg: bashrc_310 310_b 310_c 310_d"

function bashrc_expander() {
    local dir="$1"              # - Eg: ~/.zrc/john/
    local pattern="$2"          # - Eg: bashrc_310
    local f g files

    bashrc_expander_f_list=''
    bashrc_expander_p_list=''

    if [[ ! -f "${dir}/${pattern}" ]]; then
        return 0
    fi

    bashrc_expander_f_list="${pattern}"     # - File list
    bashrc_expander_p_list="${pattern}"     # - Print list

    files=$( cd ${dir}; /bin/ls ${pattern}* )
    for f in ${files}; do

        if [[ "${f}" == "${pattern}" ]]; then
            true    # - Already processed

        elif [[ "${f}" =~ \. ]]; then
            #
            # - Skip files with dots in the name, eg: bashrc_360_a.pl
            #   This constrains the file naming conventions.
            #
            true    # - Avoid loading other programming languages into Bash.

            if [[ -n "${VERBOSE}" && "${VERBOSE}" -gt 0 ]]; then
                g=$(echo "${f}" | sed -e "s/bashrc_//")     # bashrc_310_b -> 310_b
                bashrc_expander_p_list="${bashrc_expander_p_list} [${g}]x"
            fi

        else
            bashrc_expander_f_list="${bashrc_expander_f_list} ${f}"

            # - Show the subgroups ?
            if [[ -n "${VERBOSE}" && "${VERBOSE}" -gt 0 ]]; then
                g=$(echo "${f}" | sed -e "s/bashrc_//")     # bashrc_310_b -> 310_b
                bashrc_expander_p_list="${bashrc_expander_p_list} ${g}"
            fi
        fi
    done
    return 0
}


####################
# Gather sequences #
####################
#
# - Load the loaders, aka bootstrapping, with bashrc_boot
#
# - Wildcard sequences are implicitly loaded. Eg:
#   $ bashrc_sequences="100 101 102 300 310 311 312"
#   -> This loads bashrc_300, bashrc_310, bashrc_310_b, bashrc_310_c, ...

if [[ -n "${bashrc_sequences}" ]]; then
    if [[ "${VERBOSE}" -ge 1 ]]; then
        echo "-> bashrc_sequences: '${bashrc_sequences}'"
    fi

else
    export bashrc_sequences=''
fi

bashrc_boot="$(seq 10 99)"          # - 10..99

for i in ${bashrc_boot}; do
    num=$(printf "%03d" ${i})       # - 020
    if [[ -f "${myBashDir}/bashrc_${num}" ]]; then
        #
        bashrc_expander "${myBashDir}" "bashrc_${num}"
        if [[ -n "${bashrc_expander_p_list}" ]]; then
            echo "-" ${bashrc_expander_p_list}
        fi
        #
        for f in ${bashrc_expander_f_list}; do
            ## echo "- source ${myBashDir}/${f}"
            source "${myBashDir}/${f}"
        done
    fi
done


######################################
# Load system-specific context-files #
######################################

# - Prep and sanity check:

if [[ "${bashrc_nonce}" == 'all' ]]; then
    # - Detect and load all sequences, override any previous detections.
    pushd "${myBashDir}"  > /dev/null  2>&1

    bashrc_sequences=$(/bin/ls bashrc_* | grep -v bashrc_0 | cut -f2 -d_ | sort -u)
    bashrc_sequences=$(echo ${bashrc_sequences})    # - Remove newlines

    if [[ "${VERBOSE}" -gt 0 ]]; then
        echo "bashrc_nonce == all"
        echo "bashrc_sequences == '${bashrc_sequences}'"
    fi
    popd  > /dev/null  2>&1
fi


if [[ "${bashrc_sequences}" =~ ^[[:space:]]*$ ]]; then
    echo
    echo "- Warning: no Bashrc Sequences selected."
    echo "  (1) Set a nonce in ~/.bashrc-id, and configure sequence selectors in bashrc_020"
    echo "  (2) Or, put 'all' in ~/.bashrc-id"
    echo "  (3) Or, just set bashrc_sequences in ~/.zrc/bashrc-seq"
    #
    bashrc_sequences="100 101 102"
    echo
    echo " - Using a default set of sequences:"
    echo "   ${bashrc_sequences}"
    echo
else
    true
    ## echo "- bashrc_sequences == '${bashrc_sequences}'"
fi


# - Add additional BASHRC directories, for user-defined sequences after bashrc_boot.
#   Eg, in ~/.zrc.env:
#   $ export myExtraBashDirs="~/.zrc2/ ~/.zrc3/"

if [[ -n "${myExtraBashDirs}" ]]; then
    export myBashDirs="${myBashDir} ${myExtraBashDirs}"
else
    export myBashDirs="${myBashDir}"
fi

for dir in ${myBashDirs}; do
    dir2=$(eval echo "$dir")            # - expand Bash shell ~ references, eg: ~/.zrc2/ -> /home/jdb/.zrc2/
    for i in ${bashrc_sequences}; do
        if [[ -f "${dir2}/bashrc_${i}" ]]; then
            #
            bashrc_expander "${dir2}" "bashrc_${i}"
            if [[ -n "${bashrc_expander_p_list}" ]]; then
                echo "-" ${bashrc_expander_p_list}
            fi
            #
            for f in ${bashrc_expander_f_list}; do
                ## echo "- source ${dir2}/${f}"
                source "${dir2}/${f}"
            done
        fi
    done
done


########
# Done #
########

echo

# - Set the prompt, with our defined alias... if it was defined.
# - Allow overrides, if launched from a wrapper. Eg: 'ty2' alias.

if [[ -n "${bashrc_prompt_orig}" ]]; then
        bashrc_prompt="${bashrc_prompt_orig}"
fi

type prompt > /dev/null 2>&1; rc=$?
if [[ $rc -eq 0 ]]; then
    prompt
fi



###########
# Helpers #
###########

function bashrc_versions() {
    local list num
    local j x v

    for num in $bashrc_sequences; do
        x="v_bashrc_${num}"
        v=v${!x}
        printf "bashrc_%s  %s\n"  "${num}"  "${v}"

        list=$(set | egrep "^v_bashrc_${num}_")
        for j in ${list}; do
            x=$(echo ${j} | cut -f1 -d=)    # v_bashrc_316_b
            v=v${!x}                        # v6.0.0
            x=$(echo ${x} | cut -f3- -d_)   # 316_b
            printf " %-9s  %s\n"  "${x}"  "${v}"
        done
    done

    return 0
}


