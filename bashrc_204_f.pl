
# bashrc_204_f.pl
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.
#
# - pw_grepper : "false positives" detector for Git repositories
# - Template definitions for f_ape_template and f_ape_looper
#
# - Set the "APE_HOSTS" first, eg:
#   mac$ apeho awx2


#################################################
#  !!  This is file is Perl code, not Bash  !!  #
#################################################

use warnings;
use strict;

our $v_bashrc_204_f_pl='7.6.4';



    #############################
    # Standard model for Ubuntu #
    #############################
    #
    # - Eg: Ubuntu 22.04 Jammy Jellyfish

our %templates_ubu = (
    #
    # - Manually set the host, eg:
    #   mac$ apeho awx2
    #
    # - Starter access config:
    #   mac$ apek ~/.ssh-aws/id_ed25519
    #   mac$ apep 22
    #   mac$ apeu ubuntu
    #
    # - Upfront: Manually set the SSH keys:
    #   mac$ ape 99_apek-awk        # - Johns AWS
    #   mac$ ape 99_apek-aws        # - Other AWS
    #   mac$ ape 99_apek-jdb        # - Default, for all PI and non-AWS cloud systems.
    #
    # - Upfront: Manually set the initial SSH login user:
    #   mac$ ape 99_apeu-ubuntu     # - Cloud, including AWS & Digital Ocean
    #   mac$ ape 99_apeu-root       # - Raspberry-PIs, from imager

    "00"    =>  "99_apep-22, 99_apeu-ubuntu",                   # - Override defaults
    "01"    =>  "00_b, 01_a, 01_b",                             # - Introspection prep
    "02"    =>  "01_k, 04_a, 04_b, 04_c, 07_a, 05_b, 05_c",     # - Basic setup & upgrade
    "03"    =>  "05_e, 00_b",                                   # - Reboot

    # - After reboot:
    #
    "04"    =>  "03_a, 03_b, 03_d, 03_g, 03_h, 03_i, 03_j, 03_n",   # - Packages
    "05"    =>  "05_b, 05_c, 05_d, 05_e, 00_b",                     # - Upgrade and reboot?

        #
        # ... Add PI packages here (target boot), eg: ZFS, RPI-EEPROM ...
        #

    # - Add users
    #
    "10"    =>  "30_j, 30_d",                                   # - Add users
    "11"    =>  "04_q, 14_a, 01_b",                             # - Honeypot: sshd-jdb

    # - Confirm access:  mac$ ssh -p 22 jdb@....
    #   -> Then revert Ansible credentials to default:
    #   mac$ apeux
    #   mac$ apekx
    #   mac$ ape 00_b
    #
    "20"    =>  "99_apeux, 99_apekx",                           # - Revert to defaults
    "21"    =>  "00_b, 30_k",                                   # - Confirm remote access

    "23"    =>  "04_o",                                         # - Honeypot: UFW
    "24"    =>  "04_p",                                         # - Honeypot: Fail2Ban
    "25"    =>  "05_e, 00_b",                                   # - Reboot

    # - Confirm access:  mac$ ssh -p 2223 jdb@....
    #   -> Then revert Ansible credentials to default, and reboot:
    #   mac$ apepx
    #   mac$ ape 00_b
    #   mac$ ape 05_e
    #
    "26"    =>  "99_apepx",                                     # - Revert to defaults
    "27"    =>  "00_b",                                         # - Confirm access
    "28"    =>  "14_s, 14_a, 14_b",                             # - Disable regular SSH (caution!)
    "29"    =>  "00_b, 05_e, 00_b",                             # - Reboot

    # - Mainstream applications
    #
    "30"    =>  "01_k, 23_a, 23_i, 23_j, 01_b",                 # - Add Bashrc. (ssh-agent dependency)
    "31"    =>  "04_r",                                         # - Miniconda
    "32"    =>  "04_s, 04_t, 14_g, 14_j",                       # - Web
    "33"    =>  "03_s1, 03_s2,",                                # - Docker

    # - After reboot:
    #
    "41"    =>  "05_f",                                         # - Autoremove
    "42"    =>  "13_c",                                         # - Show Honeypot logs: UFW, Fail2ban
    "43"    =>  "14_a, 14_f, 14_i",                             # - Show listeners and web server statuses
);


# - Consolidate:
#
our %templates_ubu_mega = ();

for my $key (sort keys %templates_ubu) {
    $templates_ubu_mega{"00"} .= $templates_ubu{$key}. ", ";
}

for my $key (qw/00 01 02 03   04 05/) {
    $templates_ubu_mega{"01"} .= $templates_ubu{$key}. ", ";
}

for my $key (qw/10 20   27 30   31 23 24   11 28 26   27 25 32 33   25 41 42 43/) {
    $templates_ubu_mega{"02"} .= $templates_ubu{$key}. ", ";
}


$templates_ubu_mega{"00"} =~ s/,\s*$//;     # - Trim trailing ", "
$templates_ubu_mega{"01"} =~ s/,\s*$//;     # - Trim trailing ", "
$templates_ubu_mega{"02"} =~ s/,\s*$//;     # - Trim trailing ", "




    #####################
    # Additions for DEV #
    #####################

our %templates_dev = (
    "01"    =>  "03_L, 03_r",                       # - Try after reboot.
);


    #####################
    # Additions for JDB #
    #####################

our %templates_jdb = (
    "01"    =>  "20_a",                             # - git-switch-jdb
);



    #####################
    # Additions for AWS #
    #####################

our %templates_aws = (
    "01"    =>  "00_a, 01_a",                       # - AWS SSM (ToDo)
);



    ####################
    # Additions for PI #
    ####################

# - PI template 01 needs to set variable  my_target_server,  eg: 'apple-pi'

our %templates_pi = (

    # - Poweroff.
    "00_x"  =>  "05_x",

    # - [cow-pi boot] Prepare mounted USB disks:
    #   (1) shared Linux & ZFS disks.
    #
    "01"    =>  "13_j, 06_a, 06_b, 06_c, 06_e, 13_j, 40_a",

    # - [cow-pi boot] Prepare mounted USB disks:
    #   (2) distinct Linux & ZFS disks.
    #   -> Disk changes required, with power-off, power-on cycles.
    #
    "01_a"  =>  "13_j, 06_a, 06_b, 06_c, 13_j",     # - Prepare Linux boot disk(s)
    "01_b"  =>  "13_j, 06_e, 13_j, 40_a",           # - Prepare ZFS disk(s)

    # - [target boot] ZFS prep
    "02"    =>  "03_p, 03_m, 05_b, 05_c, 05_d, 05_e, 00_b, 06_j, 13_j, 40_a",

    # - [target boot] Packages, heat logger, speedtest
    "03"    =>  "03_p, 04_j, 04_k, 04_l",

    # - Argon case tools
    "04"    =>  "03_q, 04_i, 05_e, 14_z",
);



    #########################
    # Additions for Bitcoin #
    #########################

our %templates_bitcoin = (
);


    ##########################
    # Additions for Ethereum #
    ##########################

our %templates_ethereum = (
);


    ####################
    # All of the above #
    ####################

our %templates = (
    "ubu"       => { %templates_ubu      },
    "ubu_mega"  => { %templates_ubu_mega },
#
    "aws"       => { %templates_aws      },
    "dev"       => { %templates_dev      },
    "jdb"       => { %templates_jdb      },
    "pi"        => { %templates_pi       },
    "bitcoin"   => { %templates_bitcoin  },
    "ethereum"  => { %templates_ethereum },
);



# - Marker for "Perl require" in the container Perl snippet:

1;      # - "Perl true"


