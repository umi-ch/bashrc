# bashrc_101_j
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - X509 certificate tools

v_bashrc_101_j='10.6.0'


# - Tools included here:
#
#   f_curl_grep
#   i_curl_grep


    ###############
    # f_curl_grep #
    ###############
    #
    # - SSL/TLS handshake review, with Curl, also confirming a connection.
    # - This is a basic Curl wrapper, with error checks.

function f_curl_grep() {
    local host_1="$1"   # - https://en.wikipedia.org
    local host_3        # - temp
    local timeout
    local rc_0=0  rc_1=0
    local v=0
    local html_out='/tmp/qq_curl_grep.html'

    if [[ -n "${VERBOSE}" && "${VERBOSE}" -ge 1 ]]; then
        v=1
        if [[ "${VERBOSE}" -ge 2 ]]; then
            v="${VERBOSE}"
            echo "- VERBOSE $v"
        fi
    fi

    if [[ -z "${host_1}" ]]; then
        echo
        echo "- Error, missing host. Examples:"
        echo "  \$ curl_grep  https://en.wikipedia.org"
        echo "  \$ curl_grep  http://en.wikipedia.org"
        echo "  \$ curl_grep  https://en.wikipedia.org:443"
        echo "  \$ curl_grep  en.wikipedia.org"
        echo "  \$ curl_grep  en.wikipedia.org:443"
        echo
        return 1
    fi

    # - If needed, rewrap hostname into an URL as understood by Curl
    #   awx.umi.ch               ->  https://awx.umi.ch
    #   https://awx.umi.ch       ->  No change.
    #   http://awx.umi.ch        ->  No change; let an HTTPS redirect handle it,  or fail accordingly.
    #
    host_3=$(perl -le '
        use strict;
        $_ = $ARGV[0];

        if ( ! m|https?://(.*)\s*$|) {  # - Append URL prefix?
            $_ = "https://". $_;
        }

        print;
        exit 0;

    ' "${host_1}" )


    if [[ "${host_1}" != "${host_3}" ]]; then
        echo "-> ${host_3}"
        host_1="${host_3}"
    fi

    which curl > /dev/null 2>&1
    rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "- Error, curl not found in the PATH."
        echo
        return 1
    fi

    which timeout > /dev/null 2>&1
    rc=$?
    if [[ $rc -eq 0 ]]; then
        timeout="timeout ${BASHRC_SSL_TIMEOUT}"     # - Fails with rc=124
    else
        echo "-> timeout not found. Skipping firewall-delay shortcut."
        timeout=""                  # - Just do without timeout optimizer.
    fi

    # - Remove previous, if any:
    rm -f "${html_out}"

    echo "-\$ ${timeout}  curl -s -v -L  -w '-> http_code: %{http_code}\n' -o ${html_out}  ${host_1}  2>&1 | egrep '^\*|code'"
    ${timeout}  curl -s -v -L  -w "-> http_code: %{http_code}\n" -o ${html_out}  "${host_1}"  2>&1 | egrep '^\*|code'

    rc_0=${PIPESTATUS[0]}   rc_1=${PIPESTATUS[1]}

    if [[ -z "${rc_0}" ]]; then rc_0=0; fi      # - Not set? Platform dependent quirks.
    if [[ -z "${rc_1}" ]]; then rc_1=0; fi      # - Not set? Platform dependent quirks.

    # - Note the output file, but it's not written if Curl has errors.
    if [[ -f "${html_out}" ]]; then
        echo "-> html file: ${html_out}"
    else
        ## echo "-> no html file."
        true
    fi

    # - Errors?
    #
    if [[ ${rc_0} -ne 0 || ${rc_1} -ne 0 ]]; then

        if [[ ${rc_0} -eq 124 ]]; then
            echo "- Failed: connection timeout, rc=${rc_0}"
            rc_1=0
        elif [[ ${rc_0} -ne 0 ]]; then
            echo "- Failed: curl, rc=${rc_0}"
        elif [[ ${rc_1} -ne 0 ]]; then
            # - NOOP this grep failure
            ## echo "- Failed: egrep, rc=${rc_1}"
            true
            rc_1=0
        fi
    fi

    echo
    rc=$(( $rc_0 + $rc_1 ))
    return $rc
}


# - shortcuts:
alias  curlgrep=f_curl_grep
alias curl_grep=f_curl_grep



    ########
    # Info #
    ########

function i_curl_grep() {
    cat <<END
    #############################################
    # curl_grep:                                #
    # - Curl to a URL, show verbose diagnostics #
    #############################################
    - v_bashrc_101_j: ${v_bashrc_101_j}

# - Connect to an HTTPS server with Curl, showing only the verbose output.
#   This includes the SSL/TLS interactions.
#   The actual html output is saved in a temp file: /tmp/qq_curl_grep.html

$ curl_grep  https://en.wikipedia.org
    -$ timeout 5  curl -s -v -L  -w '-> http_code: %{http_code}\n' -o /tmp/qq_curl_grep.html  https://en.wikipedia.org  2>&1 | egrep '^\*|code'
    * Host en.wikipedia.org:443 was resolved.
    * IPv6: 2a02:ec80:600:ed1a::1
    * IPv4: 185.15.58.224
    *   Trying [2a02:ec80:600:ed1a::1]:443...
    * Connected to en.wikipedia.org (2a02:ec80:600:ed1a::1) port 443
    * ALPN: curl offers h2,http/1.1
    * (304) (OUT), TLS handshake, Client hello (1):
    *  CAfile: /etc/ssl/cert.pem
    *  CApath: none
    * (304) (IN), TLS handshake, Server hello (2):
    * (304) (IN), TLS handshake, Unknown (8):
    * (304) (IN), TLS handshake, Certificate (11):
    * (304) (IN), TLS handshake, CERT verify (15):
    * (304) (IN), TLS handshake, Finished (20):
    * (304) (OUT), TLS handshake, Finished (20):
    * SSL connection using TLSv1.3 / AEAD-CHACHA20-POLY1305-SHA256 / [blank] / UNDEF
    * ALPN: server accepted h2
    * Server certificate:
    *  subject: C=US; ST=California; L=San Francisco; O=Wikimedia Foundation, Inc.; CN=*.wikipedia.org
    *  start date: Sep 26 00:00:00 2024 GMT
    *  expire date: Oct 17 23:59:59 2025 GMT
    *  subjectAltName: host "en.wikipedia.org" matched cert's "*.wikipedia.org"
    *  issuer: C=US; O=DigiCert Inc; CN=DigiCert TLS Hybrid ECC SHA384 2020 CA1
    *  SSL certificate verify ok.
    * using HTTP/2
    * [HTTP/2] [1] OPENED stream for https://en.wikipedia.org/
    * [HTTP/2] [1] [:method: GET]
    * [HTTP/2] [1] [:scheme: https]
    * [HTTP/2] [1] [:authority: en.wikipedia.org]
    * [HTTP/2] [1] [:path: /]
    * [HTTP/2] [1] [user-agent: curl/8.7.1]
    * [HTTP/2] [1] [accept: */*]
    * Request completely sent off
    * Ignoring the response-body
    * Connection #0 to host en.wikipedia.org left intact
    * Issue another request to this URL: 'https://en.wikipedia.org/wiki/Main_Page'
    * Found bundle for host: 0x6000020a50e0 [can multiplex]
    * Re-using existing connection with host en.wikipedia.org
    * [HTTP/2] [3] OPENED stream for https://en.wikipedia.org/wiki/Main_Page
    * [HTTP/2] [3] [:method: GET]
    * [HTTP/2] [3] [:scheme: https]
    * [HTTP/2] [3] [:authority: en.wikipedia.org]
    * [HTTP/2] [3] [:path: /wiki/Main_Page]
    * [HTTP/2] [3] [user-agent: curl/8.7.1]
    * [HTTP/2] [3] [accept: */*]
    * Request completely sent off
    * Connection #0 to host en.wikipedia.org left intact
    -> http_code: 200
    -> html file: /tmp/qq_curl_grep.html

END
}

