# .bash_profile
#  Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

v_bash_profile='7.0.0'

# - Check the number of lines in this file, to detect unwanted changes. Eg: conda
#
wc_l_expected=21

wc_l=$(wc -l .bash_profile |  sed -e 's/^ *//;' | cut -f1 -d' ')

if [[ ${wc_l} -ne ${wc_l_expected} ]]; then
    echo "- .bash_profile:"
    echo "  -> Found ${wc_l} lines, expecting ${wc_l_expected}.  Fix: $ gcb"
fi

if [[ -f ~/.profile ]]; then
    source ~/.profile
fi

return 0
