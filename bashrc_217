# bashrc_217
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - Misc Ubuntu, and Laptop

v_bashrc_217='10.2.1'


    ##########
    # Prompt #
    ##########

# - Distinct and useable Bashrc prompt, eg:
#   b$
#   rh$

if [[ ("$(hostname -s)" == 'laptop') ]]; then
    qq="b"
    export TZ="Europe/Zurich"

elif [[ ("$(hostname -s)" =~ psnx) ]]; then
    qq="rh"
    export TZ="Europe/Zurich"

else
    qq="$(hostname -s)"
    export TZ="Europe/Zurich"
fi

if [[ -z "${root_prompt_suffix}" ]]; then
    bashrc_prompt="${qq}$ "                         # - Eg: b$
else
    bashrc_prompt="${qq}${root_prompt_suffix}$ "    # - Eg: b_R$
fi


    ################
    # Misc configs #
    ################

# - I prefer systemctl output without colors,
#     with it's unwanted side-effect of changing the program tab name to ":"
#   $ man systemctl
#   $ systemctl status apache2
#
export SYSTEMD_COLORS=false


    ################
    # Misc aliases #
    ################

# - Propagate environment variables.
alias sudoe='sudo -E'

# - Is a reboot needed? Eg, after Ubuntu upgrades.
alias is_reboot_needed="echo '-$ ls -l /var/run/ | grep -i reboot'; ls -l /var/run/ | grep -i reboot"

# - Summarize the release info
alias ubuntu_version="echo '-$ cat /etc/os-release | egrep \"^VERSION=|^PRETTY\"'; cat /etc/os-release | egrep '^VERSION=|^PRETTY' | f_indent"
alias ubuntu=ubuntu_version


#############
# Autostart #
#############

# - Load Conda, if present and not already loaded.
#   $ i_python
#
# - FYI: If startup script sets 'CONDA_EXE=disable', Conda will not load here.
#   If Conda is already loaded, ${CONDA_EXE} points to the Python loader script.

if [[ -z "${CONDA_EXE}" && -d ~/miniconda3/ ]]; then
    load_conda silent
fi



###############
# LetsEncrypt #
###############

# - Root CA of LetsEncrypt:
#   https://letsencrypt.org/certificates/

export LE_CA="${myBashDir}"/zPKI/isrgrootx1.pem


