# bashrc_215_b
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - Ubuntu: Macros to DEV & TEST the RDP & VNC

v_bashrc_215_b='9.2.0'


# - These macros are server-specific.

if [[ ("$(hostname -s)" =~ ^(ocean2|ocean3|ocean4)$ ) ]]; then
    /bin/true
else
    return 0
fi



################################
# Ubuntu Desktop & KVM configs #
################################

function    f_iso_date()  { echo $(date '+%Y-%m-%d');           }
function  f_iso_date_2()  { echo $(date '+%Y-%m-%d_%H.%M.%S');  }


# - https://www.libvirt.org/manpages/virsh.html
# - https://libvirt.org/logging.html

export LIBVIRT_DEBUG=''     # 2=info, unset = not verbose
export VIRSH_DEBUG=1        # 1=info
export VIRSH_LOG_FILE=/kvm/logs/log.$(f_iso_date_2)
export LIBVIRT_DEFAULT_URI=qemu:///system


function f_show_libvert() {
    local qq qq2

    qq2="LIBVIRT_DEBUG VIRSH_DEBUG VIRSH_LOG_FILE LIBVIRT_DEFAULT_URI"

    echo
    for qq in ${qq2}; do
        printf "%-20s : %s\n"  ${qq}  ${!qq}
    done
    echo

    return 0
}

alias jj2="echo '-$ f_show_libvert'; f_show_libvert; echo"


function i_ubuntu_18_libvert() {
    cat <<END
    ########################
    # kvm / qemu / libvert #
    ########################
    - v_bashrc_215_b: ${v_bashrc_215_b}

# - https://www.libvirt.org/manpages/virsh.html
# - https://libvirt.org/logging.html

$ f_show_libvert
    LIBVIRT_DEBUG        :
    VIRSH_DEBUG          : 1
    VIRSH_LOG_FILE       : /kvm/logs/log.\$(f_iso_date_2)
    LIBVIRT_DEFAULT_URI  : qemu:///system

# - Worth checking:
o2$ ls -lhF /var/log/libvirt/libvirtd.log
o2$ ls -lhF /run/systemd/journal/socket 

END
}


#############
# Overrides #
#############

alias jj='echo; alias | egrep "alias jj|alias x|alias s[0-9]|alias t[0-9]|alias L[0-9]|alias ipt"; echo'

alias virc="vi ~/.zrc/bashrc_215_b"
alias sorc="source ~/.zrc/bashrc_215_b"


############
# libvirtd #
############

alias L1='echo "-$ sudo systemctl status libvirtd"; sudo systemctl status libvirtd | cat'
alias L2='echo "-$ sudo systemctl restart libvirtd"; sudo systemctl restart libvirtd'
alias L3='echo "-$ sudo systemctl stop libvirtd"; sudo systemctl stop libvirtd'
alias L4='echo "-$ sudo systemctl start libvirtd"; sudo systemctl start libvirtd'


# - Find the first libvirt domain, if any.

export DOM='no-libvert-domain'

function f_get_libvert_domain() {
    local out='' out2=''
    local rc=0

    echo "-$ virsh list --uuid --all | egrep -v '^list' | egrep -E '^.' | sort | head"
    out=$(virsh list --uuid --all 2>&1)
    rc=$?
    if [[ $rc != 0 ]]; then
        echo "   -> error, rc: $rc"
        echo "${out}"
        return 1
    fi

    out2=$(echo "$out" | egrep -v '^list' | egrep -E '^.' | sort | head)
    ## echo "   ${out2}"

    echo "-$ export DOM='${out2}'"
    export "DOM=${out2}"

    return 0    
}
alias dom=f_get_libvert_domain


########
# xrdp #
########

alias x1='sudo lsof -i tcp:3389'

alias x2="date; sudo tail -f /var/log/syslog | grep xrdp"       ## tail -13000f
alias x5="date; sudo tail -f /var/log/xrdp-sesman.log"

alias s1='echo "-$ sudo systemctl status xrdp"; sudo systemctl status xrdp | cat'
alias s2='echo "-$ sudo systemctl restart xrdp"; sudo systemctl restart xrdp'
alias s3='echo "-$ sudo systemctl stop xrdp"; sudo systemctl stop xrdp'
alias s4='echo "-$ sudo systemctl start xrdp"; sudo systemctl start xrdp'

alias s5='echo "-$ sudo systemctl status xrdp-sesman.service"; sudo systemctl status xrdp-sesman.service'


function i_ubuntu_20_xrdp_remote_desktop() {
    cat <<END
    ########################
    # kvm / qemu / libvert #
    ########################
    - v_bashrc_215_b: ${v_bashrc_215_b}

# - ToDo: xrpd often fails to start after server boot.
#   -> Start it manually, like this:

o2$ f_show_libvert
    ...

o2$ sudo systemctl status xrdp
    × xrdp.service - xrdp daemon
         Loaded: loaded (/lib/systemd/system/xrdp.service; enabled; vendor preset: enabled)
         Active: failed (Result: timeout) since Sat 2024-12-14 13:24:57 CET; 18h ago
           Docs: man:xrdp(8)
                 man:xrdp.ini(5)
            CPU: 19ms
    
    Dec 14 13:23:27 ocean2 xrdp[1226]: [INFO ] xrdp_listen_pp done
    Dec 14 13:23:27 ocean2 systemd[1]: xrdp.service: Can't open PID file /run/xrdp/xrdp.pid (yet?) after start: Operation not permitted
    Dec 14 13:23:29 ocean2 xrdp[1236]: [INFO ] starting xrdp with pid 1236
    Dec 14 13:23:29 ocean2 xrdp[1236]: [INFO ] address [0.0.0.0] port [3389] mode 1
    Dec 14 13:23:29 ocean2 xrdp[1236]: [INFO ] listening to port 3389 on 0.0.0.0
    Dec 14 13:23:29 ocean2 xrdp[1236]: [INFO ] xrdp_listen_pp done
    Dec 14 13:24:57 ocean2 systemd[1]: xrdp.service: start operation timed out. Terminating.
    Dec 14 13:24:57 ocean2 xrdp[1236]: [INFO ] Received termination signal, stopping the server accept new connections thread
    Dec 14 13:24:57 ocean2 systemd[1]: xrdp.service: Failed with result 'timeout'.
    Dec 14 13:24:57 ocean2 systemd[1]: Stopped xrdp daemon.

o2$ sudo systemctl restart xrdp
o2$

o2$ sudo systemctl status xrdp
    ● xrdp.service - xrdp daemon
         Loaded: loaded (/lib/systemd/system/xrdp.service; enabled; vendor preset: enabled)
         Active: active (running) since Sun 2024-12-15 08:02:11 CET; 12s ago
           Docs: man:xrdp(8)
                 man:xrdp.ini(5)
        Process: 28856 ExecStartPre=/bin/sh /usr/share/xrdp/socksetup (code=exited, status=0/SUCCESS)
        Process: 28864 ExecStart=/usr/sbin/xrdp $XRDP_OPTIONS (code=exited, status=0/SUCCESS)
       Main PID: 28865 (xrdp)
          Tasks: 1 (limit: 38454)
         Memory: 860.0K
            CPU: 13ms
         CGroup: /system.slice/xrdp.service
                 └─28865 /usr/sbin/xrdp
    
    Dec 15 08:02:10 ocean2 systemd[1]: Starting xrdp daemon...
    Dec 15 08:02:10 ocean2 xrdp[28864]: [INFO ] address [0.0.0.0] port [3389] mode 1
    Dec 15 08:02:10 ocean2 xrdp[28864]: [INFO ] listening to port 3389 on 0.0.0.0
    Dec 15 08:02:10 ocean2 xrdp[28864]: [INFO ] xrdp_listen_pp done
    Dec 15 08:02:10 ocean2 systemd[1]: xrdp.service: Can't open PID file /run/xrdp/xrdp.pid (yet?) after start: Operation not permitted
    Dec 15 08:02:11 ocean2 systemd[1]: Started xrdp daemon.
    Dec 15 08:02:12 ocean2 xrdp[28865]: [INFO ] starting xrdp with pid 28865
    Dec 15 08:02:12 ocean2 xrdp[28865]: [INFO ] address [0.0.0.0] port [3389] mode 1
    Dec 15 08:02:12 ocean2 xrdp[28865]: [INFO ] listening to port 3389 on 0.0.0.0
    Dec 15 08:02:12 ocean2 xrdp[28865]: [INFO ] xrdp_listen_pp done


# - Now, inbound Remote Desktop connections should work, eg from Mac "Windows App".

END
}


#######
# VNC #
#######

alias x3="date; sudo tail -f /var/log/syslog | grep x11"
alias x4="date; sudo tail -f ~/.xsession-errors"

alias t1='echo "-$ sudo systemctl status x11vnc.service"; sudo systemctl status x11vnc.service | cat'
alias t2='echo "-$ sudo systemctl restart x11vnc.service"; sudo systemctl restart x11vnc.service'
alias t3='echo "-$ sudo systemctl stop x11vnc.service"; sudo systemctl stop x11vnc.service'
alias t4='echo "-$ sudo systemctl start x11vnc.service"; sudo systemctl start x11vnc.service'

