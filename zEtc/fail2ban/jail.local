# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - Revamp for Ansible:
## v_fail2ban_jail_local='7.0.1'

# JDB, 10. June 2019
#
# - This file is static content of a base template for fail2ban,
#     using John's "Honeypot security concept".
#   In this concept, Fail2Ban depends on UFW.
#
# - Reminder that Fail2Ban works by scanning log files.
#   It doesn't care directly - whether or not an associated service is running, etc.
#   The indicated [sections] are "jails", aka "filters" defined in /etc/fail2ban/filter.d/
#   Configs within a jail section (like "port=2223") are parameters for use in the filters.
#   Filters with tag 'john' are simple jails written by myself.
#
# - This files works as is, but is intended to be updated by Ansible. Eg:
#   $ egrep      '^ignoreip\s*=' /etc/fail2ban/jail.local
#   $ egrep -A1 '^\['            /etc/fail2ban/jail.local
#
# - There are several options to avoid running this "honeypot concept" on a server:
#   1. Disable fail2ban, or don't install it, or don't install UFW.
#   2. Patch the unwanted jails with 'enabled = no' (including sshd jail)
#      by editing the Ansible config lists in the host vars.
#   3. No action needed, if the service listed in a [jail] does not update log files
#      in the expected location (as defined in /etc/fail2ban/filters.d/ jail definitions.)
#      But if a [jail] is enabled in this jail.local you should 'touch' the log file once
#      to keep Fail2Ban from complaining.
#
# - Be sure the enabled filter sections below correspond to running services,
#     else Fail2Ban gets confused and might not work as expected.
#   Eg:  [apache] should be 'enabled = true' only if Apache is installed and running,


[DEFAULT]

# - Exempted IP ranges, which is updated by Ansible playbook pattern:
#   $ egrep '^ignoreip *=' jail.local
#
ignoreip = 127.0.0.1/8  ::1

# - Any attack bans all access
banaction = iptables-allports

# - Banning:
#   -> forever:  -1
#   -> 2 days:   2d
#   -> 2 weeks: 14d
#
bantime = 14d

# - Detection intervals:
#   -> all day: 1440m
#   -> 2 hours: 120m
#
findtime = 120m

# - Low tolerance:
maxretry = 1


# - SSH listener ports:
#   -> "port" is ignored with "iptables-allports", but included here as an FYI.
#
[sshd]
enabled  = false
mode     = aggressive
port     = ssh,sftp,2223,2224


# - Apache
#   https://www.xmodulo.com/configure-fail2ban-apache-http-server.html

# detect password authentication failures
[apache]
enabled  = false
filter   = apache-auth
logpath  = /var/log/apache*/*error.log

# detect potential search for exploits and php vulnerabilities
[apache-noscript]
enabled  = false
filter   = apache-noscript
logpath  = /var/log/apache*/*error.log

# detect Apache overflow attempts
[apache-overflows]
enabled  = false
filter   = apache-overflows
logpath  = /var/log/apache*/*error.log

# detect failures to find a home directory on a server
[apache-nohome]
enabled  = false
filter   = apache-nohome
logpath  = /var/log/apache*/*error.log


# - NGINX

# Strict and nasty blockers based on HTTP status code.
# Most useful for tests and honeypots.

[nginx-rc-john-400]
enabled   = false
filter    = nginx-rc-john
logpath   = /var/log/nginx/access.log
failregex = ^<HOST>\s.*?\src_400\s

[nginx-rc-john-403]
enabled   = false
filter    = nginx-rc-john
logpath   = /var/log/nginx/access.log
failregex = ^<HOST>\s.*?\src_403\s

[nginx-rc-john-404]
enabled   = false
filter    = nginx-rc-john
logpath   = /var/log/nginx/access.log
failregex = ^<HOST>\s.*?\src_404\s

[nginx-rc-john-420]
enabled   = false
filter    = nginx-rc-john
logpath   = /var/log/nginx/access.log
failregex = ^<HOST>\s.*?\src_420\s

[nginx-rc-john-444]
enabled   = false
filter    = nginx-rc-john
logpath   = /var/log/nginx/access.log
failregex = ^<HOST>\s.*?\src_444\s


# Detect too many requests (Denial of Service)
# https://easyengine.io/tutorials/nginx/fail2ban/

[nginx-req-limit-john]
enabled  = false
filter   = nginx-limit-req-john
logpath  = /var/log/nginx/*error.log


# Detect SSL failures - usually hacker probing, sending plain HTTP to an SSL port.
# https://gist.github.com/carboneater/b0299373d3c016da44c193133978dc86

[nginx-ssl-mismatch-john]
enabled  = false
filter   = nginx-ssl-failure-john
logpath  = /var/log/nginx/*error.log


